// script para reabertura de payments cancelados do atendimento-e-dicas-de-saude-24h

var query = {
  "deal.name": "atendimento-e-dicas-de-saude-24h",
  "deal.alliance.xewards.provider": "",
  "status.name": "done",
  "status.detail.name": "canceled",
  childRewardId: { $exists: false },
  "reward.consumer.mobile": { $exists: false }
};

var bulkWritePayments = [];
var bulkWriteRewards = [];
var bulkWriteTriggers = [];
var bulkWriteRequests = [];

var counter = 0;

var dateNow = new Date();

var requestNewStatus = { name: "dispatched", timestamp: dateNow, detail: { name: "success" } };
var requestStatus = { name: "forwarded", timestamp: dateNow, detail: {} };
var paymentStatus = { name: "forwarded", timestamp: dateNow, detail: { name: "retry", detail: "" } };
var rewardStatus = { name: "forwarded", timestamp: dateNow, detail: {} };

var bonuzPayments = db.getSiblingDB("bonuz").getCollection("payments");
var bonuzRewards = db.getSiblingDB("bonuz").getCollection("rewards");
var engageTriggers = db.getSiblingDB("engage").getCollection("triggers");
var businessRequests = db.getSiblingDB("business").getCollection("requests");

bonuzPayments.find(query).forEach(function (payment) {
  var request = ObjectId(payment.reward.request.id);
  var reward = payment.reward.rewardId;
  var trigger = ObjectId(payment.reward.trigger.id);

  bulkWritePayments.push({
    updateOne: {
      filter: { _id: payment._id },
      update: { $set: { "deal.alliance.xewards.provider": "2019", status: paymentStatus }, $push: { trace: { $each: [paymentStatus], $position: 0 } } }
    }
  });

  bulkWriteRewards.push({
    updateOne: {
      filter: { _id: reward },
      update: { $set: { status: rewardStatus }, $push: { trace: { $each: [rewardStatus], $position: 0 } } }
    }
  });

  bulkWriteTriggers.push({
    updateOne: {
      filter: { _id: trigger },
      update: { $set: { status: requestNewStatus }, $push: { trace: { $each: [requestNewStatus], $position: 0 } } }
    }
  });

  bulkWriteRequests.push({
    updateOne: {
      filter: { _id: request },
      update: { $set: { newStatus: requestNewStatus, status: requestStatus, lastUpdated: dateNow }, $push: { newTrace: { $each: [requestNewStatus], $position: 0 }, trace: { $each: [requestStatus], $position: 0 } } }
    }
  });

  counter++;

  if (counter % 1000 === 0) {
    bonuzPayments.bulkWrite(bulkWritePayments, { writeConcern: { w: 0 }, ordered: false });
    bonuzRewards.bulkWrite(bulkWriteRewards, { writeConcern: { w: 0 }, ordered: false });
    engageTriggers.bulkWrite(bulkWriteTriggers, { writeConcern: { w: 0 }, ordered: false });
    businessRequests.bulkWrite(bulkWriteRequests, { writeConcern: { w: 0 }, ordered: false });

    bulkWritePayments = [];
    bulkWriteRewards = [];
    bulkWriteTriggers = [];
    bulkWriteRequests = [];

    counter = 0;

    print("Enviando batches de 1000 itens");
  }
});

if (counter > 0) {
  bonuzPayments.bulkWrite(bulkWritePayments, { writeConcern: { w: 0 }, ordered: false });
  bonuzRewards.bulkWrite(bulkWriteRewards, { writeConcern: { w: 0 }, ordered: false });
  engageTriggers.bulkWrite(bulkWriteTriggers, { writeConcern: { w: 0 }, ordered: false });
  businessRequests.bulkWrite(bulkWriteRequests, { writeConcern: { w: 0 }, ordered: false });
  
  print("Enviando batches finais de " + counter + " itens");
}