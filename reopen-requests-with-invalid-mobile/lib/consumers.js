function buildConsumer(consumerId, experience, rewardValue) {
  var consumer = {
    "id": consumerId,
    "portals": ["bb"],
    "device": {},
    "sponsors": [],
    "tags": [],
    "executedExperiences": [{ "name": experience }],
    "notificationChannels": [],
    "totalBonus": NumberInt(rewardValue),
    "created": new Date(),
    "countryCode": "BR"
  }

  return consumer;
}