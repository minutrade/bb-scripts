function buildPayment(mobile, reward, deal) {
  var dateNow = new Date();

  if (!reward.rewardId && reward._id) {
    reward.rewardId = reward._id;
  }

  var payment = {
    "amount": NumberInt(deal.amount),
    "mobile": mobile || undefined,
    "trace": [
      {
        "name": "forwarded",
        "timestamp": dateNow
      },
      {
        "name": "created",
        "timestamp": dateNow
      }
    ],
    "reward": reward,
    "deal": deal,
    "deliveryAddress": {
      "questions": []
    },
    "deliveryEngine": deal.deliveryEngine,
    "status": {
      "name": "forwarded",
      "timestamp": dateNow,
      "detail": {
        "name": "retry",
        "description": ""
      }
    },
    "quantity": NumberInt(deal.quantity),
    "value": NumberInt(deal.value),
    "countryCode": "BR"
  }

  return payment;
}