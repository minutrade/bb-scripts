function getExperience(experience) {
  var experiences = {
    "bb-combo-digital": {
      "_id": ObjectId("5af49d09c2b8ca01006f22f0"),
      "name": "bb-combo-digital",
      "title": "Combo Digital",
      "officialPortal": "bb",
      "termsUrl": "",
      "callToAction": "Combo Digital",
      "shortDescription": "Escolha um Combo de Recompensas Digitais todos os meses com R$72",
      "description": "Cliente Combo Digital do BB tem a tarifa de R$72 convertida no Recompensas Digitais, e pode escolher um combo diferente a cada mês com diversas opções bem bacanas. Faça parte do Combo Digital e aproveite os benefícios que só o Banco do Brasil oferece!",
      "appreciationMessage": "Chegou a hora de você escolher o seu Combo Digital. São diversas opções. Clique em ESCOLHER RECOMPENSA, escolha já o seu e aproveite :).",
      "startDate": ISODate("2018-04-25T00:00:00.000-0300"),
      "endDate": ISODate("2118-04-25T11:49:24.321-0300"),
      "maxRecoveryFallback": 3.0,
      "appreciationMessageFallback": {
        "default": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "vivo": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "tim": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "oi": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "claro": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br"
      },
      "daysToQualify": 15.0,
      "interactive": false,
      "countryCode": "BR",
      "fallback": {
        "expirationHours": 48.0,
        "enabled": true
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/appreciation.png"
      },
      "wideBanner": {
        "srcset": "wide.png 1x, wide@1.5x.png 1.5x, wide@2x.png 2x, wide@3x.png 3x, wide@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/wide.png"
      },
      "action": {
        "text": "",
        "executedText": "",
        "executedIcon": {
          "srcset": "",
          "href": ""
        },
        "icon": {
          "srcset": "",
          "href": ""
        }
      },
      "hasBalance": false,
      "reports": {
        "totalAssociatedConsumers": 194903.0,
        "totalCreatedRewards": 4987448.0
      },
      "limits": [

      ],
      "public": true,
      "microxp": false,
      "audience": [

      ],
      "operators": [
        "vivo",
        "claro",
        "tim",
        "oi",
        "nextel",
        "sercomtel",
        "ctbc"
      ],
      "templates": [
        {
          "type": "choice",
          "events": [
            {
              "prize": "",
              "name": "default",
              "templateName": "bb-combo-digital-choice-default"
            }
          ]
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-combo-digital-presentear-amigo-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-combo-digital-primepass-oneshot-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-combo-digital-primepass-duo-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-combo-digital-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-combo-digital-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-combo-digital-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-combo-digital-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-combo-digital-claro-hibrido-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-combo-digital-assinatura-ubook-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-12-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-7e50-reais-em-12-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-7e50-reais-em-8-corridas-delivered"
            },
            {
              "prize": "dentro-da-historia-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-dentro-da-historia-r-30-00-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-em-frete-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-farmacia-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-combo-digital-primepass-smart-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            },
            {
              "prize": "desconto-de-r-80-em-cursos-online",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-de-r-80-em-cursos-online-delivered"
            },
            {
              "prize": "desconto-de-r-120-em-cursos-online",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-de-r-120-em-cursos-online-delivered"
            },
            {
              "prize": "desconto-de-r-150-em-cursos-online",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-de-r-150-em-cursos-online-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": [

          ]
        }
      ],
      "prizes": [
        {
          "_id": ObjectId("576038f2c574c115761ebee3"),
          "name": "assinatura-ubook",
          "title": "Audio-livro - 30 dias",
          "subTitle": "Assinatura ubook – 30 dias",
          "faceValue": 25.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZUBO001",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "forwarded": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "acdc",
            "ps4",
            "audio-livro"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "f0622c",
            "color": "f0622c"
          },
          "deliveryCost": 0.0,
          "price": 7.5,
          "description": "Escute livros e revistas. Exame, Veja e mais diversos títulos para você escutar de onde estiver. Curta suas revistas e livros preferidos por 30 dias com o Ubook, o melhor aplicativo de audiolivros do Brasil.",
          "alliance": {
            "title": "Ubook",
            "name": "ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Diversos livros e revistas para você escutar de onde estiver.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                14.0,
                33.0,
                45.0,
                91.0,
                92.0,
                93.0,
                95.0,
                97.0,
                99.0
              ]
            }
          ],
          "group": {
            "name": "audio-livro",
            "title": "Áudio Livro",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubook",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e4c7422d943655df4828"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "audiolivro-infantis-30-dias",
          "title": "Audiolivro Infantis - 30 dias",
          "subTitle": "Assinatura Ubook Kids de 30 dias",
          "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
          "faceValue": 7.0,
          "price": 2.4,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "audio-livro-infantil"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "xewardsOfferCode": "BZUBK001",
          "shortDescription": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma para os pequenos.",
          "praiseTexts": [

          ],
          "weight": 6.0,
          "socialWeight": 0.0,
          "group": {
            "name": "audio-livro-infantil",
            "title": "Áudio Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubkids",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5ae0d65d91afd663240ff0a9"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-4-corridas",
          "title": "Desconto de R$5,00 em 4 corridas.",
          "subTitle": "Desconto de R$5,00 em 4 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 4 corridas nas principais cidades do país.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA4C20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 4 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify20",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d80c91afd6c9f80ff0aa"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-6-corridas",
          "title": "Desconto de R$5,00 em 6 corridas",
          "subTitle": "Desconto de R$5,00 em 6 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 6 corridas nas principais cidades do país.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA6C30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 6 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify30",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d90ced255d8bda259166"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-8-corridas",
          "title": "Desconto de R$5,00 em 8 corridas",
          "subTitle": "Desconto de R$5,00 em 8 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA8C40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify40",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dc83ed255d6315259167"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-12-corridas",
          "title": "Desconto de R$7,50 em 12 corridas.",
          "subTitle": "Desconto de R$7,50 em 12 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 12 corridas nas principais cidades do país.",
          "faceValue": 90.0,
          "price": 22.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA12C90",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 12 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify90",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dbec91afd676860ff0ac"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-8-corridas",
          "title": "Desconto de R$7,50 em 8 corridas.",
          "subTitle": "Desconto de R$7,50 em 8 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 60.0,
          "price": 17.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA9C68",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify60",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("59ce53a0069814010003eeca"),
          "name": "casquinha-mcdonalds",
          "title": "Casquinha McDonald´s",
          "subTitle": "Uma casquinha McDonald´s",
          "faceValue": 3.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Unidade",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZMCC001",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua casquinha estará disponível em breve.",
            "forwarded": "Sua casquinha estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "cultura",
            "fast-food"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "df1a23",
            "color": "df1a23"
          },
          "deliveryCost": 0.0,
          "price": 0.9,
          "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
          "fullForward": false,
          "alliance": {
            "title": "McDonald's",
            "name": "mcdonalds",
            "xewards": {
              "provider": "2014"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Troque por uma casquinha do McDonald´s e curta essa saborosa recompensa.",
          "praiseTexts": [

          ],
          "weight": 7.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "fast-food",
            "title": "Fast Food",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "casquinha",
          "emoji": "🍦",
          "actions": [
            {
              "title": "ABRIR CUPOM",
              "url": "https://casquinha.bonuz.com/{{coupon.code}}"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f4ee2d50f201001dcf3b"),
          "name": "dentro-da-historia-r-10-00",
          "title": "Desconto livro infantil  - R$10",
          "subTitle": "Livro infantil personalizado - R$10",
          "faceValue": 10.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI10",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 8.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro10",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f5212d50f201001dcf3c"),
          "name": "dentro-da-historia-r-20-00",
          "title": "Desconto livro infantil  - R$20",
          "subTitle": "Livro infantil personalizado - R$20",
          "faceValue": 20.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI20",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 9.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro20",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f54b2d50f201001dcf3d"),
          "name": "dentro-da-historia-r-30-00",
          "title": "Desconto livro infantil  - R$30",
          "subTitle": "Livro infantil personalizado - R$30",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI30",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro30",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5b3145c59a47951f961e4b33"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-100-00-em-frete",
          "title": "Desconto de R$100 em frete",
          "subTitle": "Desconto de R$100,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 100.0,
          "price": 18.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI100",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete100",
          "emoji": "🏍",
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho."
        },
        {
          "_id": ObjectId("5b3145009a4795e3471e4b32"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-70-00-em-frete",
          "title": "Desconto de R$70 em frete",
          "subTitle": "Desconto de R$,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 70.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI070",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete70",
          "emoji": "🏍",
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho."
        },
        {
          "_id": ObjectId("5b31429cefd915af5ffb247b"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-em-frete",
          "title": "Desconto de R$40 em frete",
          "subTitle": "Desconto de R$40,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 40.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI040",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete40",
          "emoji": "🏍",
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho."
        },
        {
          "_id": ObjectId("5b48edea1a380d34dc897034"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-10",
          "title": "Flores com desconto de R$10",
          "subTitle": "Flores com desconto de R$10",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$10 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$10 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ee3d1a380d81b1897035"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-20",
          "title": "Flores com desconto de R$20",
          "subTitle": "Flores com desconto de R$20",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$20 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "un",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$20 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ee841a380decb8897036"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-30",
          "title": "Flores com desconto de R$30",
          "subTitle": "Flores com desconto de R$30",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$30 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$30 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ef3c1a380dbc9d897037"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-40",
          "title": "Flores com desconto de R$40",
          "subTitle": "Flores com desconto de R$40",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$40 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$40 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5a342c58823a2901002aff28"),
          "name": "gympass-mensal-r-30",
          "title": "Desconto mês academia - R$30",
          "subTitle": "R$30 em desconto em mensalidade",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "GYMPM030",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto do Gympassa está a caminho.",
            "forwarded": "Seu desconto do Gympassa está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "academia"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita para fazer uma aula ou treino em mais de 16 mil academias e estúdios parceiros do Gympass em todo o Brasil. São diversos planos a partir de R$69,90.",
          "fullForward": false,
          "alliance": {
            "title": "Gympass",
            "name": "gympass",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal30",
          "emoji": "💪",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://bit.ly/Gympassplano"
            }
          ]
        },
        {
          "_id": ObjectId("58e63bcea983a601007910ff"),
          "name": "internet-claro-300mb",
          "title": "Internet Claro 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 48.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM300",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 14.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("58e63dafa983a60100791100"),
          "name": "internet-claro-400mb",
          "title": "Internet Claro 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 64.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM400",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 19.2,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b843bc11378f010020ce6b"),
          "name": "internet-oi-300mb",
          "title": "Internet Oi 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 46.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 18.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badedb419cfc01002e938a"),
          "name": "internet-oi-400mb",
          "title": "Internet Oi 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 56.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 24.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b8430311378f010020ce69"),
          "name": "internet-vivo-300mb",
          "title": "Internet Vivo 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 52.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 15.6,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badaf4419cfc01002e9387"),
          "name": "internet-vivo-400mb",
          "title": "Internet Vivo 400MB",
          "subTitle": "Pacotes de dados",
          "faceValue": 58.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 17.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("5a0f29bfc903b90100c754b0"),
          "name": "ipass-internacional-mes-vigente",
          "title": "Wi-Fi internacional - mês vigente",
          "subTitle": "WIFI iPass Internacional - Mês Vigente",
          "faceValue": 32.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPAINT",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "wifi",
            "ipass",
            "internacional",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 9.6,
          "description": "Acesso Wi-Fi em locais públicos no mundo para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o mundo pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "internacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("5a0f28e8c903b90100c754af"),
          "name": "ipass-nacional-mes-vigente",
          "title": "ipass-nacional-mes-vigente",
          "subTitle": "WIFI iPass Nacional - Mês Vigente",
          "faceValue": 16.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPANAC",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "ipass",
            "wifi",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 4.8,
          "description": "Acesso Wi-Fi em locais públicos para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o Brasil pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "nacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a8840725f"),
          "alliance": {
            "title": "bonuz.com",
            "name": "bonuz.com",
            "xewards": {
              "provider": ""
            }
          },
          "name": "presentearAmigo",
          "title": "Bônus para Presente",
          "subTitle": "Presenteie parte dos seu bônus",
          "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "donation",
          "customBonuz": {
            "backgroundColor": "FFFFFF",
            "color": "FF0000"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("577bf83b6388d40100b2c820"),
                "required": true,
                "example": "5511988888888",
                "text": "Digite o Celular do seu amigo",
                "name": "Celular",
                "field": "mobile"
              }
            ]
          },
          "tags": [

          ],
          "operators": [
            "claro",
            "oi",
            "tim",
            "vivo",
            "nextel",
            "ctbc",
            "sercomtel"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "bonuz",
          "deliveryInfo": {
            "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Fique conectado com quem você gosta, presenteando com R$xx de crédito em bônus no celular! ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "presente",
          "emoji": "🎁"
        },
        {
          "_id": ObjectId("5adf451668ce666b35ca4793"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-duo",
          "title": "Ingresso de cinema Duo",
          "subTitle": "PrimePass Duo",
          "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "FFFFFF",
            "backgroundColor": "FFFFFF"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ingresso-de-cinema/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPADU",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um par de ingressos para curtir um filme na rede credenciada Primepass mais perto de você. ",
          "praiseTexts": [

          ],
          "weight": 4.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-par",
            "title": "Cinema Par",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso2",
          "emoji": "🎥",
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5a37ef602d50f201001dcf38"),
          "name": "primepass-oneshot",
          "title": "Ingresso cinema",
          "subTitle": "Ingresso único de cinema na rede Primepass",
          "faceValue": 24.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZPRPA01",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "cultura",
            "cinema"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 6.0,
          "price": 7.2,
          "description": "Ingresso de cinema. Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass.",
          "fullForward": false,
          "alliance": {
            "title": "primepass",
            "name": "primepass",
            "xewards": {
              "provider": "2016"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um ingresso para curtir um filme na rede credenciada Primepass mais perto de você. ",
          "praiseTexts": [

          ],
          "weight": 3.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ],
          "group": {
            "name": "cinema",
            "title": "Cinema",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ]
        },
        {
          "_id": ObjectId("5adf462568ce661704ca4794"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-smart",
          "title": "Ingresso de Cinema Smart",
          "subTitle": "Primepass Smart",
          "description": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPASM",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada Primepass mais perto de você. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-mensal",
            "title": "Cinema Mensal",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso30",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ],
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5aff2721ed86f51b5c9f4846"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-110-de-desconto-em-academia",
          "title": "R$110 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$110,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$110,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 72.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "sercomtel",
            "nextel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR72",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na  mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal110",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5aff26dfed86f519f49f4845"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-80-de-desconto-em-academia",
          "title": "R$80 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$80,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$80,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 41.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR41",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal80",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5af5eae4c6ca38787718e89a"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-35-00",
          "title": "Desconto R$35 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 35.0,
          "price": 8.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM35",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista35",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5eb46c6ca38391018e89b"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-40-00",
          "title": "Desconto R$40 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista40",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec01422d94ab9ddf482f"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-45-00",
          "title": "Desconto R$45 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 45.0,
          "price": 11.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM45",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista45",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec92422d948b60df4830"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-50-00",
          "title": "Desconto R$50 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 50.0,
          "price": 12.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM50",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista50",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5edb8422d94b410df4831"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-60-00",
          "title": "Desconto R$60 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 60.0,
          "price": 15.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM60",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista60",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5effe422d943b22df4832"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-70-00",
          "title": "Desconto R$70 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM70",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista70",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5f07cc6ca3843e018e89c"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-80-00",
          "title": "Desconto R$80 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 80.0,
          "price": 20.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM80",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista80",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e87f422d94621edf482d"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-10-00",
          "title": "Revista Impressa ou Digital - R$10",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista10",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e8f7c6ca385df418e897"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-15-00",
          "title": "Desconto R$15 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 15.0,
          "price": 3.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM15",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista15",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e94dc6ca389b6818e898"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-20-00",
          "title": "Revista Impressa ou Digital - R$20",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista20",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e9e0422d947d4fdf482e"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-25-00",
          "title": "Desconto R$25 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 25.0,
          "price": 6.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM25",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa de algumas revistas da Editora 3. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista25",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ea60c6ca381a0418e899"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-30-00",
          "title": "Revista Impressa ou Digital - R$30",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa das revistas IstoÉ ou IstoÉ Dinheiro. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista30",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e419422d943251df4827"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "ureader-livros-digitais-30-dias",
          "title": "Livros digitais para ler",
          "subTitle": "Assinatura Ureader de 30 dias",
          "description": "Ficção, romance, biografias e muito mais. Com o aplicativo Ureader, você tem milhares de títulos para manter a leitura em dia.",
          "faceValue": 20.0,
          "price": 6.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "livro-digital"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZURE001",
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Sua recompensa estará disponível em breve.",
            "forwarded": "Sua recompensa estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua assinatura Ureader de 30 dias está disponível! Acesse https://seu.bz/BktsVo6 insira o código {{voucher}}, faca o cadastro e aproveite!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Ficção, romance, biografias e mais. São vários títulos para manter a leitura em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-digital",
            "title": "Livro Digital",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ureader",
          "emoji": "📔",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://seu.bz/BktsVo6"
            }
          ]
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407250"),
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularOi",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para Oi + Internet + SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "oi"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407251"),
          "alliance": {
            "title": "TIM",
            "name": "tim",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularTIM",
          "title": "bonusCelularTIM",
          "subTitle": "Ligações para TIM, Internet e SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "tim"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407252"),
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularVivo",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para telefones Vivo",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "f65e01",
            "color": "f65e01"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("58d2c7c84e525a01006670a3"),
          "name": "claro-hibrido",
          "title": "Crédito em bônus celular",
          "subTitle": "Hibrido (Voz e Dados)",
          "faceValue": 1.0,
          "catalog": true,
          "type": "defaultPrize",
          "unit": "R$",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCBC001",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue reasonCode value=1}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=2}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=6}}Na maioria das vezes, isto acontece quando o celular é de um plano Pessoa Jurídica ou Corporativo.{{else ifvalue reasonCode value=3}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.3,
          "description": "Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "comboPrizes": [
            {
              "name": "claro-dados",
              "title": "Claro Dados",
              "subTitle": "Claro Dados",
              "faceValue": 0.0,
              "catalog": true,
              "type": "defaultPrize",
              "unit": "R$",
              "deliveryEngine": "xewards",
              "xewardsOfferCode": "BZHIB001",
              "deliveryInfo": {
                "done": "",
                "forwarded": "",
                "created": ""
              },
              "shouldQueryStatus": false,
              "operators": [
                "claro"
              ],
              "tags": [

              ],
              "deliveryAddress": {
                "questions": [

                ]
              },
              "icon": {
                "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
                "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png"
              },
              "customBonuz": {
                "backgroundColor": "ffffff",
                "color": "ffffff"
              },
              "deliveryCost": 0.0,
              "price": 0.3,
              "description": "Claro Dados",
              "fullForward": false,
              "alliance": {
                "xewards": {
                  "provider": "1"
                },
                "title": "Claro",
                "name": "claro"
              },
              "comboType": "gift",
              "expressions": [
                {
                  "expression": "value > 0 and value <= 5",
                  "result": 30.0
                },
                {
                  "expression": "value > 5 and value <= 14",
                  "result": 50.0
                },
                {
                  "expression": "value >= 15",
                  "result": 100.0
                }
              ],
              "__v": 0.0
            }
          ],
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "name": "desconto-no-picanha-barbecue"
        },
        {
          "name": "filmes-e-series-na-looke-por-30-dias",
          "weight": 5.0
        },
        {
          "name": "desconto-de-r-80-em-cursos-online"
        },
        {
          "name": "desconto-de-r-120-em-cursos-online"
        },
        {
          "name": "desconto-de-r-150-em-cursos-online"
        },
        {
          "name": "combo-digital-entretenimento-presentear-amigo"
        },
        {
          "name": "combo-digital-entretenimento-oi"
        },
        {
          "name": "combo-digital-entretenimento-claro"
        },
        {
          "name": "combo-digital-entretenimento-tim"
        },
        {
          "name": "combo-digital-entretenimento-vivo"
        },
        {
          "name": "combo-digital-cultura-presentear-amigo"
        },
        {
          "name": "combo-digital-cultura-claro"
        },
        {
          "name": "combo-digital-cultura-oi"
        },
        {
          "name": "combo-digital-cultura-tim"
        },
        {
          "name": "combo-digital-cultura-vivo"
        },
        {
          "name": "vale-um-cheddar-m-r-17"
        },
        {
          "name": "assinatura-ubook-10-dias"
        },
        {
          "name": "assinatura-ubook-promocional-15-dias"
        },
        {
          "name": "ingresso-de-cinema-2d-de-2-a-4-primepass"
        },
        {
          "name": "atendimento-e-dicas-de-saude-24h"
        }
      ],
      "consumerDataTemplate": [

      ],
      "xewardsOrder": {
        "consumerProfile": "14",
        "projectChannel": "PCBBCODI",
        "upstreamCustomer": "7"
      },
      "video": {
        "href": ""
      },
      "card": {
        "srcset": "",
        "href": ""
      },
      "detailBanner": {
        "srcset": "",
        "href": ""
      },
      "banner": {
        "srcset": "",
        "href": "wide.png 1x, wide@1.5x.png 1.5x, wide@2x.png 2x, wide@3x.png 3x, wide@4x.png 4x"
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
      },
      "rewardOffer": {
        "amount": 72.0,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "target": {
        "type": "URI",
        "action": "https://bonuz.com/"
      },
      "tags": [
        "bb-combo-digital"
      ],
      "keyWords": [

      ],
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      },
      "__v": 0.0,
      "recurrent": true
    },
    "bb-combo-digital-estilo": {
      "_id": ObjectId("5af49d365dccbe01008be667"),
      "name": "bb-combo-digital-estilo",
      "title": "Combo Digital Estilo",
      "officialPortal": "bb",
      "termsUrl": "",
      "callToAction": "Combo Digital Estilo",
      "shortDescription": "Escolha um Combo de Recompensas Digitais todos os meses com R$90",
      "description": "Cliente Combo Digital Estilo do BB tem a tarifa de R$90 convertida no Recompensas Digitais, e pode escolher um combo diferente a cada mês com diversas opções bem bacanas. Faça parte do Combo Digital Estilo e aproveite os benefícios que só o Banco do Brasil oferece!",
      "appreciationMessage": "Cliente Combo Digital Estilo do BB tem a tarifa de R$90 convertida no Recompensas Digitais, e pode escolher um combo diferente a cada mês com diversas opções bem bacanas.\n\nBasta clicar em ESCOLHER RECOMPENSA para aproveitar!",
      "startDate": ISODate("2018-04-25T00:00:00.000-0300"),
      "endDate": ISODate("2118-04-25T11:49:24.321-0300"),
      "maxRecoveryFallback": 3.0,
      "appreciationMessageFallback": {
        "default": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "vivo": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "tim": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "oi": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "claro": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br"
      },
      "daysToQualify": 15.0,
      "interactive": false,
      "countryCode": "BR",
      "fallback": {
        "expirationHours": 48.0,
        "enabled": true
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital-estilo/appreciation.png"
      },
      "wideBanner": {
        "srcset": "wide.png 1x, wide@1.5x.png 1.5x, wide@2x.png 2x, wide@3x.png 3x, wide@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital-estilo/wide.png"
      },
      "action": {
        "text": "",
        "executedText": "",
        "executedIcon": {
          "srcset": "",
          "href": ""
        },
        "icon": {
          "srcset": "",
          "href": ""
        }
      },
      "hasBalance": false,
      "reports": {
        "totalAssociatedConsumers": 180150.0,
        "totalCreatedRewards": 5761409.0
      },
      "limits": [

      ],
      "public": true,
      "microxp": false,
      "audience": [

      ],
      "operators": [
        "vivo",
        "claro",
        "tim",
        "oi",
        "nextel",
        "sercomtel",
        "ctbc"
      ],
      "templates": [
        {
          "type": "choice",
          "events": [
            {
              "prize": "",
              "name": "default",
              "templateName": "bb-combo-digital-estilo-choice-default"
            }
          ]
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-presentear-amigo-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-primepass-smart-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-primepass-oneshot-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-primepass-duo-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-7e50-reais-em-8-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-12-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-7e50-reais-em-12-corridas-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-claro-hibrido-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-20-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-dentro-da-historia-r-20-00-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-assinatura-ubook-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-20-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-20-00-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "dentro-da-historia-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-dentro-da-historia-r-30-00-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-em-frete-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-farmacia-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            },
            {
              "prize": "desconto-de-r-80-em-cursos-online",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-de-r-80-em-cursos-online-delivered"
            },
            {
              "prize": "desconto-de-r-120-em-cursos-online",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-de-r-120-em-cursos-online-delivered"
            },
            {
              "prize": "desconto-de-r-150-em-cursos-online",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-de-r-150-em-cursos-online-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": [

          ]
        }
      ],
      "prizes": [
        {
          "_id": ObjectId("576038f2c574c115761ebee3"),
          "name": "assinatura-ubook",
          "title": "Audio-livro - 30 dias",
          "subTitle": "Assinatura ubook – 30 dias",
          "faceValue": 25.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZUBO001",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "forwarded": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "acdc",
            "ps4",
            "audio-livro"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "f0622c",
            "color": "f0622c"
          },
          "deliveryCost": 0.0,
          "price": 7.5,
          "description": "Escute livros e revistas. Exame, Veja e mais diversos títulos para você escutar de onde estiver. Curta suas revistas e livros preferidos por 30 dias com o Ubook, o melhor aplicativo de audiolivros do Brasil.",
          "alliance": {
            "title": "Ubook",
            "name": "ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Diversos livros e revistas para você escutar de onde estiver.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                14.0,
                33.0,
                45.0,
                91.0,
                92.0,
                93.0,
                95.0,
                97.0,
                99.0
              ]
            }
          ],
          "group": {
            "name": "audio-livro",
            "title": "Áudio Livro",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubook",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e4c7422d943655df4828"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "audiolivro-infantis-30-dias",
          "title": "Audiolivro Infantis - 30 dias",
          "subTitle": "Assinatura Ubook Kids de 30 dias",
          "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
          "faceValue": 7.0,
          "price": 2.4,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "audio-livro-infantil"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "xewardsOfferCode": "BZUBK001",
          "shortDescription": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma para os pequenos.",
          "praiseTexts": [

          ],
          "weight": 6.0,
          "socialWeight": 0.0,
          "group": {
            "name": "audio-livro-infantil",
            "title": "Áudio Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubkids",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5ae0d65d91afd663240ff0a9"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-4-corridas",
          "title": "Desconto de R$5,00 em 4 corridas.",
          "subTitle": "Desconto de R$5,00 em 4 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 4 corridas nas principais cidades do país.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA4C20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 4 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify20",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d80c91afd6c9f80ff0aa"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-6-corridas",
          "title": "Desconto de R$5,00 em 6 corridas",
          "subTitle": "Desconto de R$5,00 em 6 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 6 corridas nas principais cidades do país.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA6C30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 6 corridas nas principais cidades do país. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify30",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d90ced255d8bda259166"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-8-corridas",
          "title": "Desconto de R$5,00 em 8 corridas",
          "subTitle": "Desconto de R$5,00 em 8 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA8C40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify40",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dc83ed255d6315259167"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-12-corridas",
          "title": "Desconto de R$7,50 em 12 corridas.",
          "subTitle": "Desconto de R$7,50 em 12 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 12 corridas nas principais cidades do país.",
          "faceValue": 90.0,
          "price": 22.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA12C90",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 12 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify90",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dbec91afd676860ff0ac"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-8-corridas",
          "title": "Desconto de R$7,50 em 8 corridas.",
          "subTitle": "Desconto de R$7,50 em 8 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 60.0,
          "price": 17.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA9C68",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify60",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("59ce53a0069814010003eeca"),
          "name": "casquinha-mcdonalds",
          "title": "Casquinha McDonald´s",
          "subTitle": "Uma casquinha McDonald´s",
          "faceValue": 3.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Unidade",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZMCC001",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua casquinha estará disponível em breve.",
            "forwarded": "Sua casquinha estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "cultura",
            "fast-food"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "df1a23",
            "color": "df1a23"
          },
          "deliveryCost": 0.0,
          "price": 0.9,
          "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
          "fullForward": false,
          "alliance": {
            "title": "McDonald's",
            "name": "mcdonalds",
            "xewards": {
              "provider": "2014"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Troque por uma casquinha do McDonald´s e curta essa saborosa recompensa.",
          "praiseTexts": [

          ],
          "weight": 7.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "fast-food",
            "title": "Fast Food",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "casquinha",
          "emoji": "🍦",
          "actions": [
            {
              "title": "ABRIR CUPOM",
              "url": "https://casquinha.bonuz.com/{{coupon.code}}"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f4ee2d50f201001dcf3b"),
          "name": "dentro-da-historia-r-10-00",
          "title": "Desconto livro infantil  - R$10",
          "subTitle": "Livro infantil personalizado - R$10",
          "faceValue": 10.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI10",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 8.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro10",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f5212d50f201001dcf3c"),
          "name": "dentro-da-historia-r-20-00",
          "title": "Desconto livro infantil  - R$20",
          "subTitle": "Livro infantil personalizado - R$20",
          "faceValue": 20.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI20",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 9.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro20",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f54b2d50f201001dcf3d"),
          "name": "dentro-da-historia-r-30-00",
          "title": "Desconto livro infantil  - R$30",
          "subTitle": "Livro infantil personalizado - R$30",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI30",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro30",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5b3145c59a47951f961e4b33"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-100-00-em-frete",
          "title": "Desconto de R$100 em frete",
          "subTitle": "Desconto de R$100,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 100.0,
          "price": 18.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI100",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "keyword": "frete100",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b3145009a4795e3471e4b32"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-70-00-em-frete",
          "title": "Desconto de R$70 em frete",
          "subTitle": "Desconto de R$,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 70.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI070",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "keyword": "frete70",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b31429cefd915af5ffb247b"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-em-frete",
          "title": "Desconto de R$40 em frete",
          "subTitle": "Desconto de R$40,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 40.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI040",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete40",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b48edea1a380d34dc897034"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-10",
          "title": "Flores com desconto de R$10",
          "subTitle": "Flores com desconto de R$10",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$10 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$10 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ee3d1a380d81b1897035"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-20",
          "title": "Flores com desconto de R$20",
          "subTitle": "Flores com desconto de R$20",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$20 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "un",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$20 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ee841a380decb8897036"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-30",
          "title": "Flores com desconto de R$30",
          "subTitle": "Flores com desconto de R$30",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$30 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$30 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ef3c1a380dbc9d897037"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-40",
          "title": "Flores com desconto de R$40",
          "subTitle": "Flores com desconto de R$40",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$40 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$40 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5a342c58823a2901002aff28"),
          "name": "gympass-mensal-r-30",
          "title": "Desconto mês academia - R$30",
          "subTitle": "R$30 em desconto em mensalidade",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "GYMPM030",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto do Gympassa está a caminho.",
            "forwarded": "Seu desconto do Gympassa está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "academia"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita para fazer uma aula ou treino em mais de 16 mil academias e estúdios parceiros do Gympass em todo o Brasil. São diversos planos a partir de R$69,90.",
          "fullForward": false,
          "alliance": {
            "title": "Gympass",
            "name": "gympass",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal30",
          "emoji": "💪",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://bit.ly/Gympassplano"
            }
          ]
        },
        {
          "_id": ObjectId("58e63bcea983a601007910ff"),
          "name": "internet-claro-300mb",
          "title": "Internet Claro 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 48.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM300",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 14.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("58e63dafa983a60100791100"),
          "name": "internet-claro-400mb",
          "title": "Internet Claro 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 64.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM400",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 19.2,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b843bc11378f010020ce6b"),
          "name": "internet-oi-300mb",
          "title": "Internet Oi 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 46.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 18.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badedb419cfc01002e938a"),
          "name": "internet-oi-400mb",
          "title": "Internet Oi 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 56.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 24.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b8430311378f010020ce69"),
          "name": "internet-vivo-300mb",
          "title": "Internet Vivo 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 52.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 15.6,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badaf4419cfc01002e9387"),
          "name": "internet-vivo-400mb",
          "title": "Internet Vivo 400MB",
          "subTitle": "Pacotes de dados",
          "faceValue": 58.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 17.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("5a0f29bfc903b90100c754b0"),
          "name": "ipass-internacional-mes-vigente",
          "title": "Wi-Fi internacional - mês vigente",
          "subTitle": "WIFI iPass Internacional - Mês Vigente",
          "faceValue": 32.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPAINT",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "wifi",
            "ipass",
            "internacional",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 9.6,
          "description": "Acesso Wi-Fi em locais públicos no mundo para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o mundo pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "internacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("5a0f28e8c903b90100c754af"),
          "name": "ipass-nacional-mes-vigente",
          "title": "Wi-Fi ilimitado - mês vigente",
          "subTitle": "WIFI iPass Nacional - Mês Vigente",
          "faceValue": 16.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPANAC",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "ipass",
            "wifi",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 4.8,
          "description": "Acesso Wi-Fi em locais públicos para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o Brasil pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "nacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a8840725f"),
          "alliance": {
            "title": "bonuz.com",
            "name": "bonuz.com",
            "xewards": {
              "provider": ""
            }
          },
          "name": "presentearAmigo",
          "title": "Bônus para Presente",
          "subTitle": "Presenteie parte dos seu bônus",
          "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "donation",
          "customBonuz": {
            "backgroundColor": "FFFFFF",
            "color": "FF0000"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("577bf83b6388d40100b2c820"),
                "required": true,
                "example": "5511988888888",
                "text": "Digite o Celular do seu amigo",
                "name": "Celular",
                "field": "mobile"
              }
            ]
          },
          "tags": [

          ],
          "operators": [
            "claro",
            "oi",
            "tim",
            "vivo",
            "nextel",
            "ctbc",
            "sercomtel"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "bonuz",
          "deliveryInfo": {
            "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Fique conectado com quem você gosta, presenteando com R$xx de crédito em bônus no celular!",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "floricultura",
          "emoji": "🎁"
        },
        {
          "_id": ObjectId("5adf451668ce666b35ca4793"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-duo",
          "title": "Ingresso de cinema Duo",
          "subTitle": "PrimePass Duo",
          "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "FFFFFF",
            "backgroundColor": "FFFFFF"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ingresso-de-cinema/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPADU",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um par de ingressos para curtir um filme na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 4.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-par",
            "title": "Cinema Par",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso2",
          "emoji": "🎥",
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5a37ef602d50f201001dcf38"),
          "name": "primepass-oneshot",
          "title": "Ingresso cinema",
          "subTitle": "Ingresso único de cinema na rede Primepass",
          "faceValue": 24.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZPRPA01",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "cultura",
            "cinema"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 6.0,
          "price": 7.2,
          "description": "Ingresso de cinema. Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass.",
          "fullForward": false,
          "alliance": {
            "title": "primepass",
            "name": "primepass",
            "xewards": {
              "provider": "2016"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um ingresso para curtir um filme na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 3.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ],
          "group": {
            "name": "cinema",
            "title": "Cinema",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ]
        },
        {
          "_id": ObjectId("5adf462568ce661704ca4794"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-smart",
          "title": "Ingresso de Cinema Smart",
          "subTitle": "Primepass Smart",
          "description": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPASM",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-mensal",
            "title": "Cinema Mensal",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso30",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ],
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5aff2721ed86f51b5c9f4846"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-110-de-desconto-em-academia",
          "title": "R$110 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$110,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$110,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 72.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "sercomtel",
            "nextel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR72",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na  mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal110",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5aff26dfed86f519f49f4845"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-80-de-desconto-em-academia",
          "title": "R$80 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$80,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$80,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 41.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR41",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na  mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal80",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5af5eae4c6ca38787718e89a"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-35-00",
          "title": "Desconto R$35 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 35.0,
          "price": 8.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM35",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista35",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5eb46c6ca38391018e89b"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-40-00",
          "title": "Desconto R$40 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista40",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec01422d94ab9ddf482f"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-45-00",
          "title": "Desconto R$45 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 45.0,
          "price": 11.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM45",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista45",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec92422d948b60df4830"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-50-00",
          "title": "Desconto R$50 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 50.0,
          "price": 12.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM50",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista50",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5edb8422d94b410df4831"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-60-00",
          "title": "Desconto R$60 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 60.0,
          "price": 15.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM60",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista60",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5effe422d943b22df4832"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-70-00",
          "title": "Desconto R$70 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM70",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista70",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5f07cc6ca3843e018e89c"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-80-00",
          "title": "Desconto R$80 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 80.0,
          "price": 20.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM80",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista80",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e87f422d94621edf482d"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-10-00",
          "title": "Revista Impressa ou Digital - R$10",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista10",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e8f7c6ca385df418e897"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-15-00",
          "title": "Desconto R$15 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 15.0,
          "price": 3.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM15",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista15",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e94dc6ca389b6818e898"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-20-00",
          "title": "Revista Impressa ou Digital - R$20",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista20",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e9e0422d947d4fdf482e"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-25-00",
          "title": "Desconto R$25 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 25.0,
          "price": 6.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM25",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista25",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ea60c6ca381a0418e899"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-30-00",
          "title": "Revista Impressa ou Digital - R$30",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa das revistas IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista30",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e419422d943251df4827"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "ureader-livros-digitais-30-dias",
          "title": "Livros digitais para ler",
          "subTitle": "Assinatura Ureader de 30 dias",
          "description": "Ficção, romance, biografias e muito mais. Com o aplicativo Ureader, você tem milhares de títulos para manter a leitura em dia.",
          "faceValue": 20.0,
          "price": 6.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "livro-digital"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZURE001",
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Sua recompensa estará disponível em breve.",
            "forwarded": "Sua recompensa estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua assinatura Ureader de 30 dias está disponível! Acesse https://seu.bz/BktsVo6 insira o código {{voucher}}, faca o cadastro e aproveite!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Ficção, romance, biografias e mais. São vários títulos para manter a leitura em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-digital",
            "title": "Livro Digital",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ureader",
          "emoji": "📔",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://seu.bz/BktsVo6"
            }
          ]
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407250"),
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularOi",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para Oi + Internet + SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "oi"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407251"),
          "alliance": {
            "title": "TIM",
            "name": "tim",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularTIM",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para TIM, Internet e SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "tim"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407252"),
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularVivo",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para telefones Vivo",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "f65e01",
            "color": "f65e01"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("58d2c7c84e525a01006670a3"),
          "name": "claro-hibrido",
          "title": "Crédito em Bônus Celular",
          "subTitle": "Hibrido (Voz e Dados)",
          "faceValue": 1.0,
          "catalog": true,
          "type": "defaultPrize",
          "unit": "R$",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCBC001",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue reasonCode value=1}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=2}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=6}}Na maioria das vezes, isto acontece quando o celular é de um plano Pessoa Jurídica ou Corporativo.{{else ifvalue reasonCode value=3}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.3,
          "description": "Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "comboPrizes": [
            {
              "name": "claro-dados",
              "title": "Claro Dados",
              "subTitle": "Claro Dados",
              "faceValue": 0.0,
              "catalog": true,
              "type": "defaultPrize",
              "unit": "R$",
              "deliveryEngine": "xewards",
              "xewardsOfferCode": "BZHIB001",
              "deliveryInfo": {
                "done": "",
                "forwarded": "",
                "created": ""
              },
              "shouldQueryStatus": false,
              "operators": [
                "claro"
              ],
              "tags": [

              ],
              "deliveryAddress": {
                "questions": [

                ]
              },
              "icon": {
                "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
                "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png"
              },
              "customBonuz": {
                "backgroundColor": "ffffff",
                "color": "ffffff"
              },
              "deliveryCost": 0.0,
              "price": 0.3,
              "description": "Claro Dados",
              "fullForward": false,
              "alliance": {
                "xewards": {
                  "provider": "1"
                },
                "title": "Claro",
                "name": "claro"
              },
              "comboType": "gift",
              "expressions": [
                {
                  "expression": "value > 0 and value <= 5",
                  "result": 30.0
                },
                {
                  "expression": "value > 5 and value <= 14",
                  "result": 50.0
                },
                {
                  "expression": "value >= 15",
                  "result": 100.0
                }
              ],
              "__v": 0.0
            }
          ],
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "name": "desconto-no-picanha-barbecue"
        },
        {
          "name": "filmes-e-series-na-looke-por-30-dias",
          "weight": 5.0
        },
        {
          "name": "desconto-de-r-80-em-cursos-online"
        },
        {
          "name": "desconto-de-r-120-em-cursos-online"
        },
        {
          "name": "desconto-de-r-150-em-cursos-online"
        },
        {
          "name": "combo-digital-estilo-entretenimento"
        },
        {
          "name": "combo-digital-estilo-cultura"
        },
        {
          "name": "vale-um-cheddar-m-r-17"
        },
        {
          "name": "assinatura-ubook-10-dias"
        },
        {
          "name": "assinatura-ubook-promocional-15-dias"
        },
        {
          "name": "ingresso-de-cinema-2d-de-2-a-4-primepass"
        }
      ],
      "consumerDataTemplate": [

      ],
      "xewardsOrder": {
        "consumerProfile": "15",
        "projectChannel": "PCBBCMES",
        "upstreamCustomer": "7"
      },
      "video": {
        "href": ""
      },
      "card": {
        "srcset": "",
        "href": ""
      },
      "detailBanner": {
        "srcset": "",
        "href": ""
      },
      "banner": {
        "srcset": "",
        "href": ""
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital-estilo/icon.png"
      },
      "rewardOffer": {
        "amount": 90.0,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "target": {
        "type": "URI",
        "action": "https://bonuz.com/"
      },
      "tags": [
        "bb-combo-digital-estilo"
      ],
      "keyWords": [

      ],
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      },
      "__v": 0.0,
      "recurrent": true
    },
    "bb-troca-facil-i": {
      "_id": ObjectId("5af49cccc2b8ca01006f22e2"),
      "name": "bb-troca-facil-i",
      "title": "Combo Troca Fácil I",
      "officialPortal": "bb",
      "termsUrl": "",
      "callToAction": "Combo Troca Fácil I",
      "shortDescription": "Ganhe R$24 e aproveite a melhor recompensa todos os meses",
      "description": "Cliente Troca Fácil I do BB tem a tarifa convertida em Recompensas Digitais, ao manter o pagamento do pacote em dia.\n\nO mais legal é que são diversas opções, e o cliente pode escolher uma recompensa diferente a cada mês.\n\nAproveite os benefícios que só o BB oferece!",
      "appreciationMessage": "Oba, chegou a hora de escolher a sua Recompensa Digital com seus R$24.\n\nSão diversas opções muito legais, clique em ESCOLHER RECOMPENSA e escolha já a sua :)\n\nAproveite!",
      "startDate": ISODate("2018-04-25T00:00:00.000-0300"),
      "endDate": ISODate("2118-04-25T11:49:24.321-0300"),
      "maxRecoveryFallback": 3.0,
      "appreciationMessageFallback": {
        "default": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "vivo": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "tim": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "oi": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "claro": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br"
      },
      "daysToQualify": 15.0,
      "interactive": false,
      "countryCode": "BR",
      "fallback": {
        "expirationHours": 48.0,
        "enabled": true
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/appreciation.png"
      },
      "wideBanner": {
        "srcset": "wide.png 1x, wide@1.5x.png 1.5x, wide@2x.png 2x, wide@3x.png 3x, wide@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/wide.png"
      },
      "action": {
        "text": "",
        "executedText": "",
        "executedIcon": {
          "srcset": "",
          "href": ""
        },
        "icon": {
          "srcset": "",
          "href": ""
        }
      },
      "hasBalance": false,
      "reports": {
        "totalAssociatedConsumers": 2384530.0,
        "totalCreatedRewards": 13387327.0
      },
      "limits": [

      ],
      "public": true,
      "microxp": false,
      "audience": [

      ],
      "operators": [
        "vivo",
        "claro",
        "tim",
        "oi",
        "nextel",
        "sercomtel",
        "ctbc"
      ],
      "templates": [
        {
          "type": "choice",
          "events": [
            {
              "prize": "",
              "name": "default",
              "templateName": "bb-troca-facil-i-choice-default"
            }
          ]
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-bonus-celular-tim-delivered"
            },
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-presentear-amigo-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-claro-hibrido-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-primepass-oneshot-delivered"
            },
            {
              "prize": "gympass-diaria-2",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-gympass-diaria-2-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-20-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "assinatura-ubook-promocional-15-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-assinatura-ubook-promocional-15-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-dentro-da-historia-r-20-00-delivered"
            },
            {
              "prize": "gympass-diaria-1",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-gympass-diaria-1-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-farmacia-delivered"
            },
            {
              "prize": "desconto-em-farmacia-90-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-em-farmacia-90-dias-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-assinatura-ubook-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-em-frete-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-primepass-duo-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-primepass-smart-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": [

          ]
        }
      ],
      "prizes": [
        {
          "_id": ObjectId("5afc45f5f92c2a6034e58fb5"),
          "alliance": {
            "name": "orizon",
            "title": "Orizon",
            "xewards": {
              "provider": "2012"
            }
          },
          "fullForward": false,
          "name": "desconto-em-farmacia-90-dias",
          "title": "Desconto em farmácias",
          "subTitle": "Desconto em farmácia",
          "description": "Descontos de medicamentos em farmácias credenciadas.",
          "faceValue": null,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "priceless",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-90-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [
              {
                "example": "01234578990",
                "text": "Digite o CPF do seu amigo",
                "name": "CPF",
                "field": "cpf",
                "required": true
              }
            ],
            "deliveryRecipientField": "cpf"
          },
          "tags": [
            "saude"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZORI090",
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "O seu desconto em medicamentos estará disponível em breve.",
            "forwarded": "O seu desconto em medicamentos estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}O seu desconto em medicamentos já pode ser utilizado e tem validade de 6 meses. Aproveite nas melhores farmácias!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-90-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Compre nas melhores farmácias com ótimos descontos que só a Orizon oferece!",
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "saude",
            "title": "Saúde",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "farmacia",
          "emoji": "",
          "actions": [
            {
              "title": "BAIXAR O APP",
              "url": "http://onelink.to/jhfydg"
            }
          ]
        },
        {
          "_id": ObjectId("576038f2c574c115761ebee3"),
          "name": "assinatura-ubook",
          "title": "Audio-livro - 30 dias",
          "subTitle": "Assinatura ubook – 30 dias",
          "faceValue": 25.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "DBZUB001",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "forwarded": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "acdc",
            "ps4",
            "audio-livro"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "f0622c",
            "color": "f0622c"
          },
          "deliveryCost": 0.0,
          "price": 7.5,
          "description": "Escute livros e revistas. Exame, Veja e mais diversos títulos para você escutar de onde estiver. Curta suas revistas e livros preferidos por 30 dias com o Ubook, o melhor aplicativo de audiolivros do Brasil.",
          "alliance": {
            "title": "Ubook",
            "name": "ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Diversos livros e revistas para você escutar de onde estiver.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                14.0,
                33.0,
                45.0,
                91.0,
                92.0,
                93.0,
                95.0,
                97.0,
                99.0
              ]
            }
          ],
          "group": {
            "name": "audio-livro",
            "title": "Áudio Livro",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubook",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e4c7422d943655df4828"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "audiolivro-infantis-30-dias",
          "title": "Audiolivro Infantis - 30 dias",
          "subTitle": "Assinatura Ubook Kids de 30 dias",
          "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
          "faceValue": 7.0,
          "price": 2.4,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "audio-livro-infantil"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "xewardsOfferCode": "BZUBK001",
          "shortDescription": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma para os pequenos.",
          "praiseTexts": [

          ],
          "weight": 6.0,
          "socialWeight": 0.0,
          "group": {
            "name": "audio-livro-infantil",
            "title": "Áudio Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubkids",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5ae0d65d91afd663240ff0a9"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-4-corridas",
          "title": "Desconto de R$5,00 em 4 corridas.",
          "subTitle": "Desconto de R$5,00 em 4 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 4 corridas nas principais cidades do país.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA4C20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 4 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify20",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d80c91afd6c9f80ff0aa"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-6-corridas",
          "title": "Desconto de R$5,00 em 6 corridas",
          "subTitle": "Desconto de R$5,00 em 6 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 6 corridas nas principais cidades do país.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA6C30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 6 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify30",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d90ced255d8bda259166"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-8-corridas",
          "title": "Desconto de R$5,00 em 8 corridas",
          "subTitle": "Desconto de R$5,00 em 8 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA8C40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify40",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dc83ed255d6315259167"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-12-corridas",
          "title": "Desconto de R$7,50 em 12 corridas.",
          "subTitle": "Desconto de R$7,50 em 12 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 12 corridas nas principais cidades do país.",
          "faceValue": 90.0,
          "price": 22.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA12C90",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 12 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify90",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dbec91afd676860ff0ac"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-8-corridas",
          "title": "Desconto de R$7,50 em 8 corridas.",
          "subTitle": "Desconto de R$7,50 em 8 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 60.0,
          "price": 17.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA9C68",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify60",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("59ce53a0069814010003eeca"),
          "name": "casquinha-mcdonalds",
          "title": "Casquinha McDonald´s",
          "subTitle": "Uma casquinha McDonald´s",
          "faceValue": 3.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Unidade",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZMCC001",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua casquinha estará disponível em breve.",
            "forwarded": "Sua casquinha estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "cultura",
            "fast-food"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "df1a23",
            "color": "df1a23"
          },
          "deliveryCost": 0.0,
          "price": 0.9,
          "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
          "fullForward": false,
          "alliance": {
            "title": "McDonald's",
            "name": "mcdonalds",
            "xewards": {
              "provider": "2014"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Troque por uma casquinha do McDonald´s e curta essa saborosa recompensa.",
          "praiseTexts": [

          ],
          "weight": 7.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "fast-food",
            "title": "Fast Food",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "casquinha",
          "emoji": "🍦",
          "actions": [
            {
              "title": "ABRIR CUPOM",
              "url": "https://casquinha.bonuz.com/{{coupon.code}}"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f4ee2d50f201001dcf3b"),
          "name": "dentro-da-historia-r-10-00",
          "title": "Desconto livro infantil  - R$10",
          "subTitle": "Livro infantil personalizado - R$10",
          "faceValue": 10.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI10",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 8.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro10",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f5212d50f201001dcf3c"),
          "name": "dentro-da-historia-r-20-00",
          "title": "Desconto livro infantil  - R$20",
          "subTitle": "Livro infantil personalizado - R$20",
          "faceValue": 20.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI20",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 9.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro20",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f54b2d50f201001dcf3d"),
          "name": "dentro-da-historia-r-30-00",
          "title": "Desconto livro infantil  - R$30",
          "subTitle": "Livro infantil personalizado - R$30",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI30",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro30",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5b3145c59a47951f961e4b33"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-100-00-em-frete",
          "title": "Desconto de R$100 em frete",
          "subTitle": "Desconto de R$100,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 100.0,
          "price": 18.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI100",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete100",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b3145009a4795e3471e4b32"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-70-00-em-frete",
          "title": "Desconto de R$70 em frete",
          "subTitle": "Desconto de R$,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 70.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI070",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete70",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b31429cefd915af5ffb247b"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-em-frete",
          "title": "Desconto de R$40 em frete",
          "subTitle": "Desconto de R$40,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 40.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI040",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete40",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b48edea1a380d34dc897034"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-10",
          "title": "Flores com desconto de R$10",
          "subTitle": "Flores com desconto de R$10",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$10 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$20 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ee3d1a380d81b1897035"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-20",
          "title": "Flores com desconto de R$20",
          "subTitle": "Flores com desconto de R$20",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$20 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "un",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$20 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ee841a380decb8897036"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-30",
          "title": "Flores com desconto de R$30",
          "subTitle": "Flores com desconto de R$30",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$30 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$30 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ef3c1a380dbc9d897037"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-40",
          "title": "Flores com desconto de R$40",
          "subTitle": "Flores com desconto de R$40",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$40 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$40 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5a342c58823a2901002aff28"),
          "name": "gympass-mensal-r-30",
          "title": "Desconto mês academia - R$30",
          "subTitle": "R$30 em desconto em mensalidade",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "GYMPM030",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto do Gympassa está a caminho.",
            "forwarded": "Seu desconto do Gympassa está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "academia"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita para fazer uma aula ou treino em mais de 16 mil academias e estúdios parceiros do Gympass em todo o Brasil. São diversos planos a partir de R$69,90.",
          "fullForward": false,
          "alliance": {
            "title": "Gympass",
            "name": "gympass",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal30",
          "emoji": "💪",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://bit.ly/Gympassplano"
            }
          ]
        },
        {
          "_id": ObjectId("58e63bcea983a601007910ff"),
          "name": "internet-claro-300mb",
          "title": "Internet Claro 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 48.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM300",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 14.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("58e63dafa983a60100791100"),
          "name": "internet-claro-400mb",
          "title": "Internet Claro 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 64.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM400",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 19.2,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b843bc11378f010020ce6b"),
          "name": "internet-oi-300mb",
          "title": "Internet Oi 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 46.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 18.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badedb419cfc01002e938a"),
          "name": "internet-oi-400mb",
          "title": "Internet Oi 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 56.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 24.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b8430311378f010020ce69"),
          "name": "internet-vivo-300mb",
          "title": "Internet Vivo 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 52.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 15.6,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badaf4419cfc01002e9387"),
          "name": "internet-vivo-400mb",
          "title": "Internet Vivo 400MB",
          "subTitle": "Pacotes de dados",
          "faceValue": 58.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 17.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("5a0f29bfc903b90100c754b0"),
          "name": "ipass-internacional-mes-vigente",
          "title": "Wi-Fi internacional - mês vigente",
          "subTitle": "WIFI iPass Internacional - Mês Vigente",
          "faceValue": 32.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPAINT",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "wifi",
            "ipass",
            "internacional",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 9.6,
          "description": "Acesso Wi-Fi em locais públicos no mundo para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o mundo pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "internacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("5a0f28e8c903b90100c754af"),
          "name": "ipass-nacional-mes-vigente",
          "title": "Wi-Fi ilimitado - mês vigente",
          "subTitle": "WIFI iPass Nacional - Mês Vigente",
          "faceValue": 16.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPANAC",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "ipass",
            "wifi",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 4.8,
          "description": "Acesso Wi-Fi em locais públicos para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o mundo pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "nacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a8840725f"),
          "alliance": {
            "title": "bonuz.com",
            "name": "bonuz.com",
            "xewards": {
              "provider": ""
            }
          },
          "name": "presentearAmigo",
          "title": "Bônus para Presente",
          "subTitle": "Presenteie parte dos seu bônus",
          "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "donation",
          "customBonuz": {
            "backgroundColor": "FFFFFF",
            "color": "FF0000"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("577bf83b6388d40100b2c820"),
                "required": true,
                "example": "5511988888888",
                "text": "Digite o Celular do seu amigo",
                "name": "Celular",
                "field": "mobile"
              }
            ]
          },
          "tags": [

          ],
          "operators": [
            "claro",
            "oi",
            "tim",
            "vivo",
            "nextel",
            "ctbc",
            "sercomtel"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "bonuz",
          "deliveryInfo": {
            "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Fique conectado com quem você gosta, presenteando com R$xx de crédito em bônus no celular!",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "presente",
          "emoji": "🎁"
        },
        {
          "_id": ObjectId("5adf451668ce666b35ca4793"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-duo",
          "title": "Ingresso de cinema Duo",
          "subTitle": "PrimePass Duo",
          "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "FFFFFF",
            "backgroundColor": "FFFFFF"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ingresso-de-cinema/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPADU",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um par de ingressos para curtir um filme na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 4.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-par",
            "title": "Cinema Par",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso2",
          "emoji": "🎥",
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5a37ef602d50f201001dcf38"),
          "name": "primepass-oneshot",
          "title": "Ingresso cinema",
          "subTitle": "Ingresso único de cinema na rede Primepass",
          "faceValue": 24.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZPRPA01",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "cultura",
            "cinema"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 6.0,
          "price": 7.2,
          "description": "Ingresso de cinema. Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass.",
          "fullForward": false,
          "alliance": {
            "title": "primepass",
            "name": "primepass",
            "xewards": {
              "provider": "2016"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um ingresso para curtir um filme na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 3.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ],
          "group": {
            "name": "cinema",
            "title": "Cinema",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ]
        },
        {
          "_id": ObjectId("5adf462568ce661704ca4794"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-smart",
          "title": "Ingresso de Cinema Smart",
          "subTitle": "Primepass Smart",
          "description": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPASM",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-mensal",
            "title": "Cinema Mensal",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso30",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ],
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5aff2721ed86f51b5c9f4846"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-110-de-desconto-em-academia",
          "title": "R$110 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$110,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$110,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 72.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "sercomtel",
            "nextel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR72",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na  mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal110",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5aff26dfed86f519f49f4845"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-80-de-desconto-em-academia",
          "title": "R$80 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$80,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$80,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 41.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR41",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal80",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5af5eae4c6ca38787718e89a"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-35-00",
          "title": "Desconto R$35 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 35.0,
          "price": 8.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM35",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista35",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5eb46c6ca38391018e89b"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-40-00",
          "title": "Desconto R$40 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista40",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec01422d94ab9ddf482f"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-45-00",
          "title": "Desconto R$45 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 45.0,
          "price": 11.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM45",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista45",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec92422d948b60df4830"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-50-00",
          "title": "Desconto R$50 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 80.0,
          "price": 12.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM50",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista50",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5edb8422d94b410df4831"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-60-00",
          "title": "Desconto R$60 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 60.0,
          "price": 15.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM60",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista60",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5effe422d943b22df4832"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-70-00",
          "title": "Desconto R$70 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM70",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista70",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5f07cc6ca3843e018e89c"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-80-00",
          "title": "Desconto R$80 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 80.0,
          "price": 20.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM80",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista80",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e87f422d94621edf482d"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-10-00",
          "title": "Revista Impressa ou Digital - R$10",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista10",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e8f7c6ca385df418e897"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-15-00",
          "title": "Desconto R$15 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 15.0,
          "price": 3.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM15",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista15",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e94dc6ca389b6818e898"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-20-00",
          "title": "Revista Impressa ou Digital - R$20",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista20",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e9e0422d947d4fdf482e"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-25-00",
          "title": "Desconto R$25 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 25.0,
          "price": 6.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM25",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista25",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ea60c6ca381a0418e899"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-30-00",
          "title": "Revista Impressa ou Digital - R$30",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa das revistas IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista30",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e419422d943251df4827"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "ureader-livros-digitais-30-dias",
          "title": "Livros digitais para ler",
          "subTitle": "Assinatura Ureader de 30 dias",
          "description": "Ficção, romance, biografias e muito mais. Com o aplicativo Ureader, você tem milhares de títulos para manter a leitura em dia.",
          "faceValue": 20.0,
          "price": 6.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "livro-digital"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZURE001",
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Sua recompensa estará disponível em breve.",
            "forwarded": "Sua recompensa estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua assinatura Ureader de 30 dias está disponível! Acesse https://seu.bz/BktsVo6 insira o código {{voucher}}, faca o cadastro e aproveite!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Ficção, romance, biografias e mais. São vários títulos para manter a leitura em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-digital",
            "title": "Livro Digital",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ureader",
          "emoji": "📔",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://seu.bz/BktsVo6"
            }
          ]
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407250"),
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularOi",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para Oi + Internet + SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "oi"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407251"),
          "alliance": {
            "title": "TIM",
            "name": "tim",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularTIM",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para TIM, Internet e SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "tim"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407252"),
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularVivo",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para telefones Vivo",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "f65e01",
            "color": "f65e01"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("58d2c7c84e525a01006670a3"),
          "name": "claro-hibrido",
          "title": "Crédito em bônus celular",
          "subTitle": "Hibrido (Voz e Dados)",
          "faceValue": 1.0,
          "catalog": true,
          "type": "defaultPrize",
          "unit": "R$",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCBC001",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue reasonCode value=1}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=2}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=6}}Na maioria das vezes, isto acontece quando o celular é de um plano Pessoa Jurídica ou Corporativo.{{else ifvalue reasonCode value=3}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.3,
          "description": "Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "comboPrizes": [
            {
              "name": "claro-dados",
              "title": "Claro Dados",
              "subTitle": "Claro Dados",
              "faceValue": 0.0,
              "catalog": true,
              "type": "defaultPrize",
              "unit": "R$",
              "deliveryEngine": "xewards",
              "xewardsOfferCode": "BZHIB001",
              "deliveryInfo": {
                "done": "",
                "forwarded": "",
                "created": ""
              },
              "shouldQueryStatus": false,
              "operators": [
                "claro"
              ],
              "tags": [

              ],
              "deliveryAddress": {
                "questions": [

                ]
              },
              "icon": {
                "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
                "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png"
              },
              "customBonuz": {
                "backgroundColor": "ffffff",
                "color": "ffffff"
              },
              "deliveryCost": 0.0,
              "price": 0.3,
              "description": "Claro Dados",
              "fullForward": false,
              "alliance": {
                "xewards": {
                  "provider": "1"
                },
                "title": "Claro",
                "name": "claro"
              },
              "comboType": "gift",
              "expressions": [
                {
                  "expression": "value > 0 and value <= 5",
                  "result": 30.0
                },
                {
                  "expression": "value > 5 and value <= 14",
                  "result": 50.0
                },
                {
                  "expression": "value >= 15",
                  "result": 100.0
                }
              ],
              "__v": 0.0
            }
          ],
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "name": "desconto-no-picanha-barbecue"
        },
        {
          "name": "filmes-e-series-na-looke-por-30-dias",
          "weight": 5.0
        },
        {
          "name": "troca-facil-i-cultura-presentear-amigo"
        },
        {
          "name": "troca-facil-i-cultura-claro"
        },
        {
          "name": "troca-facil-i-cultura-tim"
        },
        {
          "name": "troca-facil-i-cultura-oi"
        },
        {
          "name": "troca-facil-i-cultura-vivo"
        },
        {
          "name": "vale-um-cheddar-m-r-17"
        },
        {
          "name": "assinatura-ubook-10-dias"
        },
        {
          "name": "assinatura-ubook-promocional-15-dias"
        },
        {
          "name": "ingresso-de-cinema-2d-de-2-a-4-primepass"
        },
        {
          "name": "desconto-de-r-80-em-cursos-online"
        }
      ],
      "consumerDataTemplate": [

      ],
      "xewardsOrder": {
        "consumerProfile": "12",
        "projectChannel": "PCBBTF01",
        "upstreamCustomer": "7"
      },
      "video": {
        "href": ""
      },
      "card": {
        "srcset": "",
        "href": ""
      },
      "detailBanner": {
        "srcset": "",
        "href": ""
      },
      "banner": {
        "srcset": "",
        "href": ""
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/icon.png"
      },
      "rewardOffer": {
        "amount": 24.0,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "target": {
        "type": "URI",
        "action": "https://bonuz.com/"
      },
      "tags": [

      ],
      "keyWords": [

      ],
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      },
      "recurrent": true,
      "__v": 0.0
    },
    "bb-troca-facil-ii": {
      "_id": ObjectId("5af49c7b9dac9e0100823520"),
      "name": "bb-troca-facil-ii",
      "title": "Combo Troca Fácil II",
      "officialPortal": "bb",
      "termsUrl": "",
      "callToAction": "Troca Fácil II",
      "shortDescription": "Ganhe R$41 e aproveite a melhor recompensa todos os meses",
      "description": "Cliente Troca Fácil II do BB tem a tarifa convertida em Recompensas Digitais, ao manter o pagamento do pacote em dia.\n\nO mais legal é que são diversas opções, e o cliente pode escolher uma recompensa diferente a cada mês.\n\nAproveite os benefícios que só o BB oferece!",
      "appreciationMessage": "Oba, chegou a hora de escolher a sua Recompensa Digital.\n\nSão diversas opções muito legais, clique em ESCOLHER RECOMPENSA e escolha já a sua :)\n\nAproveite!",
      "startDate": ISODate("2018-04-25T00:00:00.000-0300"),
      "endDate": ISODate("2118-04-25T11:49:24.321-0300"),
      "maxRecoveryFallback": 3.0,
      "appreciationMessageFallback": {
        "default": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "vivo": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "tim": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "oi": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br",
        "claro": "Não foi possível entregar sua recompensa, mas você não pode perder essa chance! \n\nEscolha outra recompensa e se quiser, confira as regras de participação em www.recompensasdigitais.com.br"
      },
      "daysToQualify": 15.0,
      "interactive": false,
      "countryCode": "BR",
      "fallback": {
        "enabled": true,
        "expirationHours": 360.0
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/appreciation.png"
      },
      "wideBanner": {
        "srcset": "wide.png 1x, wide@1.5x.png 1.5x, wide@2x.png 2x, wide@3x.png 3x, wide@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/wide.png"
      },
      "action": {
        "text": "",
        "executedText": "",
        "executedIcon": {
          "srcset": "",
          "href": ""
        },
        "icon": {
          "srcset": "",
          "href": ""
        }
      },
      "hasBalance": false,
      "reports": {
        "totalAssociatedConsumers": 545460.0,
        "totalCreatedRewards": 5519895.0
      },
      "limits": [

      ],
      "public": true,
      "microxp": false,
      "audience": [

      ],
      "operators": [
        "vivo",
        "claro",
        "tim",
        "oi",
        "nextel",
        "sercomtel",
        "ctbc"
      ],
      "templates": [
        {
          "type": "choice",
          "events": [
            {
              "prize": "",
              "name": "default",
              "templateName": "bb-troca-facil-ii-choice-default"
            }
          ]
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "dentro-da-historia-r-30-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-dentro-da-historia-r-30-00-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-primepass-oneshot-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-assinatura-ubook-delivered"
            },
            {
              "prize": "dentro-da-historia-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-dentro-da-historia-r-20-00-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-farmacia-delivered"
            },
            {
              "prize": "desconto-em-farmacia-180-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-em-farmacia-180-dias-delivered"
            },
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-presentear-amigo-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-claro-hibrido-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-20-00-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-12-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-7e50-reais-em-12-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-7e50-reais-em-8-corridas-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-em-frete-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-primepass-duo-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-primepass-smart-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": [

          ]
        }
      ],
      "prizes": [
        {
          "_id": ObjectId("5afc4632f92c2acafce58fb6"),
          "alliance": {
            "name": "orizon",
            "title": "Orizon",
            "xewards": {
              "provider": "2012"
            }
          },
          "fullForward": false,
          "name": "desconto-em-farmacia-180-dias",
          "title": "Desconto em farmácias",
          "subTitle": "Desconto em farmácia",
          "shortDescription": "Compre nas melhores farmácias com ótimos descontos que só a Orizon oferece!",
          "description": "Descontos de medicamentos em farmácias credenciadas.",
          "faceValue": null,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "priceless",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-180-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [
              {
                "example": "01234578990",
                "text": "Digite o CPF do seu amigo",
                "name": "CPF",
                "field": "cpf",
                "required": true
              }
            ],
            "deliveryRecipientField": "cpf"
          },
          "tags": [
            "saude"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZORI180",
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "O seu desconto em medicamentos estará disponível em breve.",
            "forwarded": "O seu desconto em medicamentos estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}O seu desconto em medicamentos já pode ser utilizado e tem validade de 6 meses. Aproveite nas melhores farmácias!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-180-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "saude",
            "title": "Saúde",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "farmacia",
          "emoji": "",
          "actions": [
            {
              "title": "BAIXAR O APP",
              "url": "http://onelink.to/jhfydg"
            }
          ]
        },
        {
          "_id": ObjectId("576038f2c574c115761ebee3"),
          "name": "assinatura-ubook",
          "title": "Audio-livro - 30 dias",
          "subTitle": "Assinatura ubook – 30 dias",
          "faceValue": 25.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZUBO001",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "forwarded": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "acdc",
            "ps4",
            "audio-livro"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "f0622c",
            "color": "f0622c"
          },
          "deliveryCost": 0.0,
          "price": 7.5,
          "description": "Escute livros e revistas. Exame, Veja e mais diversos títulos para você escutar de onde estiver. Curta suas revistas e livros preferidos por 30 dias com o Ubook, o melhor aplicativo de audiolivros do Brasil.",
          "alliance": {
            "title": "Ubook",
            "name": "ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Diversos livros e revistas para você escutar de onde estiver.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                14.0,
                33.0,
                45.0,
                91.0,
                92.0,
                93.0,
                95.0,
                97.0,
                99.0
              ]
            }
          ],
          "group": {
            "name": "audio-livro",
            "title": "Áudio Livro",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubook",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e4c7422d943655df4828"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "audiolivro-infantis-30-dias",
          "title": "Audiolivro Infantis - 30 dias",
          "subTitle": "Assinatura Ubook Kids de 30 dias",
          "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
          "faceValue": 7.0,
          "price": 2.4,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "audio-livro-infantil"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "xewardsOfferCode": "BZUBK001",
          "shortDescription": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma para os pequenos.",
          "praiseTexts": [

          ],
          "weight": 6.0,
          "socialWeight": 0.0,
          "group": {
            "name": "audio-livro-infantil",
            "title": "Áudio Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ubkids",
          "emoji": "🎧",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.ubook.com/bonuz"
            }
          ]
        },
        {
          "_id": ObjectId("5ae0d65d91afd663240ff0a9"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-4-corridas",
          "title": "Desconto de R$5,00 em 4 corridas.",
          "subTitle": "Desconto de R$5,00 em 4 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 4 corridas nas principais cidades do país.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA4C20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 4 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify20",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d80c91afd6c9f80ff0aa"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-6-corridas",
          "title": "Desconto de R$5,00 em 6 corridas",
          "subTitle": "Desconto de R$5,00 em 6 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 6 corridas nas principais cidades do país.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA6C30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 6 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify30",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0d90ced255d8bda259166"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-5-reais-em-8-corridas",
          "title": "Desconto de R$5,00 em 8 corridas",
          "subTitle": "Desconto de R$5,00 em 8 corridas.",
          "description": "Com Cabify você ganha R$5,00 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA8C40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$5 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify40",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dc83ed255d6315259167"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-12-corridas",
          "title": "Desconto de R$7,50 em 12 corridas.",
          "subTitle": "Desconto de R$7,50 em 12 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 12 corridas nas principais cidades do país.",
          "faceValue": 90.0,
          "price": 22.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA12C90",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 12 corridas nas principais cidades do país. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify90",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("5ae0dbec91afd676860ff0ac"),
          "alliance": {
            "name": "CABIFY",
            "title": "Cabify",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "cabify-desconto-7e50-reais-em-8-corridas",
          "title": "Desconto de R$7,50 em 8 corridas.",
          "subTitle": "Desconto de R$7,50 em 8 corridas.",
          "description": "Com Cabify você ganha R$7,50 de desconto em 8 corridas nas principais cidades do país.",
          "faceValue": 60.0,
          "price": 17.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-9-corridas/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "transporte"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZCA9C68",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Com Cabify você troca R$7,50 em desconto para 8 corridas nas principais cidades do país.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "cabify60",
          "emoji": "🚕"
        },
        {
          "_id": ObjectId("59ce53a0069814010003eeca"),
          "name": "casquinha-mcdonalds",
          "title": "Casquinha McDonald´s",
          "subTitle": "Uma casquinha McDonald´s",
          "faceValue": 3.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Unidade",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZMCC001",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua casquinha estará disponível em breve.",
            "forwarded": "Sua casquinha estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "cultura",
            "fast-food"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "df1a23",
            "color": "df1a23"
          },
          "deliveryCost": 0.0,
          "price": 0.9,
          "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
          "fullForward": false,
          "alliance": {
            "title": "McDonald's",
            "name": "mcdonalds",
            "xewards": {
              "provider": "2014"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Troque por uma casquinha do McDonald´s e curta essa saborosa recompensa.",
          "praiseTexts": [

          ],
          "weight": 7.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "fast-food",
            "title": "Fast Food",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "casquinha",
          "emoji": "🍦",
          "actions": [
            {
              "title": "ABRIR CUPOM",
              "url": "https://casquinha.bonuz.com/{{coupon.code}}"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f4ee2d50f201001dcf3b"),
          "name": "dentro-da-historia-r-10-00",
          "title": "Desconto livro infantil  - R$10",
          "subTitle": "Livro infantil personalizado - R$10",
          "faceValue": 10.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI10",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "kids",
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 8.0,
          "socialWeight": 0.0,
          "rules": [

          ],
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro10",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f5212d50f201001dcf3c"),
          "name": "dentro-da-historia-r-20-00",
          "title": "Desconto livro infantil  - R$20",
          "subTitle": "Livro infantil personalizado - R$20",
          "faceValue": 20.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI20",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 9.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro20",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5a37f54b2d50f201001dcf3d"),
          "name": "dentro-da-historia-r-30-00",
          "title": "Desconto livro infantil  - R$30",
          "subTitle": "Livro infantil personalizado - R$30",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZDDHI30",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "livro-infantil"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
          "fullForward": false,
          "alliance": {
            "title": "Dentro da História",
            "name": "dentrodahistoria",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Torne seu pequeno um protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-infantil",
            "title": "Livro Infantil",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "dentro30",
          "emoji": "📖",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.dentrodahistoria.com.br/"
            }
          ]
        },
        {
          "_id": ObjectId("5b3145c59a47951f961e4b33"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-100-00-em-frete",
          "title": "Desconto de R$100 em frete",
          "subTitle": "Desconto de R$100,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 100.0,
          "price": 18.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI100",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "keyword": "frete100",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b3145009a4795e3471e4b32"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-de-r-70-00-em-frete",
          "title": "Desconto de R$70 em frete",
          "subTitle": "Desconto de R$,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 70.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "1",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI070",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "frete70",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b31429cefd915af5ffb247b"),
          "alliance": {
            "name": "RAPPI",
            "title": "RAPPI",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "desconto-em-frete",
          "title": "Desconto de R$40 em frete",
          "subTitle": "Desconto de R$40,00 no frete da Rappi",
          "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
          "faceValue": 40.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "delivery"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZRPI040",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "delivery",
            "title": "Delivery",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "shortDescription": "Roupa, petshop ou supermercado? A Rappi agiliza tudo isso e entrega na sua casa ou trabalho.",
          "keyword": "frete40",
          "emoji": "🏍"
        },
        {
          "_id": ObjectId("5b48edea1a380d34dc897034"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-10",
          "title": "Flores com desconto de R$10",
          "subTitle": "Flores com desconto de R$10",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$10 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Descontos de até 85% em diversos medicamentos nas melhores farmácias do Brasil."
        },
        {
          "_id": ObjectId("5b48ee3d1a380d81b1897035"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-20",
          "title": "Flores com desconto de R$20",
          "subTitle": "Flores com desconto de R$20",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$20 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "un",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$20 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ee841a380decb8897036"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-30",
          "title": "Flores com desconto de R$30",
          "subTitle": "Flores com desconto de R$30",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$30 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$30 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5b48ef3c1a380dbc9d897037"),
          "alliance": {
            "name": "GIULIANA FLORES",
            "title": "GIULIANA FLORES",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "flores-com-desconto-de-r-40",
          "title": "Flores com desconto de R$40",
          "subTitle": "Flores com desconto de R$40",
          "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$40 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "group": {
            "name": "floricultura",
            "title": "Floricultura",
            "created": ISODate("2018-08-06T15:00:00.000-0300")
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZGIFL40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "",
            "forwarded": "",
            "done": ""
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Em qualquer data, aproveite R$40 de desconto para presentear com flores ou kits especiais Giuliana Flores."
        },
        {
          "_id": ObjectId("5a342c58823a2901002aff28"),
          "name": "gympass-mensal-r-30",
          "title": "Desconto mês academia - R$30",
          "subTitle": "R$30 em desconto em mensalidade",
          "faceValue": 30.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "GYMPM030",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu desconto do Gympassa está a caminho.",
            "forwarded": "Seu desconto do Gympassa está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "academia"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.0,
          "description": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita para fazer uma aula ou treino em mais de 16 mil academias e estúdios parceiros do Gympass em todo o Brasil. São diversos planos a partir de R$69,90.",
          "fullForward": false,
          "alliance": {
            "title": "Gympass",
            "name": "gympass",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal30",
          "emoji": "💪",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://bit.ly/Gympassplano"
            }
          ]
        },
        {
          "_id": ObjectId("58e63bcea983a601007910ff"),
          "name": "internet-claro-300mb",
          "title": "Internet Claro 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 48.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM300",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 14.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("58e63dafa983a60100791100"),
          "name": "internet-claro-400mb",
          "title": "Internet Claro 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 64.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCPM400",
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 19.2,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b843bc11378f010020ce6b"),
          "name": "internet-oi-300mb",
          "title": "Internet Oi 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 46.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 18.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badedb419cfc01002e938a"),
          "name": "internet-oi-400mb",
          "title": "Internet Oi 400MB",
          "subTitle": "Pacote de dados",
          "faceValue": 56.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZOPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "oi"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 24.0,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59b8430311378f010020ce69"),
          "name": "internet-vivo-300mb",
          "title": "Internet Vivo 300MB",
          "subTitle": "Pacote de dados",
          "faceValue": 52.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM300",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 15.6,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados300mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("59badaf4419cfc01002e9387"),
          "name": "internet-vivo-400mb",
          "title": "Internet Vivo 400MB",
          "subTitle": "Pacotes de dados",
          "faceValue": 58.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "MB",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZVPM400",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Seu pacote de dados será entregue em breve.",
            "forwarded": "Seu pacote de dados será entregue em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-icon.png"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 17.4,
          "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
          "fullForward": false,
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": "3"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "internet",
            "title": "Internet Móvel",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "dados400mb",
          "emoji": "🌐"
        },
        {
          "_id": ObjectId("5a0f29bfc903b90100c754b0"),
          "name": "ipass-internacional-mes-vigente",
          "title": "Wi-Fi internacional - mês vigente",
          "subTitle": "WIFI iPass Internacional - Mês Vigente",
          "faceValue": 32.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPAINT",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "wifi",
            "ipass",
            "internacional",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 9.6,
          "description": "Acesso Wi-Fi em locais públicos no mundo para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o mundo pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "internacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("5a0f28e8c903b90100c754af"),
          "name": "ipass-nacional-mes-vigente",
          "title": "Wi-Fi ilimitado - mês vigente",
          "subTitle": "WIFI iPass Nacional - Mês Vigente",
          "faceValue": 16.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "Assinatura",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZIPANAC",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Sua recompensa do iPass estará disponível em breve.",
            "forwarded": "Sua recompensa do iPass estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "ipass",
            "wifi",
            "internet"
          ],
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("58791a2b3100450100de21c0"),
                "required": true,
                "example": "exemplo@email.com",
                "text": "Digite o seu email",
                "name": "Email",
                "field": "email"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu primeiro nome",
                "name": "FirstName",
                "field": "firstname"
              },
              {
                "_id": ObjectId("58791a2b3100450100de21bf"),
                "required": false,
                "example": "",
                "text": "Digite o seu sobrenome",
                "name": "LastName",
                "field": "lastname"
              }
            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 4.8,
          "description": "Acesso Wi-Fi em locais públicos para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
          "fullForward": false,
          "alliance": {
            "title": "iPass",
            "name": "ipass",
            "xewards": {
              "provider": "2015"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Wi-fi em todo o mundo pra se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "internet",
            "title": "Internet",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "nacional",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a8840725f"),
          "alliance": {
            "title": "bonuz.com",
            "name": "bonuz.com",
            "xewards": {
              "provider": ""
            }
          },
          "name": "presentearAmigo",
          "title": "Bônus para Presente",
          "subTitle": "Presenteie parte dos seu bônus",
          "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "donation",
          "customBonuz": {
            "backgroundColor": "FFFFFF",
            "color": "FF0000"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [
              {
                "_id": ObjectId("577bf83b6388d40100b2c820"),
                "required": true,
                "example": "5511988888888",
                "text": "Digite o Celular do seu amigo",
                "name": "Celular",
                "field": "mobile"
              }
            ]
          },
          "tags": [

          ],
          "operators": [
            "claro",
            "oi",
            "tim",
            "vivo",
            "nextel",
            "ctbc",
            "sercomtel"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "bonuz",
          "deliveryInfo": {
            "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
            "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Fique conectado com quem você gosta, presenteando com R$xx de crédito em bônus no celular!",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "presente",
          "emoji": "🎁"
        },
        {
          "_id": ObjectId("5adf451668ce666b35ca4793"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-duo",
          "title": "Ingresso de cinema Duo",
          "subTitle": "PrimePass Duo",
          "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "FFFFFF",
            "backgroundColor": "FFFFFF"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ingresso-de-cinema/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPADU",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um par de ingressos para curtir um filme na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 4.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-par",
            "title": "Cinema Par",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso2",
          "emoji": "🎥",
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5a37ef602d50f201001dcf38"),
          "name": "primepass-oneshot",
          "title": "Ingresso cinema",
          "subTitle": "Ingresso único de cinema na rede Primepass",
          "faceValue": 24.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "unit": "voucher",
          "deliveryEngine": "coupon",
          "xewardsOfferCode": "BZPRPA01",
          "comboPrizes": [

          ],
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "tags": [
            "cultura",
            "cinema"
          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 6.0,
          "price": 7.2,
          "description": "Ingresso de cinema. Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass.",
          "fullForward": false,
          "alliance": {
            "title": "primepass",
            "name": "primepass",
            "xewards": {
              "provider": "2016"
            }
          },
          "__v": 0.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite um ingresso para curtir um filme na rede credenciada Primepass mais perto de você.",
          "praiseTexts": [

          ],
          "weight": 3.0,
          "socialWeight": 0.0,
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ],
          "group": {
            "name": "cinema",
            "title": "Cinema",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ]
        },
        {
          "_id": ObjectId("5adf462568ce661704ca4794"),
          "alliance": {
            "name": "primepass",
            "title": "primepass",
            "xewards": {
              "provider": "null"
            }
          },
          "fullForward": false,
          "name": "primepass-smart",
          "title": "Ingresso de Cinema Smart",
          "subTitle": "Primepass Smart",
          "description": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@2x.png 2x, prize-icon@3x.png 3x"
          },
          "unit": "UN",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "cinema"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZPRPASM",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada Primepass mais perto de você. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "cinema-mensal",
            "title": "Cinema Mensal",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ingresso30",
          "emoji": "🎥",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://primepass.club/resgate"
            }
          ],
          "rules": [
            {
              "name": "ddd",
              "value": [
                11.0,
                12.0,
                15.0,
                16.0,
                17.0,
                18.0,
                19.0,
                21.0,
                27.0,
                31.0,
                34.0,
                35.0,
                41.0,
                43.0,
                47.0,
                48.0,
                51.0,
                53.0,
                54.0,
                61.0,
                62.0,
                63.0,
                65.0,
                67.0,
                68.0,
                69.0,
                71.0,
                77.0,
                79.0,
                81.0,
                82.0,
                83.0,
                84.0,
                85.0,
                86.0,
                91.0,
                92.0,
                94.0,
                95.0,
                96.0,
                97.0,
                98.0
              ]
            }
          ]
        },
        {
          "_id": ObjectId("5aff2721ed86f51b5c9f4846"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-110-de-desconto-em-academia",
          "title": "R$110 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$110,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$110,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 72.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "sercomtel",
            "nextel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR72",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na  mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal110",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5aff26dfed86f519f49f4845"),
          "alliance": {
            "name": "gympass",
            "title": "Gympass",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "r-80-de-desconto-em-academia",
          "title": "R$80 de Desconto em Academia",
          "subTitle": "Cupom de desconto promocional de R$80,00",
          "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$80,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
          "faceValue": 41.0,
          "price": 1.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Coupon",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "academia"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "GYMPPR41",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Yoga, Crossfit ou Musculação? Ganhe desconto na  mensalidade e escolha sua modalidade favorita.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "academia",
            "title": "Academia",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "mensal80",
          "emoji": "💪"
        },
        {
          "_id": ObjectId("5af5eae4c6ca38787718e89a"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-35-00",
          "title": "Desconto R$35 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 35.0,
          "price": 8.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM35",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista35",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5eb46c6ca38391018e89b"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-40-00",
          "title": "Desconto R$40 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 40.0,
          "price": 10.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM40",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista40",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec01422d94ab9ddf482f"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-45-00",
          "title": "Desconto R$45 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 45.0,
          "price": 11.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM45",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista45",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ec92422d948b60df4830"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-50-00",
          "title": "Desconto R$50 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 50.0,
          "price": 12.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM50",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista50",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5edb8422d94b410df4831"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-60-00",
          "title": "Desconto R$60 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 60.0,
          "price": 15.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM60",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista60",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5effe422d943b22df4832"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-70-00",
          "title": "Desconto R$70 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 70.0,
          "price": 17.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM70",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista70",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5f07cc6ca3843e018e89c"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-desconto-de-r-80-00",
          "title": "Desconto R$80 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 80.0,
          "price": 20.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM80",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura trimestral da versão Impressa das revista IstoÉ ou IstoÉ Dinheiro.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista80",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e87f422d94621edf482d"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-10-00",
          "title": "Revista Impressa ou Digital - R$10",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 10.0,
          "price": 2.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM10",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista10",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e8f7c6ca385df418e897"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-15-00",
          "title": "Desconto R$15 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 15.0,
          "price": 3.75,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM15",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista15",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5e94dc6ca389b6818e898"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-20-00",
          "title": "Revista Impressa ou Digital - R$20",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 20.0,
          "price": 5.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM20",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista20",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e9e0422d947d4fdf482e"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-25-00",
          "title": "Desconto R$25 - Assinatura de Revista",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 25.0,
          "price": 6.25,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM25",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa da sua revista favorita da Editora 3.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista25",
          "emoji": "📰"
        },
        {
          "_id": ObjectId("5af5ea60c6ca381a0418e899"),
          "alliance": {
            "name": "editora3",
            "title": "Editora3",
            "xewards": {
              "provider": ""
            }
          },
          "fullForward": false,
          "name": "revistas-impressas-e-digitais-desconto-de-r-30-00",
          "title": "Revista Impressa ou Digital - R$30",
          "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
          "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
          "faceValue": 30.0,
          "price": 7.5,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "revista"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZE3IM30",
          "deliveryEngine": "coupon",
          "deliveryInfo": {
            "created": "Seu desconto estará disponível em breve.",
            "forwarded": "Seu desconto estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse https://bit.ly/2yIZml7 insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Desconto na assinatura digital ou impressa das revistas IstoÉ ou IstoÉ Dinheiro. ",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "revista",
            "title": "Revista",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "revista30",
          "emoji": "📰",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
            }
          ]
        },
        {
          "_id": ObjectId("5af5e419422d943251df4827"),
          "alliance": {
            "name": "ubook",
            "title": "Ubook",
            "xewards": {
              "provider": "2011"
            }
          },
          "fullForward": false,
          "name": "ureader-livros-digitais-30-dias",
          "title": "Livros digitais para ler",
          "subTitle": "Assinatura Ureader de 30 dias",
          "description": "Ficção, romance, biografias e muito mais. Com o aplicativo Ureader, você tem milhares de títulos para manter a leitura em dia.",
          "faceValue": 20.0,
          "price": 6.0,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "oneTimeOnly",
          "customBonuz": {
            "color": "ffffff",
            "backgroundColor": "ffffff"
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "unit": "Assinatura",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [
            "livro-digital"
          ],
          "operators": [
            "vivo",
            "claro",
            "tim",
            "oi",
            "nextel",
            "sercomtel",
            "ctbc"
          ],
          "xewardsOfferCode": "BZURE001",
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Sua recompensa estará disponível em breve.",
            "forwarded": "Sua recompensa estará disponível em breve.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua assinatura Ureader de 30 dias está disponível! Acesse https://seu.bz/BktsVo6 insira o código {{voucher}}, faca o cadastro e aproveite!{{/ifvalue}}"
          },
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "shortDescription": "Ficção, romance, biografias e mais. São vários títulos para manter a leitura em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "group": {
            "name": "livro-digital",
            "title": "Livro Digital",
            "created": ISODate("2018-07-05T10:00:00.000-0300")
          },
          "keyword": "ureader",
          "emoji": "📔",
          "actions": [
            {
              "title": "RESGATAR CUPOM",
              "url": "https://seu.bz/BktsVo6"
            }
          ]
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407250"),
          "alliance": {
            "title": "Oi",
            "name": "oi",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularOi",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para Oi + Internet + SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "oi"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407251"),
          "alliance": {
            "title": "TIM",
            "name": "tim",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularTIM",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para TIM, Internet e SMS",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "",
            "color": ""
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "tim"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("555f7d55fb71969a88407252"),
          "alliance": {
            "title": "Vivo",
            "name": "vivo",
            "xewards": {
              "provider": ""
            }
          },
          "name": "bonusCelularVivo",
          "title": "Crédito em bônus celular",
          "subTitle": "Ligações para telefones Vivo",
          "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
          "faceValue": 1.0,
          "price": 0.3,
          "deliveryCost": 0.0,
          "catalog": true,
          "type": "defaultPrize",
          "customBonuz": {
            "backgroundColor": "f65e01",
            "color": "f65e01"
          },
          "unit": "R$",
          "deliveryAddress": {
            "questions": [

            ]
          },
          "tags": [

          ],
          "operators": [
            "vivo"
          ],
          "xewardsOfferCode": "BZCBC001",
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "deliveryEngine": "xewards",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "fullForward": false,
          "shouldQueryStatus": false,
          "xewardsQuantity": 1.0,
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "_id": ObjectId("58d2c7c84e525a01006670a3"),
          "name": "claro-hibrido",
          "title": "Crédito em bônus celular",
          "subTitle": "Hibrido (Voz e Dados)",
          "faceValue": 1.0,
          "catalog": true,
          "type": "defaultPrize",
          "unit": "R$",
          "deliveryEngine": "xewards",
          "xewardsOfferCode": "BZCBC001",
          "deliveryInfo": {
            "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
            "done": "{{#ifvalue reasonCode value=1}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=2}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=6}}Na maioria das vezes, isto acontece quando o celular é de um plano Pessoa Jurídica ou Corporativo.{{else ifvalue reasonCode value=3}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
          },
          "shouldQueryStatus": false,
          "operators": [
            "claro"
          ],
          "tags": [

          ],
          "deliveryAddress": {
            "questions": [

            ]
          },
          "icon": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png",
            "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
          },
          "customBonuz": {
            "backgroundColor": "ffffff",
            "color": "ffffff"
          },
          "deliveryCost": 0.0,
          "price": 0.3,
          "description": "Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
          "fullForward": false,
          "alliance": {
            "title": "Claro",
            "name": "claro",
            "xewards": {
              "provider": "1"
            }
          },
          "__v": 0.0,
          "comboPrizes": [
            {
              "name": "claro-dados",
              "title": "Claro Dados",
              "subTitle": "Claro Dados",
              "faceValue": 0.0,
              "catalog": true,
              "type": "defaultPrize",
              "unit": "R$",
              "deliveryEngine": "xewards",
              "xewardsOfferCode": "BZHIB001",
              "deliveryInfo": {
                "done": "",
                "forwarded": "",
                "created": ""
              },
              "shouldQueryStatus": false,
              "operators": [
                "claro"
              ],
              "tags": [

              ],
              "deliveryAddress": {
                "questions": [

                ]
              },
              "icon": {
                "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
                "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-icon.png"
              },
              "customBonuz": {
                "backgroundColor": "ffffff",
                "color": "ffffff"
              },
              "deliveryCost": 0.0,
              "price": 0.3,
              "description": "Claro Dados",
              "fullForward": false,
              "alliance": {
                "xewards": {
                  "provider": "1"
                },
                "title": "Claro",
                "name": "claro"
              },
              "comboType": "gift",
              "expressions": [
                {
                  "expression": "value > 0 and value <= 5",
                  "result": 30.0
                },
                {
                  "expression": "value > 5 and value <= 14",
                  "result": 50.0
                },
                {
                  "expression": "value >= 15",
                  "result": 100.0
                }
              ],
              "__v": 0.0
            }
          ],
          "wideBanner": {
            "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-wideBanner.png",
            "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
          },
          "group": {
            "name": "bonus",
            "title": "Bônus Celular",
            "created": ISODate("2018-05-03T14:37:19.079-0300")
          },
          "shortDescription": "Para navegar na internet, usar redes sociais e manter o papo em dia.",
          "praiseTexts": [

          ],
          "weight": 0.0,
          "socialWeight": 0.0,
          "keyword": "bonus",
          "emoji": "📱"
        },
        {
          "name": "desconto-no-picanha-barbecue"
        },
        {
          "name": "filmes-e-series-na-looke-por-30-dias",
          "weight": 5.0
        },
        {
          "name": "troca-facil-ii-entretenimento"
        },
        {
          "name": "troca-facil-ii-cultura-presentear-amigo"
        },
        {
          "name": "troca-facil-ii-cultura-claro"
        },
        {
          "name": "troca-facil-ii-cultura-oi"
        },
        {
          "name": "troca-facil-ii-cultura-tim"
        },
        {
          "name": "troca-facil-ii-cultura-vivo"
        },
        {
          "name": "vale-um-cheddar-m-r-17"
        },
        {
          "name": "assinatura-ubook-10-dias"
        },
        {
          "name": "assinatura-ubook-promocional-15-dias"
        },
        {
          "name": "ingresso-de-cinema-2d-de-2-a-4-primepass"
        },
        {
          "name": "desconto-de-r-80-em-cursos-online"
        }
      ],
      "consumerDataTemplate": [

      ],
      "xewardsOrder": {
        "consumerProfile": "13",
        "projectChannel": "PCBBTF02",
        "upstreamCustomer": "7"
      },
      "video": {
        "href": ""
      },
      "card": {
        "srcset": "",
        "href": ""
      },
      "detailBanner": {
        "srcset": "",
        "href": ""
      },
      "banner": {
        "srcset": "",
        "href": ""
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
      },
      "rewardOffer": {
        "amount": 41.0,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "target": {
        "type": "URI",
        "action": "https://bonuz.com/"
      },
      "tags": [
        "bb-troca-facil-ii"
      ],
      "keyWords": [

      ],
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      },
      "__v": 0.0,
      "recurrent": true
    }
  };

  return experiences[experience];
}