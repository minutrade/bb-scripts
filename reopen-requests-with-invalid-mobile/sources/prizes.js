var getPrize = (function (db) {
  var expsBB = [
    'bb-troca-facil-i',
    'bb-troca-facil-ii',
    'bb-combo-digital',
    'bb-combo-digital-estilo'
  ];

  print('Finding prizes...');

  var prizes = {};
  var bonuz = db.getSiblingDB('bonuz');
  bonuz.experiences.find({
    name: { $in: expsBB }
  }).forEach(function (exp) {
    exp.prizes.forEach(function (prize) {
      var prz = bonuz.prizes.findOne({ name: prize.name });
      prizes[prize.name] = prz;
    })
  })

  print('Prizes set');

  return function getPrize(prize) {
    return prizes[prize];
  }
})(db);