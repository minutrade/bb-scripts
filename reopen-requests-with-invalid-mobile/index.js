load('./lib/consumers.js');
load('./lib/rewards.js');
load('./lib/payments.js');
load('./sources/experiences.js');
load('./sources/configExp.js');
load('./preferences.js');

// var runLimit = 20000;

var businessRequests = db.getSiblingDB('business').getCollection('requests');
var engageTriggers = db.getSiblingDB('engage').getCollection('triggers');
var consumerConsumers = db.getSiblingDB('bonuzConsumer').getCollection('consumers');
var consumerProfiles = db.getSiblingDB('bonuzConsumer').getCollection('profiles');
var bonuzRewards = db.getSiblingDB('bonuz').getCollection('rewards');
var bonuzPayments = db.getSiblingDB('bonuz').getCollection('payments');

function create_UUID() {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}

function write(collection, operations, log) {
  print('performing a bulk operation for ' + log + ' with ' + operations.length + ' itens')
  collection.bulkWrite(operations, { writeConcern: { w: 0 }, ordered: false });
  return [];
}

// definindo variaveis de controle
var limit = {
  'bb-troca-facil-i': 24,
  'bb-troca-facil-ii': 41,
  'bb-combo-digital': 72,
  'bb-combo-digital-estilo': 90
};
var expsBB = [
  'bb-troca-facil-i',
  'bb-troca-facil-ii',
  'bb-combo-digital',
  'bb-combo-digital-estilo'
];

var dateNow = new Date();
var requestNewStatus = { 'name': 'dispatched', 'timestamp': dateNow, 'detail': { 'name': 'success' } };
var requestStatus = { 'name': 'forwarded', 'timestamp': dateNow, 'detail': {} }

var count = 0;

var counters = {
  requests: 0,
  triggers: 0,
  consumers: 0,
  rewards: 0,
  payments: 0,
  profiles: 0
}
var bulkWriteRequests = [];
var bulkWriteTriggers = [];
var bulkWriteConsumers = [];
var bulkWriteRewards = [];
var bulkWritePayments = [];
var bulkWriteProfiles = [];

// identifica todos os requests com status processed-failed do BB por mobile invalido
var requestsCursor = businessRequests.find({
  'newStatus.name': 'processed',
  'newStatus.detail.name': 'failed',
  'experience.name': { $in: expsBB },
  'created': { $gt: ISODate('2018-07-31T00:00:00.000-0300') },
  'errorDescription': 'Número de telefone inválido',
  'actionTypes': 'reward'
// }, { _id: 1, triggerId: 1, batchId: 1, data: 1, 'experience.name': 1 }).limit(runLimit);
}, { _id: 1, triggerId: 1, batchId: 1, data: 1, 'experience.name': 1 }).noCursorTimeout();

requestsCursor.forEach(function (request) {

  print('Atualizando request: ' + request._id.str);

  // definindo variaveis de contexto
  var consumerId = create_UUID().replace(/-/g, '');
  var rewardId = new ObjectId();
  var rewardValue = limit[request.experience.name];
  var experience = getExperience(request.experience.name);

  // atualiza request para status dispatched
  bulkWriteRequests.push({
    updateOne: {
      filter: { _id: request._id }, update: {
        $set: { newStatus: requestNewStatus, status: requestStatus, lastUpdated: dateNow, errorDescription: null, error: null },
        $push: { newTrace: { $each: [requestNewStatus], $position: 0 }, trace: { $each: [requestStatus], $position: 0 } }
      }
    }
  });

  // atualiza trigger para status dispatched
  bulkWriteTriggers.push({
    updateOne: {
      filter: { _id: new ObjectId(request.triggerId) }, update: {
        $set: {
          result: {
            data: {
              actionTypes: ["reward"],
              rewards: [{ id: rewardId.str }],
              rewardValue: rewardValue,
              id: request.triggerId,
              canceled: { value: 0, prizes: [], pricelessPrizesQuantity: 0 },
              delivered: { value: 0, prizes: [], pricelessPrizesQuantity: 0 },
              pricelessPrizes: { quantity: 0, prizes: [] },
              forwardedValue: 0,
              preQualifiedPrizes: []
            }, code: NumberInt(201)
          }, status: requestNewStatus
        },
        $push: { trace: { $each: [requestNewStatus], $position: 0 } }
      }
    }
  });

  // cria o profile e o consumer se necessario
  var existingProfile = consumerProfiles.findOne({
    portal: "bb",
    cpf: request.data.cpf,
    consumerId: { $exists: true }
  });
  if (!existingProfile) {
    consumerProfiles.insertOne({
      "cpf": request.data.cpf,
      "consumerId": consumerId,
      "portal": "bb",
      "keys": ["portal", "cpf", "consumerId"],
      "created": dateNow
    });
    counters.profiles++;
    var consumer = buildConsumer(consumerId, experience.name, experience.sponsor.customBonuz, rewardValue);
    consumerConsumers.insertOne(consumer);
    counters.consumers++;
  } else {
    consumerId = existingProfile.consumerId;
    bulkWriteConsumers.push({
      updateOne: {
        filter: { id: consumerId },
        update: {
          $inc: { totalBonus: rewardValue },
          $addToSet: { tags: experience.name, executedExperiences: { name: experience.name } }
        }
      }
    });
  }

  // cria uma reward para o consumer sem mobile
  var reward = buildReward(rewardId, rewardValue, consumerId, experience, request.batchId, request._id, new ObjectId(request.triggerId));

  // define os prizes elegiveis para o consumer
  var updateReward = getPreferencesUpdate(configExp, reward.experience.name, rewardValue);
  reward.consumerData = updateReward.consumerData;
  reward.chosenDeals = updateReward.chosenDeals;
  reward.offeredDeals = updateReward.offeredDeals;
  reward.eligibleDeals = updateReward.eligibleDeals;
  bulkWriteRewards.push({ insertOne: { document: reward } });

  // cria os payments relacionados a reward
  reward.chosenDeals.forEach(function (deal) {
    var payment = buildPayment(request.data.mobile, reward, deal);
    bulkWritePayments.push({ insertOne: { document: payment } });
    counters.payments++;
  });

  /*
  - validacao da rotina de retryPayments para conciliacao dos casos
  */

  if (bulkWriteConsumers.length >= 1000) {
    bulkWriteConsumers = write(consumerConsumers, bulkWriteConsumers, "consumers");
  }

  if (bulkWritePayments.length >= 1000) {
    bulkWritePayments = write(bonuzPayments, bulkWritePayments, "payments");
  }

  if (bulkWriteProfiles.length >= 1000) {
    bulkWriteProfiles = write(consumerProfiles, bulkWriteProfiles, "profiles");
  }

  count++;
  counters.requests++;
  counters.triggers++;
  counters.rewards++;
  if (count % 1000 === 0) {
    bulkWriteRequests = write(businessRequests, bulkWriteRequests, "requests");
    bulkWriteTriggers = write(engageTriggers, bulkWriteTriggers, "triggers");
    bulkWriteRewards = write(bonuzRewards, bulkWriteRewards, "rewards");
    count = 0;
  }
});

if (count > 0) {
  bulkWriteRequests = write(businessRequests, bulkWriteRequests, "requests");
  bulkWriteTriggers = write(engageTriggers, bulkWriteTriggers, "triggers");
  bulkWriteRewards = write(bonuzRewards, bulkWriteRewards, "rewards");
}

if (bulkWriteConsumers.length > 0) {
  bulkWriteConsumers = write(consumerConsumers, bulkWriteConsumers, "consumers");
}

if (bulkWritePayments.length > 0) {
  bulkWritePayments = write(bonuzPayments, bulkWritePayments, "payments");
}

if (bulkWriteProfiles.length > 0) {
  bulkWriteProfiles = write(consumerProfiles, bulkWriteProfiles, "profiles");
}

requestsCursor.close();
printjson(counters);
