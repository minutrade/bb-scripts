load(path + './sources/prizes.js');

function getPreferencesUpdate(configExp, experience, rewardValue) {
  var configByOperator = configExp.find(function (config) {
    return (config.operators.length === 0 && config.experience === experience);
  });
  var random = Math.floor(Math.random() * configByOperator.prizes.length);
  var digitalRewardsPreferences = configByOperator.prizes[random];
  var updateReward = { consumerData: {}, eligibleDeals: [], chosenDeals: [], offeredDeals: [] };
  var valorTotal = 0;
  if (digitalRewardsPreferences) {
    updateReward.consumerData = {
      digitalRewardsPreferences: digitalRewardsPreferences
    };

    digitalRewardsPreferences.forEach(function (digitalPrefPrize) {
      // var prize = bonuzPrizes.findOne({ name: digitalPrefPrize.name }, { _id: 0 });
      var prize = getPrize(digitalPrefPrize.name);
      valorTotal += prize.faceValue;

      updateReward.eligibleDeals.push(prize);

      updateReward.offeredDeals.push({
        faceValue: NumberInt(prize.faceValue),
        fullForward: prize.fullForward,
        name: prize.name,
        operators: prize.operators,
        type: prize.type,
        unit: prize.unit
      });

      prize.quantity = 1;
      prize.amount = NumberInt(prize.faceValue);
      prize.value = NumberInt(prize.faceValue);
      delete prize.group;
      delete prize.fullForward;
      delete prize.keyword;
      delete prize.subTitle;
      updateReward.chosenDeals.push(prize);
    });

    // verifica se o valor das recompensas passa o da reward e ajusta se necessario
    if (valorTotal > rewardValue && valorTotal <= configByOperator.roundLimit) {
      print('ajustando valor')
      var subtract = rewardValue - configByOperator.roundLimit;
      updateReward.eligibleDeals[0].faceValue += subtract;
      updateReward.offeredDeals[0].faceValue += subtract;
      updateReward.chosenDeals[0].faceValue += subtract;
      updateReward.chosenDeals[0].value += subtract;
      updateReward.chosenDeals[0].amount += subtract;
    }
  }

  return updateReward;
}