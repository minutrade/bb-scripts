db.getSiblingDB("bonuz").getCollection("rewards").count({
  "created": { $gt: ISODate("2018-11-29T00:00:00.000-0200") },
  "isDonation": false,
  "experience.name": { $in: ["bb-troca-facil-i", "bb-troca-facil-ii", "bb-combo-digital", "bb-combo-digital-estilo"] },
  "consumer.mobile": { $exists: false }
});