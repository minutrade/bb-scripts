var bulkOperations = [];
var counter = 0;

db.getSiblingDB("bonuz").getCollection("payments").aggregate(
  [
    {
      "$match": {
        "status.name": "forwarded",
        "status.timestamp": {
          "$gt": ISODate("2018-11-29T00:00:00.000-0200")
        },
        "status.detail.name": "retry",
        "reward.consumer.mobile": {
          "$exists": false
        }
      }
    },
    {
      "$project": {
        "_id": 1.0,
        "rewardId": "$reward.rewardId"
      }
    },
    {
      "$lookup": {
        "from": "rewards",
        "localField": "rewardId",
        "foreignField": "_id",
        "as": "reward"
      }
    },
    {
      "$match": {
        "reward": {
          "$size": 0.0
        }
      }
    }
  ],
  {
    "allowDiskUse": false
  }
).forEach(function (result) {
  bulkOperations.push({
    deleteOne: {
      filter: { _id: result._id }
    }
  });
  counter++;

  if (counter % 1000 === 0) {
    print(counter);
    bulkOperations = [];
  }
});

if (counter > 0) {
  print('final counter: ' + counter);
}