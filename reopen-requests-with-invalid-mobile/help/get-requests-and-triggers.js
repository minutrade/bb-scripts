rs.slaveOk();
var requests = db.getSiblingDB("business").getCollection("requests").findOne({
  'newStatus.name': 'processed',
  'newStatus.detail.name': 'failed',
  'experience.name': {
    $in: [
      'bb-troca-facil-i',
      'bb-troca-facil-ii',
      'bb-combo-digital',
      'bb-combo-digital-estilo'
    ]
  },
  'created': { $gt: ISODate('2018-07-31T00:00:00.000-0300') },
  'errorDescription': 'Número de telefone inválido',
  'actionTypes': 'reward'
})

var triggers = db.getSiblingDB("engage").getCollection("triggers").findOne({ _id: ObjectId(requests.triggerId) });

printjson(requests)
printjson(triggers)