var businessRequests = db.getSiblingDB('business').getCollection('requests');
var engageTriggers = db.getSiblingDB('engage').getCollection('triggers');
var bonuzRewards = db.getSiblingDB('bonuz').getCollection('rewards');
var bonuzPayments = db.getSiblingDB('bonuz').getCollection('payments');




var requestId = ObjectId("5bfef5a3d6db9b5f5b6bb6d5");

var request = businessRequests.findOne({ _id: requestId });
var trigger = engageTriggers.findOne({ _id: ObjectId(request.triggerId) }, { status: 1, "result": 1, created: 1 });
var reward = bonuzRewards.findOne({ _id: new ObjectId(trigger.result.data.rewards[0].id) }, { consumer: 1, value: 1, status: 1 });
var payments = bonuzPayments.find({ "reward.rewardId": ObjectId(trigger.result.data.rewards[0].id) }, { status: 1, "deal.name": 1, "deal.deliveryEngine": 1 }).toArray();

printjson(request);
printjson(trigger);
printjson(reward);
printjson(payments);