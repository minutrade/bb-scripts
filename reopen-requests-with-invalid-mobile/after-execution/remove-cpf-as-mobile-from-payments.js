var operations = [];
var bonuzRewards = db.getSiblingDB("bonuz").getCollection("rewards");
var bonuzPayments = db.getSiblingDB("bonuz").getCollection("payments");

db.getSiblingDB("bonuz").getCollection("rewards").aggregate(
  [
    {
      "$match": {
        "created": {
          // data em que o script de reabertura de pedidos foi executado
          "$gt": ISODate("2018-11-29T00:00:00.000-0200")
        },
        "isDonation": false,
        "experience.name": {
          "$in": [
            "bb-troca-facil-i",
            "bb-troca-facil-ii",
            "bb-combo-digital",
            "bb-combo-digital-estilo"
          ]
        },
        "consumer.mobile": {
          "$exists": false
        }
      }
    },
    {
      "$project": {
        "_id": 0.0,
        "rewardId": "$_id"
      }
    },
    {
      "$lookup": {
        "from": "payments",
        "localField": "rewardId",
        "foreignField": "reward.rewardId",
        "as": "payment"
      }
    },
    {
      "$unwind": {
        "path": "$payment"
      }
    },
    {
      "$project": {
        "rewardId": 1.0,
        "paymentId": "$payment._id"
      }
    }
  ],
  {
    "allowDiskUse": false
  }
).forEach(function (item) {
  operations.push({ updateOne: { filter: { _id: item.paymentId }, update: { $unset: { mobile: "" } } } });
  counter++;
  if (counter % 1000 === 0) {
    bonuzPayments.bulkWrite(operations, { writeConcern: { w: 0 }, ordered: false });
    counter = 0;
    operations = [];
  }
});

if (counter > 0) {
  bonuzPayments.bulkWrite(operations, { writeConcern: { w: 0 }, ordered: false });
}