#!/bin/bash
echo
echo "----------------------"
echo "--Início do processo--"
echo "----------------------"
date

echo
echo "Processando o arquivo OCIF780.txt"
date
awk '(substr($0,1,1) != 0)&&(substr($0,1,1) != 9) {print $0","substr($0,17,11)","substr($0,28,4)}' /data/db-processor/files/OCIF780.txt | mongoimport --host 172.18.34.100 --port 26689 --username caetano.milord --password caetano.milord --authenticationDatabase admin -d bonuzConsumer -c consumer_bb --type csv --columnsHaveTypes --fields "raw.string(),cpf.string(),codClube.string()" --writeConcern {w:0} --quiet

echo
echo "Gerando as recompensas"
date
mongo --host 172.18.34.100 --port 26689 --username caetano.milord --password caetano.milord --authenticationDatabase admin --quiet index_batch.js | mongoimport --host 172.18.34.100 --port 26689 --username caetano.milord --password caetano.milord --authenticationDatabase admin -d bonuz -c rewards_bb --type json --writeConcern {w:0} --numInsertionWorkers 500 --quiet

echo
echo "Recompensas geradas com sucesso"
date

echo
echo "-------------------"
echo "--Fim do processo--"
echo "-------------------"
date
