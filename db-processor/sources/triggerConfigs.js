function getTriggerConfig(experience) {
  var triggerConfigs = {
    "banco-do-brasil-conta-facil": {
      "_id": ObjectId("5a130ea30e085307004ea5dc"),
      "event": {
        "_id": ObjectId("565f2f01abfb0ca0620ab500"),
        "name": "fee-paid"
      },
      "zcript_": {
        "parameters": {
          "action": "Recompensar",
          "condition": {
            "enabled": false,
            "field": "sempre"
          },
          "rewardAmount": {
            "type": "percent",
            "value": 8.3333333,
            "field": "amount"
          },
          "limit": {
            "value": 6500,
            "type": "bonus",
            "period": "monthly",
            "field": "codigoDoCliente"
          }
        },
        "url": "http://delivery-reward-by-event-rules:4169/deliveryRewards"
      },
      "experience": "banco-do-brasil-conta-facil",
      "action": "Recompensar",
      "maskedFields": {
        "mobile": "(99)9?9999-9999"
      },
      "validationSchema": {
        "properties": {
          "name": {
            "description": "Nome do Cliente",
            "type": "string",
            "default": "cliente (Opcional)",
            "minLength": 1,
            "maxLength": 200
          },
          "codigoDoCliente": {
            "description": "Identificador",
            "type": "string",
            "default": "identificator001"
          },
          "cpf": {
            "description": "Cpf",
            "type": "string",
            "default": "CPF (Opcional)"
          },
          "mobile": {
            "description": "Celular",
            "type": "string",
            "default": "31988888888",
            "minLength": 10,
            "maxLength": 15
          },
          "amount": {
            "description": "Valor",
            "type": "number",
            "default": "10"
          }
        },
        "required": [
          "mobile",
          "amount"
        ]
      },
      "sourceType": "business",
      "firstJob": "denyRounding",
      "jobs": {
        "denyRounding": {
          "type": "expression",
          "expressionName": "valueModIsZero",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "divider",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitValue": 6500,
            "limitField": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.divider.data"
            },
            "acceptPartial": true
          },
          "success": "createReward",
          "fail": "done"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        }
      },
      "triggerUniqueKey": [],
      "actionTypes": [
        "reward"
      ]
    },
    "banco-do-brasil-clube-ponto-pra-voce": {
      "_id": ObjectId("5a130ea30e085307004ea5c9"),
      "event": {
        "_id": ObjectId("565f2f01abfb0ca0620ab500"),
        "name": "fee-paid"
      },
      "zcript_": {
        "parameters": {
          "action": "Recompensar",
          "condition": {
            "enabled": false
          },
          "rewardAmount": {
            "type": "percent",
            "value": 8.3333333,
            "field": "amount"
          }
        },
        "url": "http://delivery-reward-by-event-rules:4169/deliveryRewards"
      },
      "experience": "banco-do-brasil-clube-ponto-pra-voce",
      "action": "Recompensar",
      "maskedFields": {
        "mobile": "(99)9?9999-9999"
      },
      "validationSchema": {
        "properties": {
          "name": {
            "description": "Nome do Cliente",
            "type": "string",
            "minLength": 1,
            "maxLength": 200,
            "default": "cliente (Opcional)"
          },
          "codigoDoCliente": {
            "description": "Identificador",
            "type": "string",
            "default": "identificator001"
          },
          "cpf": {
            "description": "Cpf",
            "type": "string",
            "default": "CPF (Opcional)"
          },
          "mobile": {
            "description": "Celular",
            "type": "string",
            "minLength": 10,
            "maxLength": 15,
            "default": "31988888888"
          },
          "amount": {
            "description": "Valor",
            "type": "number",
            "default": "10"
          }
        },
        "required": [
          "mobile",
          "amount"
        ]
      },
      "sourceType": "business",
      "firstJob": "denyRounding",
      "jobs": {
        "denyRounding": {
          "type": "expression",
          "expressionName": "valueModIsZero",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "divider",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitValue": 240,
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.divider.data"
            },
            "acceptPartial": true
          },
          "success": "createReward",
          "fail": "done"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        }
      },
      "actionTypes": [
        "reward"
      ]
    },
    "bb-ourocard-anuidade-bonificada": {
      "_id": ObjectId("5afdddc0f35a5a187db682e8"),
      "event": {
        "_id": ObjectId("565f2f01abfb0ca0620ab500"),
        "name": "fee-paid"
      },
      "experience": "bb-ourocard-anuidade-bonificada",
      "action": "Recompensar",
      "maskedFields": {
        "mobile": "(99)9?9999-9999",
        "ref_month": "99/9999"
      },
      "validationSchema": {
        "properties": {
          "name": {
            "description": "Nome do Cliente",
            "type": "string",
            "default": "cliente (Opcional)",
            "minLength": 1,
            "maxLength": 200
          },
          "codigoDoCliente": {
            "description": "Identificador",
            "type": "string",
            "default": "identificator001"
          },
          "cpf": {
            "description": "Cpf",
            "type": "string",
            "default": "CPF (Opcional)"
          },
          "mobile": {
            "description": "Celular",
            "type": "string",
            "default": "31988888888",
            "minLength": 10,
            "maxLength": 15
          },
          "amount": {
            "description": "Valor",
            "type": "number",
            "default": 10
          }
        },
        "required": [
          "mobile",
          "amount",
          "codigoDoCliente"
        ]
      },
      "sourceType": "business",
      "actionTypes": [
        "reward"
      ],
      "triggerUniqueKey": [],
      "firstJob": "denyRounding",
      "jobs": {
        "denyRounding": {
          "type": "expression",
          "expressionName": "valueModIsZero",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "divider",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "fixed",
              "value": 216
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitValue": 6500,
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.divider.data"
            },
            "acceptPartial": true
          },
          "success": "createReward",
          "fail": "done"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        }
      }
    },
    "bb-combo-digital-estilo": {
      "_id": ObjectId("5addf02cbedc852881cc70c6"),
      "event": {
        "_id": ObjectId("565f2f01abfb0ca0620ab500"),
        "name": "fee-paid"
      },
      "experience": "bb-combo-digital-estilo",
      "action": "Recompensar",
      "maskedFields": {
        "mobile": "(99)9?9999-9999",
        "ref_month": "99/9999"
      },
      "validationSchema": {
        "properties": {
          "name": {
            "description": "Nome do Cliente",
            "type": "string",
            "default": "cliente (Opcional)",
            "minLength": 1,
            "maxLength": 200
          },
          "codigoDoCliente": {
            "description": "Identificador",
            "type": "string",
            "default": "identificator001"
          },
          "cpf": {
            "description": "Cpf",
            "type": "string",
            "default": "CPF (Opcional)"
          },
          "mobile": {
            "description": "Celular",
            "type": "string",
            "default": "31988888888",
            "minLength": 10,
            "maxLength": 15
          },
          "amount": {
            "description": "Valor",
            "type": "number",
            "default": 10
          }
        },
        "required": [
          "mobile",
          "amount"
        ]
      },
      "sourceType": "business",
      "actionTypes": [
        "reward"
      ],
      "triggerUniqueKey": [],
      "firstJob": "validPontos",
      "jobs": {
        "validPontos": {
          "type": "expression",
          "expressionName": "greaterOrEqual",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 1080
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "divider",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "fixed",
              "value": 1080
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitValue": 3,
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "fixed",
              "value": 1
            },
            "acceptPartial": true
          },
          "success": "getProfile",
          "fail": "done"
        },
        "getProfile": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getProfile/",
          "parameters": {
            "keysCompareTo": [
              "experience",
              "mobile"
            ],
            "queryString": [
              "mobile",
              "experience"
            ],
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            }
          },
          "success": "createProfile",
          "fail": "done"
        },
        "createProfile": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createProfile/",
          "parameters": {
            "profileKeys": [
              "portal",
              "cpf",
              "mobile"
            ],
            "portal": {
              "type": "fixed",
              "value": "bb"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "cpf": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "profileFieldsBody": [
              "portal",
              "mobile",
              "cpf"
            ]
          },
          "success": "createReward",
          "fail": "createReward"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "digitalRewardsPreferences": {
              "type": "field",
              "value": "trace.getProfile.data.digitalRewardsPreferences"
            },
            "lastChosenDeals": {
              "type": "field",
              "value": "trace.getProfile.data.lastChosenDeals"
            },
            "consumerDataFields": [
              "digitalRewardsPreferences",
              "lastChosenDeals"
            ],
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.divider.data"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        }
      },
      "account": null
    },
    "bb-combo-digital": {
      "_id": ObjectId("5addedbdbedc851c07cc70c5"),
      "event": {
        "_id": ObjectId("565f2f01abfb0ca0620ab500"),
        "name": "fee-paid"
      },
      "experience": "bb-combo-digital",
      "action": "Recompensar",
      "maskedFields": {
        "mobile": "(99)9?9999-9999",
        "ref_month": "99/9999"
      },
      "validationSchema": {
        "properties": {
          "name": {
            "description": "Nome do Cliente",
            "type": "string",
            "default": "cliente (Opcional)",
            "minLength": 1,
            "maxLength": 200
          },
          "codigoDoCliente": {
            "description": "Identificador",
            "type": "string",
            "default": "identificator001"
          },
          "cpf": {
            "description": "Cpf",
            "type": "string",
            "default": "CPF (Opcional)"
          },
          "mobile": {
            "description": "Celular",
            "type": "string",
            "default": "31988888888",
            "minLength": 10,
            "maxLength": 15
          },
          "amount": {
            "description": "Valor",
            "type": "number",
            "default": 10
          }
        },
        "required": [
          "mobile",
          "amount"
        ]
      },
      "sourceType": "business",
      "actionTypes": [
        "reward"
      ],
      "triggerUniqueKey": [],
      "firstJob": "validPontos",
      "jobs": {
        "validPontos": {
          "type": "expression",
          "expressionName": "greaterOrEqual",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 864
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "divider",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "fixed",
              "value": 864
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitValue": 3,
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "fixed",
              "value": 1
            },
            "acceptPartial": true
          },
          "success": "getProfile",
          "fail": "done"
        },
        "getProfile": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getProfile/",
          "parameters": {
            "keysCompareTo": [
              "experience",
              "mobile"
            ],
            "queryString": [
              "mobile",
              "experience"
            ],
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            }
          },
          "success": "createProfile",
          "fail": "done"
        },
        "createProfile": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createProfile/",
          "parameters": {
            "profileKeys": [
              "portal",
              "cpf",
              "mobile"
            ],
            "portal": {
              "type": "fixed",
              "value": "bb"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "cpf": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "profileFieldsBody": [
              "portal",
              "mobile",
              "cpf"
            ]
          },
          "success": "createReward",
          "fail": "createReward"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "digitalRewardsPreferences": {
              "type": "field",
              "value": "trace.getProfile.data.digitalRewardsPreferences"
            },
            "lastChosenDeals": {
              "type": "field",
              "value": "trace.getProfile.data.lastChosenDeals"
            },
            "consumerDataFields": [
              "digitalRewardsPreferences",
              "lastChosenDeals"
            ],
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.divider.data"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        }
      },
      "account": null
    },
    "bb-troca-facil-i": {
      "_id": ObjectId("5ae0917685296366c5c5cd09"),
      "event": {
        "_id": ObjectId("565f2f01abfb0ca0620ab500"),
        "name": "fee-paid"
      },
      "experience": "bb-troca-facil-i",
      "action": "Recompensar",
      "maskedFields": {
        "mobile": "(99)9?9999-9999",
        "ref_month": "99/9999"
      },
      "validationSchema": {
        "properties": {
          "name": {
            "description": "Nome do Cliente",
            "type": "string",
            "default": "cliente (Opcional)",
            "minLength": 1,
            "maxLength": 200
          },
          "codigoDoCliente": {
            "description": "Identificador",
            "type": "string",
            "default": "identificator001"
          },
          "cpf": {
            "description": "Cpf",
            "type": "string",
            "default": "CPF (Opcional)"
          },
          "mobile": {
            "description": "Celular",
            "type": "string",
            "default": "31988888888",
            "minLength": 10,
            "maxLength": 15
          },
          "amount": {
            "description": "Valor",
            "type": "number",
            "default": 10
          }
        },
        "required": [
          "mobile",
          "amount"
        ]
      },
      "sourceType": "business",
      "actionTypes": [
        "reward"
      ],
      "triggerUniqueKey": [],
      "firstJob": "denyRounding",
      "jobs": {
        "denyRounding": {
          "type": "expression",
          "expressionName": "valueModIsZero",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "getConsumers",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "getConsumers",
          "fail": "done"
        },
        "getConsumers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getConsumer/",
          "parameters": {
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            }
          },
          "success": "hasTagOrizon",
          "fail": "done"
        },
        "hasTagOrizon": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.getConsumers.data.tags"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "offer-orizon"
            }
          },
          "success": "divider",
          "fail": "getOffers"
        },
        "getOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getOffers/",
          "parameters": {
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "id": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            }
          },
          "success": "createDateLessThanExpirationDate",
          "fail": "concatCPFToOrizon"
        },
        "concatCPFToOrizon": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "cpf:"
            }
          },
          "success": "deliveryOrizon",
          "fail": "done"
        },
        "deliveryOrizon": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "rewardPriceless",
            "mobile": {
              "type": "field",
              "value": "trace.getConsumers.data.mobile"
            },
            "prizeName": {
              "type": "fixed",
              "value": "desconto-em-farmacia-90-dias"
            },
            "answer": {
              "type": "field",
              "value": "trace.concatCPFToOrizon.data"
            },
            "answerField": {
              "type": "fixed",
              "value": "cpf"
            },
            "quantity": {
              "type": "fixed",
              "value": "1"
            }
          },
          "success": "calculateExpireDate",
          "fail": "done"
        },
        "createOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createOffers/",
          "parameters": {
            "consumerId": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            },
            "campaign": {
              "type": "fixed",
              "value": "bb-troca-facil-i-degustacao-orizon-90-dias"
            },
            "expirationDate": {
              "type": "field",
              "value": "trace.calculateExpireDate.data"
            },
            "rewardOffer": {
              "type": "fixed",
              "value": "0"
            },
            "cluster": {
              "type": "fixed",
              "value": "bb-troca-facil-i-degustacao-orizon-90-dias"
            },
            "template": {
              "type": "field",
              "value": "trigger.experience"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "actionEvent": {
              "type": "field",
              "value": "trigger.event"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "patchConsumers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/patchConsumer/",
          "actionType": "rewards",
          "parameters": {
            "consumerId": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            },
            "value": {
              "type": "fixed",
              "value": "offer-orizon"
            },
            "path": {
              "type": "fixed",
              "value": "/tags/0"
            },
            "op": {
              "type": "fixed",
              "value": "add"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitValue": 3,
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "fixed",
              "value": 1
            },
            "acceptPartial": true
          },
          "success": "createProfile",
          "fail": "done"
        },
        "createProfile": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createProfile/",
          "parameters": {
            "profileKeys": [
              "portal",
              "cpf",
              "mobile"
            ],
            "portal": {
              "type": "fixed",
              "value": "bb"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "cpf": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "profileFieldsBody": [
              "portal",
              "mobile",
              "cpf"
            ]
          },
          "success": "createReward",
          "fail": "createReward"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.divider.data"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        },
        "createDateLessThanExpirationDate": {
          "type": "expression",
          "expressionName": "dateLessOrEqual",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.created"
            },
            "comparisonValue": {
              "type": "field",
              "value": "trace.getOffers.data.0.expirationDate"
            }
          },
          "success": "divider",
          "fail": "patchConsumers"
        },
        "calculateExpireDate": {
          "type": "expression",
          "expressionName": "addOrSubtractDays",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.created"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 90
            }
          },
          "success": "createOffers",
          "fail": "done"
        }
      },
      "account": null,
      "jobs_": {
        "denyRounding": {
          "type": "expression",
          "expressionName": "valueModIsZero",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "getConsumers",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "getConsumers",
          "fail": "done"
        },
        "getConsumers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getConsumer/",
          "parameters": {
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            }
          },
          "success": "hasTagOrizon",
          "fail": "done"
        },
        "hasTagOrizon": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.getConsumers.data.tags"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "offer-orizon"
            }
          },
          "success": "divider",
          "fail": "getOffers"
        },
        "getOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getOffers/",
          "parameters": {
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "id": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            }
          },
          "success": "divider",
          "fail": "concatCPFToOrizon"
        },
        "concatCPFToOrizon": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "cpf:"
            }
          },
          "success": "deliveryOrizon",
          "fail": "done"
        },
        "deliveryOrizon": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "rewardPriceless",
            "mobile": {
              "type": "field",
              "value": "trace.getConsumers.data.mobile"
            },
            "prizeName": {
              "type": "fixed",
              "value": "desconto-em-farmacia-90-dias"
            },
            "answer": {
              "type": "field",
              "value": "trace.concatCPFToOrizon.data"
            },
            "answerField": {
              "type": "fixed",
              "value": "cpf"
            },
            "quantity": {
              "type": "fixed",
              "value": "1"
            }
          },
          "success": "CalculateExpireDate",
          "fail": "done"
        },
        "CalculateExpireDate": {
          "type": "expression",
          "expressionName": "addOrSubtractDays",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.created"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 90
            }
          },
          "success": "createOffers",
          "fail": "done"
        },
        "createOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createOffers/",
          "parameters": {
            "consumerId": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            },
            "campaign": {
              "type": "fixed",
              "value": "bb-troca-facil-i-degustacao-orizon-90-dias"
            },
            "expirationDate": {
              "type": "field",
              "value": "trace.CalculateExpireDate.data"
            },
            "rewardOffer": {
              "type": "fixed",
              "value": "0"
            },
            "cluster": {
              "type": "fixed",
              "value": "bb-troca-facil-i-degustacao-orizon-90-dias"
            },
            "template": {
              "type": "field",
              "value": "trigger.experience"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "actionEvent": {
              "type": "field",
              "value": "trigger.event"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitValue": 450,
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.divider.data"
            },
            "acceptPartial": true
          },
          "success": "createReward",
          "fail": "done"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.divider.data"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        }
      }
    },
    "bb-troca-facil-ii": {
      "_id": ObjectId("5ae094e2852963e5f2c5cd0e"),
      "event": {
        "_id": ObjectId("565f2f01abfb0ca0620ab500"),
        "name": "fee-paid"
      },
      "experience": "bb-troca-facil-ii",
      "action": "Recompensar",
      "maskedFields": {
        "mobile": "(99)9?9999-9999",
        "ref_month": "99/9999"
      },
      "validationSchema": {
        "properties": {
          "name": {
            "description": "Nome do Cliente",
            "type": "string",
            "default": "cliente (Opcional)",
            "minLength": 1,
            "maxLength": 200
          },
          "codigoDoCliente": {
            "description": "Identificador",
            "type": "string",
            "default": "identificator001"
          },
          "cpf": {
            "description": "Cpf",
            "type": "string",
            "default": "CPF (Opcional)"
          },
          "mobile": {
            "description": "Celular",
            "type": "string",
            "default": "31988888888",
            "minLength": 10,
            "maxLength": 15
          },
          "amount": {
            "description": "Valor",
            "type": "number",
            "default": 10
          }
        },
        "required": [
          "mobile",
          "amount"
        ]
      },
      "sourceType": "business",
      "actionTypes": [
        "reward"
      ],
      "triggerUniqueKey": [],
      "firstJob": "validPontos",
      "jobs": {
        "validPontos": {
          "type": "expression",
          "expressionName": "greaterOrEqual",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 360
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "getConsumers",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "getConsumers",
          "fail": "done"
        },
        "getConsumers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getConsumer/",
          "parameters": {
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            }
          },
          "success": "hasTagOrizon",
          "fail": "done"
        },
        "hasTagOrizon": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.getConsumers.data.tags"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "offer-orizon"
            }
          },
          "success": "divider",
          "fail": "getOffers"
        },
        "getOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getOffers/",
          "parameters": {
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "id": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            }
          },
          "success": "createDateLessThanExpirationDate",
          "fail": "concatCPFToOrizon"
        },
        "concatCPFToOrizon": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "cpf:"
            }
          },
          "success": "deliveryOrizon",
          "fail": "done"
        },
        "deliveryOrizon": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "rewardPriceless",
            "mobile": {
              "type": "field",
              "value": "trace.getConsumers.data.mobile"
            },
            "prizeName": {
              "type": "fixed",
              "value": "desconto-em-farmacia-180-dias"
            },
            "answer": {
              "type": "field",
              "value": "trace.concatCPFToOrizon.data"
            },
            "answerField": {
              "type": "fixed",
              "value": "cpf"
            },
            "quantity": {
              "type": "fixed",
              "value": "1"
            }
          },
          "success": "calculateExpireDate",
          "fail": "done"
        },
        "createOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createOffers/",
          "parameters": {
            "consumerId": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            },
            "campaign": {
              "type": "fixed",
              "value": "bb-troca-facil-ii-degustacao-orizon-180-dias"
            },
            "expirationDate": {
              "type": "field",
              "value": "trace.calculateExpireDate.data"
            },
            "rewardOffer": {
              "type": "fixed",
              "value": "0"
            },
            "cluster": {
              "type": "fixed",
              "value": "bb-troca-facil-ii-degustacao-orizon-180-dias"
            },
            "template": {
              "type": "field",
              "value": "trigger.experience"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "actionEvent": {
              "type": "field",
              "value": "trigger.event"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "fixed",
              "value": 492
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "patchConsumers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/patchConsumer/",
          "actionType": "rewards",
          "parameters": {
            "consumerId": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            },
            "value": {
              "type": "fixed",
              "value": "offer-orizon"
            },
            "path": {
              "type": "fixed",
              "value": "/tags/0"
            },
            "op": {
              "type": "fixed",
              "value": "add"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitValue": 3,
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "fixed",
              "value": 1
            },
            "acceptPartial": true
          },
          "success": "createProfile",
          "fail": "done"
        },
        "createProfile": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createProfile/",
          "parameters": {
            "profileKeys": [
              "portal",
              "cpf",
              "mobile"
            ],
            "portal": {
              "type": "fixed",
              "value": "bb"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "cpf": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "profileFieldsBody": [
              "portal",
              "mobile",
              "cpf"
            ]
          },
          "success": "createReward",
          "fail": "createReward"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.divider.data"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "rewards",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        },
        "createDateLessThanExpirationDate": {
          "type": "expression",
          "expressionName": "dateLessOrEqual",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.created"
            },
            "comparisonValue": {
              "type": "field",
              "value": "trace.getOffers.data.0.expirationDate"
            }
          },
          "success": "divider",
          "fail": "patchConsumers"
        },
        "calculateExpireDate": {
          "type": "expression",
          "expressionName": "addOrSubtractDays",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.created"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 180
            }
          },
          "success": "createOffers",
          "fail": "done"
        }
      },
      "account": null,
      "jobs_": {
        "validPontos": {
          "type": "expression",
          "expressionName": "greaterOrEqual",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.amount"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 360
            }
          },
          "success": "replaceMobile",
          "fail": "done"
        },
        "replaceMobile": {
          "type": "expression",
          "expressionName": "removeMask",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.mobile"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "[^0-9]"
            }
          },
          "success": "mobilePattern",
          "fail": "done"
        },
        "mobilePattern": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "(^055|^55)([0-9]{10,11}$)"
            }
          },
          "success": "getConsumers",
          "fail": "concat55"
        },
        "concat55": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.replaceMobile.data"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "55"
            }
          },
          "success": "getConsumers",
          "fail": "done"
        },
        "getConsumers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getConsumer/",
          "parameters": {
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            }
          },
          "success": "hasTagOrizon",
          "fail": "done"
        },
        "hasTagOrizon": {
          "type": "expression",
          "expressionName": "isIn",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trace.getConsumers.data.tags"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "offer-orizon"
            }
          },
          "success": "divider",
          "fail": "getOffers"
        },
        "getOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/getOffers/",
          "parameters": {
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "id": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            }
          },
          "success": "divider",
          "fail": "concatCPFToOrizon"
        },
        "concatCPFToOrizon": {
          "type": "expression",
          "expressionName": "concatStringReverse",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.data.cpf"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": "cpf:"
            }
          },
          "success": "deliveryOrizon",
          "fail": "done"
        },
        "deliveryOrizon": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "rewardPriceless",
            "mobile": {
              "type": "field",
              "value": "trace.getConsumers.data.mobile"
            },
            "prizeName": {
              "type": "fixed",
              "value": "desconto-em-farmacia-180-dias"
            },
            "answer": {
              "type": "field",
              "value": "trace.concatCPFToOrizon.data"
            },
            "answerField": {
              "type": "fixed",
              "value": "cpf"
            },
            "quantity": {
              "type": "fixed",
              "value": "1"
            }
          },
          "success": "CalculateExpireDate",
          "fail": "done"
        },
        "CalculateExpireDate": {
          "type": "expression",
          "expressionName": "addOrSubtractDays",
          "parameters": {
            "fieldValue": {
              "type": "field",
              "value": "trigger.created"
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 180
            }
          },
          "success": "createOffers",
          "fail": "done"
        },
        "createOffers": {
          "type": "service",
          "url": "http://engage-lineup-consumer:4297/createOffers/",
          "parameters": {
            "consumerId": {
              "type": "field",
              "value": "trace.getConsumers.data.id"
            },
            "campaign": {
              "type": "fixed",
              "value": "bb-troca-facil-i-degustacao-orizon-180-dias"
            },
            "expirationDate": {
              "type": "field",
              "value": "trace.CalculateExpireDate.data"
            },
            "rewardOffer": {
              "type": "fixed",
              "value": "0"
            },
            "cluster": {
              "type": "fixed",
              "value": "bb-troca-facil-i-degustacao-orizon-180-dias"
            },
            "template": {
              "type": "field",
              "value": "trigger.experience"
            },
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "experience": {
              "type": "field",
              "value": "trigger.experience"
            },
            "actionEvent": {
              "type": "field",
              "value": "trigger.event"
            }
          },
          "success": "divider",
          "fail": "done"
        },
        "divider": {
          "type": "expression",
          "expressionName": "divider",
          "parameters": {
            "fieldValue": {
              "type": "fixed",
              "value": 492
            },
            "comparisonValue": {
              "type": "fixed",
              "value": 12
            }
          },
          "success": "validLimit",
          "fail": "done"
        },
        "validLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitValue": 450,
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.divider.data"
            },
            "acceptPartial": true
          },
          "success": "createReward",
          "fail": "done"
        },
        "createReward": {
          "type": "service",
          "url": "http://engage-lineup-reward:4298/createReward/",
          "actionType": "rewards",
          "parameters": {
            "rewardType": "reward",
            "mobile": {
              "type": "field",
              "value": "trace.concat55.data|trace.replaceMobile.data"
            },
            "value": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            }
          },
          "success": "done",
          "fail": "rollbackLimit"
        },
        "rollbackLimit": {
          "type": "service",
          "url": "http://engage-lineup-limit:4296/verifyLimit/",
          "parameters": {
            "limitType": "bonus",
            "limitPeriod": "monthly",
            "limitField": {
              "type": "field",
              "value": "trigger.data.codigoDoCliente"
            },
            "valueReward": {
              "type": "field",
              "value": "trace.validLimit.data.value"
            },
            "rollback": true
          },
          "success": "done",
          "fail": "done"
        }
      }
    }
  }

  return triggerConfigs[experience]
}