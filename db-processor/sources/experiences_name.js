function getExperience(experience) {
  var experiences = {
    "banco-do-brasil-clube-ponto-pra-voce": {
      "name": "banco-do-brasil-clube-ponto-pra-voce",
      "title": "Clube Ponto Pra Você"
    },
    "banco-do-brasil-conta-facil": {
      "name": "banco-do-brasil-conta-facil",
      "title": "Conta Fácil"
    },
    "bb-combo-digital": {
      "name": "bb-combo-digital",
      "title": "Combo Digital"
    },
    "bb-combo-digital-estilo": {
      "name": "bb-combo-digital-estilo",
      "title": "Combo Digital Estilo"
    },
    "bb-ourocard-anuidade-bonificada": {
      "name": "bb-ourocard-anuidade-bonificada",
      "title": "Ourocard Anuidade Bonificada"
    },
    "bb-troca-facil-i": {
      "name": "bb-troca-facil-i",
      "title": "Combo Troca Fácil I"
    },
    "bb-troca-facil-ii": {
      "name": "bb-troca-facil-ii",
      "title": "Combo Troca Fácil II"
    }
  };

  return experiences[experience];
}
