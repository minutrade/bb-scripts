function getExperience(experience) {
  var experiences = {
    "banco-do-brasil-clube-ponto-pra-voce": {
      "_id": ObjectId("594822d6fe80471d7201cf7b"),
      "name": "banco-do-brasil-clube-ponto-pra-voce",
      "title": "Clube Ponto Pra Você",
      "officialPortal": "bb",
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/banco-do-brasil-clube-ponto-pra-voce/appreciation.png"
      },
      "templates": [
        {
          "events": [],
          "type": "choice"
        },
        {
          "events": [
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-claro-delivered",
              "name": "delivered",
              "prize": "bonusCelularClaro"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-claro-canceled",
              "name": "canceled",
              "prize": "bonusCelularClaro"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-oi-delivered",
              "name": "delivered",
              "prize": "bonusCelularOi"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-oi-canceled",
              "name": "canceled",
              "prize": "bonusCelularOi"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-tim-delivered",
              "name": "delivered",
              "prize": "bonusCelularTIM"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-tim-canceled",
              "name": "canceled",
              "prize": "bonusCelularTIM"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-vivo-delivered",
              "name": "delivered",
              "prize": "bonusCelularVivo"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-bonus-celular-vivo-canceled",
              "name": "canceled",
              "prize": "bonusCelularVivo"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-assinatura-app-delivered",
              "name": "delivered",
              "prize": "assinatura-app"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-assinatura-playkids-promocional-30-dias-delivered",
              "name": "delivered",
              "prize": "assinatura-playkids-promocional-30-dias"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-assinatura-ubook-delivered",
              "name": "delivered",
              "prize": "assinatura-ubook"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-assinatura-ubook-promocional-30-dias-delivered",
              "name": "delivered",
              "prize": "assinatura-ubook-promocional-30-dias"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-ipass-internacional-mes-vigente-delivered",
              "name": "delivered",
              "prize": "ipass-internacional-mes-vigente"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-ipass-nacional-mes-vigente-delivered",
              "name": "delivered",
              "prize": "ipass-nacional-mes-vigente"
            },
            {
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-presentear-amigo-delivered",
              "name": "delivered",
              "prize": "presentear-amigo"
            },
            {
              "templateName": "gympass-diaria-r-20-delivered",
              "name": "delivered",
              "prize": "gympass-diaria-r-20"
            },
            {
              "templateName": "gympass-mensal-r-30-delivered",
              "name": "delivered",
              "prize": "gympass-mensal-r-30"
            },
            {
              "templateName": "dentro-da-historia-r-20-00-delivered",
              "name": "delivered",
              "prize": "dentro-da-historia-r-20-00"
            }
          ],
          "type": "delivered"
        },
        {
          "type": "information",
          "events": [
            {
              "prize": "",
              "name": "appreciation",
              "templateName": "banco-do-brasil-clube-ponto-pra-voce-appreciation-default"
            }
          ]
        }
      ],
      "xewardsOrder": {
        "projectChannel": "5",
        "consumerProfile": "3",
        "upstreamCustomer": "7"
      },
      "listIcon": {
        "srcset": "list-icon.png 1x, list-icon@1.5x.png 1.5x, list-icon@2x.png 2x, list-icon@3x.png 3x, list-icon@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/banco-do-brasil-clube-ponto-pra-voce/list-icon.png"
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/banco-do-brasil-clube-ponto-pra-voce/icon.png"
      },
      "rewardOffer": {
        "prefix": "Ganhe até",
        "suffix": "em bônus",
        "currency": "BRL",
        "amount": 40
      },
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3-us-west-2.amazonaws.com/assets-dev.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3-us-west-2.amazonaws.com/assets-dev.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3-us-west-2.amazonaws.com/assets-dev.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB"
        }
      },
      "fallback": {
        "expirationHours": 48,
        "enabled": true
      },
      "countryCode": "BR"
    },
    "banco-do-brasil-conta-facil": {
      "_id": ObjectId("58ef6109374fbe01009314c6"),
      "name": "banco-do-brasil-conta-facil",
      "title": "Conta Fácil",
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/banco-do-brasil-conta-facil/appreciation.png"
      },
      "templates": [
        {
          "type": "choice",
          "events": []
        },
        {
          "type": "delivered",
          "events": [
            {
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-claro-delivered"
            },
            {
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-claro-canceled"
            },
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "canceled",
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-oi-canceled"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "canceled",
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-tim-canceled"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-vivo-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "canceled",
              "templateName": "banco-do-brasil-conta-facil-bonus-celular-vivo-canceled"
            },
            {
              "templateName": "banco-do-brasil-conta-facil-assinatura-app-delivered"
            },
            {
              "templateName": "banco-do-brasil-conta-facil-assinatura-ubook-delivered"
            },
            {
              "templateName": "gympass-diaria-1-delivered"
            },
            {
              "templateName": "dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "banco-do-brasil-conta-facil-presentear-amigo-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": [
            {
              "prize": "",
              "name": "appreciation",
              "templateName": "banco-do-brasil-conta-facil-appreciation-default"
            }
          ]
        }
      ],
      "xewardsOrder": {
        "projectChannel": "PCBBCTAF",
        "consumerProfile": "10",
        "upstreamCustomer": "7"
      },
      "listIcon": {
        "srcset": "list-icon.png 1x, list-icon@1.5x.png 1.5x, list-icon@2x.png 2x, list-icon@3x.png 3x, list-icon@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/banco-do-brasil-conta-facil/list-icon.png"
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/banco-do-brasil-conta-facil/icon.png"
      },
      "rewardOffer": {
        "prefix": "Ganhe",
        "suffix": "em bônus",
        "currency": "BRL",
        "amount": 10
      },
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        },
        "tags": [
          "banco"
        ],
        "logo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "listLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        }
      },
      "fallback": {
        "expirationHours": 48,
        "enabled": true
      },
      "countryCode": "BR",
      "officialPortal": "bb"
    },
    "bb-combo-digital": {
      "_id": ObjectId("5af49d09c2b8ca01006f22f0"),
      "name": "bb-combo-digital",
      "title": "Combo Digital",
      "officialPortal": "bb",
      "countryCode": "BR",
      "fallback": {
        "expirationHours": 48,
        "enabled": true
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-combo-digital/appreciation.png"
      },
      "templates": [
        {
          "type": "choice",
          "events": [
            {
              "prize": "",
              "name": "donation",
              "templateName": "bb-combo-digital-choice-default"
            }
          ]
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-combo-digital-presentear-amigo-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-combo-digital-primepass-oneshot-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-combo-digital-primepass-duo-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-combo-digital-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-combo-digital-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-combo-digital-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-combo-digital-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-combo-digital-claro-hibrido-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-combo-digital-assinatura-ubook-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-12-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-7e50-reais-em-12-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-cabify-desconto-7e50-reais-em-8-corridas-delivered"
            },
            {
              "prize": "dentro-da-historia-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-dentro-da-historia-r-30-00-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-em-frete-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-combo-digital-desconto-farmacia-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-combo-digital-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-combo-digital-primepass-smart-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": []
        }
      ],
      "xewardsOrder": {
        "consumerProfile": "14",
        "projectChannel": "PCBBCODI",
        "upstreamCustomer": "7"
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-combo-digital/icon.png"
      },
      "rewardOffer": {
        "amount": 72,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      }
    },
    "bb-combo-digital-estilo": {
      "_id": ObjectId("5af49d365dccbe01008be667"),
      "name": "bb-combo-digital-estilo",
      "title": "Combo Digital Estilo",
      "officialPortal": "bb",
      "countryCode": "BR",
      "fallback": {
        "expirationHours": 48,
        "enabled": true
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-combo-digital-estilo/appreciation.png"
      },
      "templates": [
        {
          "type": "choice",
          "events": []
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-presentear-amigo-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-primepass-smart-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-primepass-oneshot-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-primepass-duo-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-7e50-reais-em-8-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-12-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-7e50-reais-em-12-corridas-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-claro-hibrido-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-20-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-dentro-da-historia-r-20-00-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-assinatura-ubook-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-20-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-20-00-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "dentro-da-historia-r-30-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-dentro-da-historia-r-30-00-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-em-frete-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-desconto-farmacia-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-combo-digital-estilo-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": []
        }
      ],
      "xewardsOrder": {
        "consumerProfile": "15",
        "projectChannel": "PCBBCMES",
        "upstreamCustomer": "7"
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-combo-digital-estilo/icon.png"
      },
      "rewardOffer": {
        "amount": 90,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      }
    },
    "bb-ourocard-anuidade-bonificada": {
      "_id": ObjectId("5a959b6ca1ca900100bb513f"),
      "name": "bb-ourocard-anuidade-bonificada",
      "title": "Ourocard Anuidade Bonificada",
      "officialPortal": "bb",
      "countryCode": "BR",
      "fallback": {
        "expirationHours": 48,
        "enabled": true
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-ourocard-anuidade-bonificada/appreciation.png"
      },
      "templates": [
        {
          "type": "choice",
          "events": []
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-ourocard-anuidade-bonificada-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-bonus-celular-oi-canceled"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-ourocard-anuidade-bonificada-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-bonus-celular-tim-canceled"
            },
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-ourocard-anuidade-bonificada-presentear-amigo-delivered"
            },
            {
              "prize": "presentearAmigo",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-presentear-amigo-canceled"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-ourocard-anuidade-bonificada-bonus-celular-vivo-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-bonus-celular-vivo-canceled"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-ourocard-anuidade-bonificada-claro-hibrido-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-claro-hibrido-canceled"
            },
            {
              "prize": "gympass-diaria-2",
              "name": "delivered",
              "templateName": "bb-ourocard-anuidade-bonificada-gympass-diaria-2-delivered"
            },
            {
              "prize": "gympass-diaria-2",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-gympass-diaria-2-canceled"
            },
            {
              "prize": "assinatura-ubook-promocional-15-dias",
              "name": "delivered",
              "templateName": "bb-ourocard-anuidade-bonificada-assinatura-ubook-promocional-15-dias-delivered"
            },
            {
              "prize": "assinatura-ubook-promocional-15-dias",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-assinatura-ubook-promocional-15-dias-canceled"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "canceled",
              "templateName": "bb-ourocard-anuidade-bonificada-ipass-nacional-mes-vigente-canceled"
            }
          ]
        },
        {
          "type": "information",
          "events": [
            {
              "prize": "",
              "name": "appreciation",
              "templateName": "bb-ourocard-anuidade-bonificada-appreciation-default"
            }
          ]
        }
      ],
      "xewardsOrder": {
        "consumerProfile": "11",
        "projectChannel": "PCBBOCUN",
        "upstreamCustomer": "7"
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-ourocard-anuidade-bonificada/icon.png"
      },
      "rewardOffer": {
        "amount": 18,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em bônus"
      },
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      }
    },
    "bb-troca-facil-i": {
      "_id": ObjectId("5af49cccc2b8ca01006f22e2"),
      "name": "bb-troca-facil-i",
      "title": "Combo Troca Fácil I",
      "officialPortal": "bb",
      "countryCode": "BR",
      "fallback": {
        "expirationHours": 48,
        "enabled": true
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-troca-facil-i/appreciation.png"
      },
      "templates": [
        {
          "type": "choice",
          "events": []
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-bonus-celular-tim-delivered"
            },
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-presentear-amigo-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-claro-hibrido-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-primepass-oneshot-delivered"
            },
            {
              "prize": "gympass-diaria-2",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-gympass-diaria-2-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-20-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "assinatura-ubook-promocional-15-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-assinatura-ubook-promocional-15-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "dentro-da-historia-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-dentro-da-historia-r-20-00-delivered"
            },
            {
              "prize": "gympass-diaria-1",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-gympass-diaria-1-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-farmacia-delivered"
            },
            {
              "prize": "desconto-em-farmacia-90-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-em-farmacia-90-dias-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-assinatura-ubook-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-em-frete-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-primepass-duo-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-primepass-smart-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": []
        }
      ],
      "xewardsOrder": {
        "consumerProfile": "12",
        "projectChannel": "PCBBTF01",
        "upstreamCustomer": "7"
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-troca-facil-i/icon.png"
      },
      "rewardOffer": {
        "amount": 24,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      }
    },
    "bb-troca-facil-ii": {
      "_id": ObjectId("5b2d4431c1692e3395a5c010"),
      "name": "bb-troca-facil-ii",
      "title": "Combo Troca Fácil II",
      "officialPortal": "bb",
      "countryCode": "BR",
      "fallback": {
        "enabled": true,
        "expirationHours": 360
      },
      "appreciationBanner": {
        "srcset": "appreciation.png 1x, appreciation@1.5x.png 1.5x, appreciation@2x.png 2x, appreciation@3x.png 3x, appreciation@4x.png 4x",
        "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/experiences/bb-troca-facil-ii/appreciation.png"
      },
      "templates": [
        {
          "type": "choice",
          "events": [
            {
              "prize": "",
              "name": "default",
              "templateName": "bb-troca-facil-ii-choice-default"
            }
          ]
        },
        {
          "type": "delivered",
          "events": [
            {
              "prize": "dentro-da-historia-r-30-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-dentro-da-historia-r-30-00-delivered"
            },
            {
              "prize": "casquinha-mcdonalds",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-casquinha-mcdonalds-delivered"
            },
            {
              "prize": "primepass-oneshot",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-primepass-oneshot-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-30-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-30-00-delivered"
            },
            {
              "prize": "assinatura-ubook",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-assinatura-ubook-delivered"
            },
            {
              "prize": "dentro-da-historia-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-dentro-da-historia-r-20-00-delivered"
            },
            {
              "prize": "dentro-da-historia-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-dentro-da-historia-r-10-00-delivered"
            },
            {
              "prize": "ureader-livros-digitais-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-u-reader-livros-digitais-30-dias-delivered"
            },
            {
              "prize": "audiolivro-infantis-30-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-audiolivro-infantis-30-dias-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-4-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-5-reais-em-4-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-6-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-5-reais-em-6-corridas-delivered"
            },
            {
              "prize": "desconto-farmacia",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-farmacia-delivered"
            },
            {
              "prize": "desconto-em-farmacia-180-dias",
              "name": "delivered",
              "templateName": "bb-troca-facil-i-desconto-em-farmacia-180-dias-delivered"
            },
            {
              "prize": "presentearAmigo",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-presentear-amigo-delivered"
            },
            {
              "prize": "bonusCelularOi",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-bonus-celular-oi-delivered"
            },
            {
              "prize": "bonusCelularTIM",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-bonus-celular-tim-delivered"
            },
            {
              "prize": "bonusCelularVivo",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-bonus-celular-vivo-delivered"
            },
            {
              "prize": "claro-hibrido",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-claro-hibrido-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-20-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-20-00-delivered"
            },
            {
              "prize": "cabify-desconto-5-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-5-reais-em-8-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-12-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-7e50-reais-em-12-corridas-delivered"
            },
            {
              "prize": "cabify-desconto-7e50-reais-em-8-corridas",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-cabify-desconto-7e50-reais-em-8-corridas-delivered"
            },
            {
              "prize": "desconto-de-r-100-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-de-r-100-00-em-frete-delivered"
            },
            {
              "prize": "desconto-de-r-70-00-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-de-r-70-00-em-frete-delivered"
            },
            {
              "prize": "desconto-em-frete",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-desconto-em-frete-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-10",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-10-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-20",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-20-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-30-delivered"
            },
            {
              "prize": "flores-com-desconto-de-r-40",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-flores-com-desconto-de-r-40-delivered"
            },
            {
              "prize": "gympass-mensal-r-30",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-gympass-mensal-r-30-delivered"
            },
            {
              "prize": "ipass-internacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-ipass-internacional-mes-vigente-delivered"
            },
            {
              "prize": "ipass-nacional-mes-vigente",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-ipass-nacional-mes-vigente-delivered"
            },
            {
              "prize": "primepass-duo",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-primepass-duo-delivered"
            },
            {
              "prize": "primepass-smart",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-primepass-smart-delivered"
            },
            {
              "prize": "r-110-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-r-110-de-desconto-em-academia-delivered"
            },
            {
              "prize": "r-80-de-desconto-em-academia",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-r-80-de-desconto-em-academia-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-35-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-35-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-40-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-40-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-45-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-45-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-50-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-50-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-60-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-60-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-70-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-70-00-delivered"
            },
            {
              "prize": "revistas-impressas-desconto-de-r-80-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-desconto-de-r-80-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-10-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-10-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-15-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-15-00-delivered"
            },
            {
              "prize": "revistas-impressas-e-digitais-desconto-de-r-25-00",
              "name": "delivered",
              "templateName": "bb-troca-facil-ii-revistas-impressas-e-digitais-desconto-de-r-25-00-delivered"
            }
          ]
        },
        {
          "type": "information",
          "events": []
        }
      ],
      "xewardsOrder": {
        "consumerProfile": "13",
        "projectChannel": "PCBBTF02",
        "upstreamCustomer": "7"
      },
      "listIcon": {
        "srcset": "",
        "href": ""
      },
      "icon": {
        "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
        "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
      },
      "rewardOffer": {
        "amount": 41,
        "currency": "BRL",
        "prefix": "Ganhe",
        "suffix": "em recompensas"
      },
      "sponsor": {
        "name": "bb",
        "title": "Banco do Brasil",
        "listLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "cardLogo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "logo": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/sponsors/bb/logo.png",
          "srcset": "logo.png 1x, logo@2x.png 2x, logo@3x.png 3x"
        },
        "tags": [
          "banco"
        ],
        "customBonuz": {
          "color": "0056AB",
          "backgroundColor": "0056AB",
          "useImage": true
        }
      }
    }
  }

  return experiences[experience];
}