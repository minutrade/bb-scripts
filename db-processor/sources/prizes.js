var sourcePrizes = {
  "claro-hibrido": {
    "_id": ObjectId("5931663c9ab0c60fe506b7cf"),
    "name": "claro-hibrido",
    "title": "Bônus Celular",
    "subTitle": "Hibrido (Voz e Dados)",
    "faceValue": 1.0,
    "catalog": true,
    "type": "defaultPrize",
    "unit": "R$",
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "BZCBC001",
    "deliveryInfo": {
      "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "done": "{{#ifvalue reasonCode value=1}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=2}}Na maioria das vezes, isto acontece quando o celular não está ativo na operadora.{{else ifvalue reasonCode value=6}}Na maioria das vezes, isto acontece quando o celular é de um plano Pessoa Jurídica ou Corporativo.{{else ifvalue reasonCode value=3}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "claro"
    ],
    "tags": [
      "bonus"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_logo_lists_small_claro.png",
      "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x"
    },
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 0.3,
    "description": "Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
    "fullForward": false,
    "alliance": {
      "name": "claro",
      "title": "Claro",
      "xewards": {
        "provider": "1"
      }
    },
    "__v": 0.0,
    "comboPrizes": [
      {
        "name": "claro-dados",
        "title": "Claro Dados",
        "subTitle": "Claro Dados",
        "faceValue": 1.0,
        "catalog": true,
        "type": "defaultPrize",
        "unit": "R$",
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZHIB001",
        "deliveryInfo": {
          "done": "",
          "forwarded": "",
          "created": ""
        },
        "shouldQueryStatus": false,
        "operators": [
          "claro"
        ],
        "tags": [

        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_logo_lists_small_claro.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.3,
        "description": "Claro Dados",
        "fullForward": false,
        "alliance": {
          "xewards": {
            "provider": "1"
          },
          "title": "Claro",
          "name": "claro"
        },
        "comboType": "gift",
        "expressions": [
          {
            "expression": "value > 0 and value <= 5",
            "result": 30.0
          },
          {
            "expression": "value > 5 and value <= 14",
            "result": 50.0
          },
          {
            "expression": "value >= 15",
            "result": 100.0
          }
        ],
        "__v": 0.0
      }
    ],
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/claro-hibrido/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "bonus",
      "title": "Bônus Celular",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "bonus",
    "emoji": "📱"
  },
  "bonusCelularOi": {
    "_id": ObjectId("555f7d55fb71969a88407250"),
    "alliance": {
      "name": "oi",
      "title": "Oi",
      "xewards": {
        "provider": "4"
      }
    },
    "name": "bonusCelularOi",
    "title": "Bônus Celular",
    "subTitle": "Ligações para Oi + Internet + SMS",
    "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
    "faceValue": 1.0,
    "price": 0.3,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "defaultPrize",
    "customBonuz": {
      "color": "",
      "backgroundColor": ""
    },
    "unit": "R$",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "bonus"
    ],
    "operators": [
      "oi"
    ],
    "xewardsOfferCode": "BZCBC001",
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_logo_lists_small_oi.png",
      "srcset": "img_logo_lists_small_oi.png 1x,img_logo_lists_small_oi@2x.png 2x,img_logo_lists_small_oi@3x.png 3x"
    },
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
    },
    "fullForward": false,
    "shouldQueryStatus": false,
    "xewardsQuantity": 1.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularOi/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "bonus",
      "title": "Bônus Celular",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "bonus",
    "emoji": "📱"
  },
  "bonusCelularTIM": {
    "_id": ObjectId("555f7d55fb71969a88407251"),
    "alliance": {
      "name": "tim",
      "title": "TIM",
      "xewards": {
        "provider": "2"
      }
    },
    "name": "bonusCelularTIM",
    "title": "Bônus Celular",
    "subTitle": "Ligações para TIM, Internet e SMS",
    "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
    "faceValue": 1.0,
    "price": 0.3,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "defaultPrize",
    "customBonuz": {
      "color": "",
      "backgroundColor": ""
    },
    "unit": "R$",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "bonus"
    ],
    "operators": [
      "tim"
    ],
    "xewardsOfferCode": "BZCBC001",
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_logo_lists_small_tim.png",
      "srcset": "img_logo_lists_small_tim.png 1x,img_logo_lists_small_tim@2x.png 2x,img_logo_lists_small_tim@3x.png 3x"
    },
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
    },
    "fullForward": false,
    "shouldQueryStatus": false,
    "xewardsQuantity": 1.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularTIM/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "bonus",
      "title": "Bônus Celular",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "bonus",
    "emoji": "📱"
  },
  "bonusCelularVivo": {
    "_id": ObjectId("555f7d55fb71969a88407252"),
    "alliance": {
      "name": "vivo",
      "title": "Vivo",
      "xewards": {
        "provider": "3"
      }
    },
    "name": "bonusCelularVivo",
    "title": "Bônus Celular",
    "subTitle": "Ligações para telefones Vivo",
    "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
    "faceValue": 1.0,
    "price": 0.3,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "defaultPrize",
    "customBonuz": {
      "color": "f65e01",
      "backgroundColor": "f65e01"
    },
    "unit": "R$",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "bonus"
    ],
    "operators": [
      "vivo"
    ],
    "xewardsOfferCode": "BZCBC001",
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_logo_lists_small_vivo.png",
      "srcset": "img_logo_lists_small_vivo.png 1x,img_logo_lists_small_vivo@2x.png 2x,img_logo_lists_small_vivo@3x.png 3x"
    },
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
    },
    "fullForward": false,
    "shouldQueryStatus": false,
    "xewardsQuantity": 1.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/bonusCelularVivo/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "bonus",
      "title": "Bônus Celular",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "bonus",
    "emoji": "📱"
  },
  "assinatura-ubook": {
    "_id": ObjectId("576038f2c574c115761ebee3"),
    "name": "assinatura-ubook",
    "title": "Audio-livro - 30 dias",
    "subTitle": "Audio-livro - 30 dias",
    "faceValue": 25.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "Assinatura",
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "DBZUB001",
    "deliveryInfo": {
      "created": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
      "forwarded": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi"
    ],
    "tags": [
      "audio-livro"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook-promocional-30-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x"
    },
    "customBonuz": {
      "backgroundColor": "f0622c",
      "color": "f0622c"
    },
    "deliveryCost": 0.0,
    "price": 7.5,
    "description": "Escute livros e revistas. Exame, Veja e mais diversos títulos para você escutar de onde estiver. Curta suas revistas e livros preferidos por 30 dias com o Ubook, o melhor aplicativo de audiolivros do Brasil.",
    "alliance": {
      "title": "Ubook",
      "name": "ubook",
      "xewards": {
        "provider": "2011"
      }
    },
    "fullForward": false,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "audio-livro",
      "title": "Áudio Livro",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "ubook",
    "emoji": "🎧",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.ubook.com/bonuz"
      }
    ]
  },
  "ipass-nacional-mes-vigente": {
    "_id": ObjectId("5a0f28e8c903b90100c754af"),
    "name": "ipass-nacional-mes-vigente",
    "title": "Wi-Fi ilimitado - mês vigente",
    "subTitle": "Wi-Fi ilimitado - mês vigente",
    "faceValue": 16.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "Assinatura",
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "BZIPANAC",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Sua recompensa do iPass estará disponível em breve.",
      "forwarded": "Sua recompensa do iPass estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "ipass",
      "wifi",
      "internet-wifi"
    ],
    "deliveryAddress": {
      "questions": [
        {
          "_id": ObjectId("58791a2b3100450100de21c0"),
          "required": true,
          "example": "exemplo@email.com",
          "text": "Digite o seu email",
          "name": "Email",
          "field": "email"
        },
        {
          "_id": ObjectId("58791a2b3100450100de21bf"),
          "required": false,
          "example": "",
          "text": "Digite o seu primeiro nome",
          "name": "FirstName",
          "field": "firstname"
        },
        {
          "_id": ObjectId("58791a2b3100450100de21bf"),
          "required": false,
          "example": "",
          "text": "Digite o seu sobrenome",
          "name": "LastName",
          "field": "lastname"
        }
      ]
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 4.8,
    "description": "Acesso Wi-Fi em locais públicos para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
    "fullForward": false,
    "alliance": {
      "title": "iPass",
      "name": "ipass",
      "xewards": {
        "provider": ""
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-mes-vigente/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          12.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          27.0,
          31.0,
          34.0,
          35.0,
          41.0,
          43.0,
          47.0,
          48.0,
          51.0,
          53.0,
          54.0,
          61.0,
          62.0,
          63.0,
          65.0,
          67.0,
          68.0,
          69.0,
          71.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          91.0,
          92.0,
          94.0,
          95.0,
          96.0,
          97.0,
          98.0
        ]
      }
    ],
    "group": {
      "name": "internet-wifi",
      "title": "Internet Wi-Fi",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "nacional",
    "emoji": "📱"
  },
  "ipass-internacional-mes-vigente": {
    "_id": ObjectId("5a0f29bfc903b90100c754b0"),
    "name": "ipass-internacional-mes-vigente",
    "title": "Wi-Fi internacional - mês vigente",
    "subTitle": "Wi-Fi internacional - mês vigente",
    "faceValue": 32.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "Assinatura",
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "BZIPAINT",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Sua recompensa do iPass estará disponível em breve.",
      "forwarded": "Sua recompensa do iPass estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "wifi",
      "ipass",
      "internacional",
      "internet-wifi"
    ],
    "deliveryAddress": {
      "questions": [
        {
          "_id": ObjectId("58791a2b3100450100de21c0"),
          "required": true,
          "example": "exemplo@email.com",
          "text": "Digite o seu email",
          "name": "Email",
          "field": "email"
        },
        {
          "_id": ObjectId("58791a2b3100450100de21bf"),
          "required": false,
          "example": "",
          "text": "Digite o seu primeiro nome",
          "name": "FirstName",
          "field": "firstname"
        },
        {
          "_id": ObjectId("58791a2b3100450100de21bf"),
          "required": false,
          "example": "",
          "text": "Digite o seu sobrenome",
          "name": "LastName",
          "field": "lastname"
        }
      ]
    },
    "icon": {
      "srcset": "prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-icon.png"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 9.6,
    "description": "Acesso Wi-Fi em locais públicos no mundo para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
    "fullForward": false,
    "alliance": {
      "title": "iPass",
      "name": "ipass",
      "xewards": {
        "provider": "2015"
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-internacional-mes-vigente/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          31.0,
          41.0,
          61.0
        ]
      }
    ],
    "group": {
      "name": "internet-wifi",
      "title": "Internet Wi-Fi",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "internacional",
    "emoji": "📱"
  },
  "internet-claro-100mb": {
    "_id": ObjectId("58e63a81a983a601007910fd"),
    "__v": 0.0,
    "alliance": {
      "title": "Claro",
      "name": "claro",
      "xewards": {
        "provider": "1"
      }
    },
    "catalog": true,
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": 20.0,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-100mb/prize-icon.png"
    },
    "name": "internet-claro-100mb",
    "operators": [
      "claro"
    ],
    "price": 6.0,
    "shouldQueryStatus": false,
    "subTitle": "Pacotes de dados",
    "tags": [
      "internet",
      "kids",
      "cultura",
      "geracao-y"
    ],
    "title": "Internet Claro 100MB",
    "type": "oneTimeOnly",
    "unit": "MB",
    "xewardsOfferCode": "BZCPM100",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-100mb/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "rewardValue",
        "value": [
          0.0
        ]
      },
      {
        "name": "ddd",
        "value": [
          11.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          27.0,
          31.0,
          34.0,
          35.0,
          41.0,
          43.0,
          47.0,
          48.0,
          51.0,
          53.0,
          54.0,
          61.0,
          62.0,
          65.0,
          69.0,
          71.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          91.0,
          92.0,
          98.0
        ]
      }
    ],
    "keyword": "dados100mb",
    "emoji": "🌐"
  },
  "internet-claro-200mb": {
    "_id": ObjectId("58e63b2aa983a601007910fe"),
    "__v": 0.0,
    "alliance": {
      "title": "Claro",
      "name": "claro",
      "xewards": {
        "provider": "1"
      }
    },
    "catalog": true,
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": 36.0,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-200mb/prize-icon.png"
    },
    "name": "internet-claro-200mb",
    "operators": [
      "claro"
    ],
    "price": 10.8,
    "shouldQueryStatus": false,
    "subTitle": "Pacote de dados",
    "tags": [
      "internet"
    ],
    "title": "Internet Claro 200MB",
    "type": "oneTimeOnly",
    "unit": "MB",
    "xewardsOfferCode": "BZCPM200",
    "wideBanner": {
      "href": "",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "dados200mb",
    "emoji": "🌐"
  },
  "internet-vivo-100mb": {
    "_id": ObjectId("59a81d79bcfb980100b3ee5b"),
    "__v": 0.0,
    "alliance": {
      "title": "Vivo",
      "name": "vivo",
      "xewards": {
        "provider": "3"
      }
    },
    "catalog": true,
    "comboPrizes": [

    ],
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": 22.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-100mb/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "internet-vivo-100mb",
    "operators": [
      "vivo"
    ],
    "price": 6.6,
    "shouldQueryStatus": false,
    "subTitle": "Pacotes de dados",
    "tags": [
      "internet",
      "kids",
      "cultura",
      "geracao-y"
    ],
    "title": "Internet Vivo 100MB",
    "type": "oneTimeOnly",
    "unit": "MB",
    "xewardsOfferCode": "BZVPM100",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_widebanner_vivo.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "rewardValue",
        "value": [
          0.0
        ]
      },
      {
        "name": "ddd",
        "value": [
          11.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          27.0,
          31.0,
          34.0,
          35.0,
          41.0,
          43.0,
          47.0,
          48.0,
          51.0,
          53.0,
          54.0,
          61.0,
          62.0,
          65.0,
          69.0,
          71.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          91.0,
          92.0,
          98.0
        ]
      }
    ],
    "keyword": "dados100mb",
    "emoji": "🌐"
  },
  "dentro-da-historia-r-20-00": {
    "_id": ObjectId("5a7b3145558bde55d1f1f21b"),
    "name": "dentro-da-historia-r-20-00",
    "title": "Livro infantil personalizado - R$20",
    "subTitle": "Livro infantil personalizado - R$20",
    "faceValue": 20.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "voucher",
    "deliveryEngine": "coupon",
    "xewardsOfferCode": "BZDDHI20",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
      "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "livro-infantil"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 0.0,
    "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
    "fullForward": false,
    "alliance": {
      "title": "Dentro da História",
      "name": "dentrodahistoria",
      "xewards": {
        "provider": ""
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          12.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          27.0,
          31.0,
          34.0,
          35.0,
          41.0,
          43.0,
          47.0,
          48.0,
          51.0,
          53.0,
          54.0,
          61.0,
          62.0,
          63.0,
          65.0,
          67.0,
          68.0,
          69.0,
          71.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          91.0,
          92.0,
          94.0,
          95.0,
          96.0,
          97.0,
          98.0
        ]
      }
    ],
    "group": {
      "name": "livro-infantil",
      "title": "Livro Infantil",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "dentro20",
    "emoji": "📖",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.dentrodahistoria.com.br/"
      }
    ]
  },
  "gympass-diaria-r-20": {
    "_id": ObjectId("5a33fe070e02c80100a91013"),
    "name": "gympass-diaria-r-20",
    "title": "Desconto diária academia - R$20",
    "subTitle": "Desconto diária academia - R$20",
    "faceValue": 20.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "VOUCHER",
    "deliveryEngine": "coupon",
    "xewardsOfferCode": "GYMPD020",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Seu desconto do Gympassa está a caminho.",
      "forwarded": "Seu desconto do Gympassa está a caminho.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassavulsa, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "academia"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-diaria-r-20/prize-icon.png"
    },
    "customBonuz": {
      "backgroundColor": "FFFFFF",
      "color": "FFFFFF"
    },
    "deliveryCost": 0.0,
    "price": 0.0,
    "description": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita para fazer uma aula ou treino em mais de 16 mil academias e estúdios parceiros do Gympass em todo o Brasil. São diversos planos a partir de R$69,90.",
    "fullForward": false,
    "alliance": {
      "xewards": {
        "provider": ""
      },
      "title": "Gympass",
      "name": "gympass"
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "academia",
      "title": "Academia",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "diaria20",
    "emoji": "💪",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://bit.ly/Gympassavulsa"
      }
    ]
  },
  "gympass-mensal-r-30": {
    "_id": ObjectId("5a342c58823a2901002aff28"),
    "name": "gympass-mensal-r-30",
    "title": "Desconto mês academia - R$30",
    "subTitle": "Desconto mês academia - R$30",
    "faceValue": 30.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "voucher",
    "deliveryEngine": "coupon",
    "xewardsOfferCode": "GYMPM030",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Seu desconto do Gympassa está a caminho.",
      "forwarded": "Seu desconto do Gympassa está a caminho.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "academia"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-icon.png"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 0.0,
    "description": "Yoga, Crossfit ou Musculação? Ganhe desconto na mensalidade e escolha sua modalidade favorita para fazer uma aula ou treino em mais de 16 mil academias e estúdios parceiros do Gympass em todo o Brasil. São diversos planos a partir de R$69,90.",
    "fullForward": false,
    "alliance": {
      "xewards": {
        "provider": ""
      },
      "title": "Gympass",
      "name": "gympass"
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-mensal-r-30/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "academia",
      "title": "Academia",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "mensal30",
    "emoji": "💪",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://bit.ly/Gympassplano"
      }
    ]
  },
  "presentearAmigo": {
    "_id": ObjectId("555f7d55fb71969a8840725f"),
    "alliance": {
      "title": "bonuz.com",
      "name": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "name": "presentearAmigo",
    "title": "Bônus para Presente",
    "subTitle": "Presenteie parte dos seu bônus",
    "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
    "faceValue": 1.0,
    "price": 0.3,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "donation",
    "customBonuz": {
      "backgroundColor": "FFFFFF",
      "color": "FF0000"
    },
    "unit": "R$",
    "deliveryAddress": {
      "questions": [
        {
          "_id": ObjectId("577bf83b6388d40100b2c820"),
          "required": true,
          "example": "11988888888",
          "text": "Digite o Celular do seu amigo",
          "name": "Celular",
          "field": "mobile"
        }
      ]
    },
    "tags": [
      "bonus"
    ],
    "operators": [
      "claro",
      "oi",
      "tim",
      "vivo",
      "nextel",
      "ctbc",
      "sercomtel"
    ],
    "xewardsOfferCode": "BZCBC001",
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_prize_list_gift.png",
      "srcset": "img_prize_list_gift.png 1x,img_prize_list_gift@2x.png 2x,img_prize_list_gift@3x.png 3x"
    },
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
      "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
      "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
    },
    "fullForward": false,
    "shouldQueryStatus": false,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/presentearAmigo/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "bonus",
      "title": "Bônus Celular",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "presente",
    "emoji": "🎁"
  },
  "primepass-oneshot": {
    "_id": ObjectId("5a37ef602d50f201001dcf38"),
    "name": "primepass-oneshot",
    "title": "Ingresso cinema",
    "subTitle": "Ingresso cinema",
    "faceValue": 24.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "voucher",
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "BZPRPA01",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
      "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "cultura",
      "cinema"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "srcset": "prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-icon.png"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 6.0,
    "price": 7.2,
    "description": "Ingresso de cinema. Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass.",
    "fullForward": false,
    "alliance": {
      "xewards": {
        "provider": "2016"
      },
      "title": "primepass",
      "name": "primepass"
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          12.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          27.0,
          31.0,
          34.0,
          35.0,
          41.0,
          43.0,
          47.0,
          48.0,
          51.0,
          53.0,
          54.0,
          61.0,
          62.0,
          63.0,
          65.0,
          67.0,
          68.0,
          69.0,
          71.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          91.0,
          92.0,
          94.0,
          95.0,
          96.0,
          97.0,
          98.0
        ]
      }
    ],
    "group": {
      "name": "cinema",
      "title": "Cinema",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "ingresso",
    "emoji": "🎥",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://primepass.club/resgate"
      }
    ]
  },
  "desconto-no-picanha-barbecue": {
    "_id": ObjectId("5ba40afa45a7f0121ce1693a"),
    "actions": [
      {
        "url": "",
        "title": ""
      }
    ],
    "alliance": {
      "name": "Bob´s",
      "title": "Bob´s",
      "xewards": {
        "provider": "Bob´s"
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "FFFFFF",
      "backgroundColor": "FFFFFF"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "",
    "emoji": "",
    "faceValue": 11.0,
    "group": {
      "name": "fast-food",
      "title": "Fast Food"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-no-picanha-barbecue/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "keyword": "",
    "name": "desconto-no-picanha-barbecue",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 2.75,
    "rules": [

    ],
    "shortDescription": "",
    "socialWeight": 0.0,
    "subTitle": "Desconto de R$ 11 para saborear um lanche com hambúrguer, fatias generosas de queijo, salada na medida e o clássico molho barbecue.",
    "tags": [

    ],
    "title": "Desconto no Picanha Barbecue",
    "type": "oneTimeOnly",
    "unit": "UN",
    "xewardsOfferCode": "BZBOBPBP",
    "weight": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-no-picanha-barbecue/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    }
  },
  "filmes-e-series-na-looke-por-30-dias": {
    "_id": ObjectId("5b844614dd85001021e31a49"),
    "alliance": {
      "name": "LOOKE",
      "title": "LOOKE",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "filmes-e-series-na-looke-por-30-dias",
    "title": "Filmes e séries na Looke por 30 dias",
    "subTitle": "Filmes e séries na Looke por 30 dias",
    "description": "Em casa, no caminho ou onde quiser. Aproveite os melhores filmes e séries por 30 dias.",
    "faceValue": 23.0,
    "price": 5.75,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "entretenimento"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "ctbc",
      "sercomtel"
    ],
    "xewardsOfferCode": "BZLOOK30",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "group": {
      "name": "entretenimento",
      "title": "Entretenimento",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "",
    "emoji": "",
    "weight": 0.0,
    "socialWeight": 0.0
  },
  "ureader-livros-digitais-30-dias": {
    "_id": ObjectId("5af5e419422d943251df4827"),
    "alliance": {
      "name": "ubook",
      "title": "Ubook",
      "xewards": {
        "provider": "2011"
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Sua recompensa estará disponível em breve.",
      "forwarded": "Sua recompensa estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua assinatura Ureader de 30 dias está disponível! Acesse https://bit.ly/2txfiSm insira o código {{voucher}}, faca o cadastro e aproveite!{{/ifvalue}}"
    },
    "description": "Ficção, romance, biografias e muito mais. Com o aplicativo Ureader, você tem milhares de títulos para manter a leitura em dia.",
    "faceValue": 20.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "ureader-livros-digitais-30-dias",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 6.0,
    "shortDescription": "Ficção, romance, biografias e mais. São vários títulos para manter a leitura em dia.",
    "subTitle": "Assinatura Ureader de 30 dias",
    "tags": [
      "livro-digital"
    ],
    "title": "Livros digitais para ler",
    "type": "oneTimeOnly",
    "unit": "Assinatura",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ureader-livros-digitais-30-dias/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "BZURE001",
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          12.0,
          13.0,
          14.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          22.0,
          24.0,
          27.0,
          28.0,
          31.0,
          32.0,
          33.0,
          34.0,
          35.0,
          37.0,
          38.0,
          41.0,
          42.0,
          43.0,
          44.0,
          45.0,
          46.0,
          47.0,
          48.0,
          49.0,
          51.0,
          53.0,
          54.0,
          55.0,
          61.0,
          62.0,
          63.0,
          64.0,
          65.0,
          66.0,
          67.0,
          68.0,
          69.0,
          71.0,
          73.0,
          74.0,
          75.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          87.0,
          88.0,
          89.0,
          91.0,
          92.0,
          93.0,
          94.0,
          95.0,
          96.0,
          97.0,
          98.0,
          99.0
        ]
      }
    ],
    "group": {
      "name": "livro-digital",
      "title": "Livro Digital",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "ureader",
    "emoji": "📔",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://bit.ly/2txfiSm"
      }
    ]
  },
  "revistas-impressas-e-digitais-desconto-de-r-20-00": {
    "_id": ObjectId("5af5e94dc6ca389b6818e898"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-e-digitais-desconto-de-r-20-00",
    "title": "Revista Impressa ou Digital - R$20",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 20.0,
    "price": 5.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM20",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS20 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-20/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-20-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista20",
    "emoji": "📰",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
      }
    ],
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "vale-um-cheddar-m-r-17": {
    "_id": ObjectId("5bb65e8c45a7f0121ce3dc48"),
    "actions": [

    ],
    "alliance": {
      "name": "Bob´s",
      "title": "Bob´s",
      "xewards": {
        "provider": "Bob´s"
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "Se você adora cheddar, vai querer devorar este sanduíche. Hambúrguer acompanhado de muito queijo cheddar cremoso e cebolas crocantes refogadas com molho shoyu.",
    "emoji": "🍔",
    "faceValue": 17.0,
    "group": {
      "name": "fast-food",
      "title": "Fast Food"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/vale-um-cheddar-m-r-17/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "keyword": "Cheddar17",
    "name": "vale-um-cheddar-m-r-17",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 4.25,
    "rules": [
      {
        "name": "ddd",
        "value": [
          67.0,
          61.0,
          62.0,
          64.0,
          31.0,
          32.0,
          33.0,
          34.0,
          35.0,
          37.0,
          38.0,
          21.0,
          22.0,
          24.0,
          11.0,
          12.0,
          13.0,
          14.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          41.0,
          42.0,
          43.0,
          44.0,
          45.0,
          46.0,
          47.0,
          48.0,
          49.0,
          51.0,
          53.0,
          54.0,
          55.0,
          27.0,
          28.0
        ]
      }
    ],
    "shortDescription": "Sanduíche Cheddar M Bob's R$17,00",
    "socialWeight": 1.0,
    "subTitle": "Sanduíche Cheddar M do Bob's",
    "tags": [

    ],
    "title": "Vale um Cheddar M R$17",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "BZBOBC17",
    "weight": 1.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/vale-um-cheddar-m-r-17/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null
  },
  "assinatura-ubook-10-dias": {
    "_id": ObjectId("5bb3a15445a7f0121c9208bf"),
    "alliance": {
      "name": "ubook",
      "title": "Ubook",
      "xewards": {
        "provider": "2011"
      }
    },
    "fullForward": false,
    "name": "assinatura-ubook-10-dias",
    "title": "Assinatura Ubook 10 dias",
    "subTitle": "Livros e revistas digitais para escutar",
    "description": "Exame, Veja, Tititi e mais diversos títulos para você escutar de onde estiver. Curta suas revistas e livros preferidos por 10 dias com o Ubook, o melhor aplicativo de audiolivros do Brasil.",
    "faceValue": 10.0,
    "price": 2.7,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook-10-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [

    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZUBOP10",
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook-10-dias/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "actions": null,
    "emoji": null,
    "group": null,
    "keyword": null,
    "praiseTexts": [

    ],
    "rules": [

    ]
  },
  "assinatura-ubook-promocional-15-dias": {
    "_id": ObjectId("5a29885dccbe280100f3b4a8"),
    "name": "assinatura-ubook-promocional-15-dias",
    "title": "Audio-livro - 15 dias",
    "subTitle": "Assinatura ubook promocional - 15 dias",
    "faceValue": 13.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "Assinatura",
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "BZUBOP15",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
      "forwarded": "Em breve você receberá seu código para escutar seus livros preferidos onde quiser.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "audio-livro"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook-promocional-15-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "customBonuz": {
      "color": "f0622c",
      "backgroundColor": "f0622c"
    },
    "deliveryCost": 0.0,
    "price": 3.0,
    "description": "Escute livros e revistas. Exame, Veja e mais diversos títulos para você escutar de onde estiver. Curta suas revistas e livros preferidos por 15 dias com o Ubook, o melhor aplicativo de audiolivros do Brasil.",
    "fullForward": false,
    "alliance": {
      "name": "ubook",
      "title": "Ubook",
      "xewards": {
        "provider": "2011"
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/assinatura-ubook-promocional-15-dias/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "audio-livro",
      "title": "Áudio Livro",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "ubook",
    "emoji": "🎧",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.ubook.com/bonuz"
      }
    ]
  },
  "cabify-desconto-5-reais-em-4-corridas": {
    "_id": ObjectId("5ae0d65d91afd663240ff0a9"),
    "alliance": {
      "name": "CABIFY",
      "title": "Cabify",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
    },
    "description": "Com Cabify você ganha R$5,00 de desconto em 4 corridas nas principais cidades do país.",
    "faceValue": 20.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "cabify-desconto-5-reais-em-4-corridas",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 5.0,
    "subTitle": "Desconto de R$5,00 em 4 corridas.",
    "tags": [
      "transporte"
    ],
    "title": "Desconto de R$5,00 em 4 corridas.",
    "type": "oneTimeOnly",
    "unit": "UN",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-4-corridas/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "BZCA4C20",
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          13.0,
          19.0,
          21.0,
          51.0,
          31.0,
          41.0,
          61.0
        ]
      }
    ],
    "group": {
      "name": "transporte",
      "title": "Transporte",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "cabify20",
    "emoji": "🚕"
  },
  "ingresso-de-cinema-2d-de-2-a-4-primepass": {
    "_id": ObjectId("5bb660a145a7f0121ce49cf4"),
    "alliance": {
      "name": "primepass",
      "title": "primepass",
      "xewards": {
        "provider": "null"
      }
    },
    "fullForward": false,
    "name": "ingresso-de-cinema-2d-de-2-a-4-primepass",
    "title": "Ingresso cinema de 2ª a 4ª",
    "subTitle": "Ingresso único de cinema de 2ª a 4ª na rede Primepass",
    "description": "Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
    "faceValue": 18.0,
    "price": 4.5,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ingresso-de-cinema-2d-de-2-a-4-primepass/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [

    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "sercomtel",
      "nextel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZPRPA2AA",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ingresso-de-cinema-2d-de-2-a-4-primepass/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "actions": null,
    "emoji": null,
    "group": null,
    "keyword": null,
    "praiseTexts": [

    ],
    "rules": [

    ]
  },
  "revistas-impressas-desconto-de-r-35-00": {
    "_id": ObjectId("5af5eae4c6ca38787718e89a"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-desconto-de-r-35-00",
    "title": "Desconto R$35 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 35.0,
    "price": 8.75,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM35",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS35 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-35/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-35-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista35",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-desconto-de-r-40-00": {
    "_id": ObjectId("5af5eb46c6ca38391018e89b"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-desconto-de-r-40-00",
    "title": "Desconto R$40 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 40.0,
    "price": 10.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM40",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS40 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-40/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-40-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista40",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-e-digitais-desconto-de-r-10-00": {
    "_id": ObjectId("5af5e87f422d94621edf482d"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-e-digitais-desconto-de-r-10-00",
    "title": "Revista Impressa ou Digital - R$10",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 10.0,
    "price": 2.5,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM10",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS10 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-10/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-10-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista10",
    "emoji": "📰",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
      }
    ],
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-e-digitais-desconto-de-r-15-00": {
    "_id": ObjectId("5af5e8f7c6ca385df418e897"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-e-digitais-desconto-de-r-15-00",
    "title": "Desconto R$15 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 15.0,
    "price": 3.75,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM15",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS15 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-15/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-15-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista15",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-e-digitais-desconto-de-r-25-00": {
    "_id": ObjectId("5af5e9e0422d947d4fdf482e"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-e-digitais-desconto-de-r-25-00",
    "title": "Desconto R$25 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 25.0,
    "price": 6.25,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM25",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS25 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-25/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-25-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista25",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-e-digitais-desconto-de-r-30-00": {
    "_id": ObjectId("5af5ea60c6ca381a0418e899"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-e-digitais-desconto-de-r-30-00",
    "title": "Revista Impressa ou Digital - R$30",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 30.0,
    "price": 7.5,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM30",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS30 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-30/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-e-digitais-desconto-de-r-30-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista30",
    "emoji": "📰",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.assine3.com.br/parceria3/minutrade-10/identificacao"
      }
    ],
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "ipass-nacional-promocional-mes-vigente": {
    "_id": ObjectId("5a33f52c0e02c80100a9100f"),
    "name": "ipass-nacional-promocional-mes-vigente",
    "title": "Rede wi-fi no Brasil",
    "subTitle": "WIFI iPass nacional - Mês Vigente",
    "faceValue": 10.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "Assinatura",
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "BZIPAPRO",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Sua recompensa do iPass estará disponível em breve.",
      "forwarded": "Sua recompensa do iPass estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Sua recompensa do iPass está disponível. Você receberá um e-mail com as informações de acesso.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "geracao-y",
      "internet"
    ],
    "deliveryAddress": {
      "questions": [
        {
          "_id": "58791a2b3100450100de21c0",
          "required": true,
          "example": "exemplo@email.com",
          "text": "Digite o seu email",
          "name": "Email",
          "field": "email"
        },
        {
          "_id": "58791a2b3100450100de21bf",
          "required": false,
          "example": "",
          "text": "Digite o seu primeiro nome",
          "name": "FirstName",
          "field": "firstname"
        },
        {
          "_id": "58791a2b3100450100de21bf",
          "required": false,
          "example": "",
          "text": "Digite o seu sobrenome",
          "name": "LastName",
          "field": "lastname"
        }
      ]
    },
    "icon": {
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/ipass-nacional-promocional-mes-vigente/prize-icon.png"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 3.0,
    "description": "Acesso Wi-Fi em locais públicos para você se manter conectado em aeroportos, restaurantes, hospitais, hotéis e muito mais. Com o iPass, são mais de dois milhões de pontos wi-fi pra aproveitar até o último dia deste mês.",
    "fullForward": false,
    "alliance": {
      "name": "ipass",
      "title": "iPass",
      "xewards": {
        "provider": "2015"
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/img_widebanner_ipass.png",
      "srcset": ""
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "internet",
      "title": "Internet Móvel"
    },
    "keyword": "nacional",
    "emoji": "📱",
    "actions": null,
    "rules": [

    ]
  },
  "dentro-da-historia-r-10-00": {
    "_id": ObjectId("5a5cb2e184c81b010095cca3"),
    "name": "dentro-da-historia-r-10-00",
    "title": "Livro infantil personalizado - R$10",
    "subTitle": "Livro infantil personalizado - R$10",
    "faceValue": 10.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "voucher",
    "deliveryEngine": "coupon",
    "xewardsOfferCode": "BZDDHI10",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
      "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "kids",
      "livro-infantil"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 0.0,
    "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
    "fullForward": false,
    "alliance": {
      "title": "Dentro da História",
      "name": "dentrodahistoria",
      "xewards": {
        "provider": ""
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          12.0,
          13.0,
          14.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          22.0,
          24.0,
          27.0,
          28.0,
          31.0,
          32.0,
          33.0,
          34.0,
          35.0,
          37.0,
          38.0,
          41.0,
          42.0,
          43.0,
          44.0,
          45.0,
          46.0,
          47.0,
          48.0,
          49.0,
          51.0,
          53.0,
          54.0,
          55.0,
          61.0,
          62.0,
          63.0,
          64.0,
          65.0,
          66.0,
          67.0,
          68.0,
          69.0,
          71.0,
          73.0,
          74.0,
          75.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          87.0,
          88.0,
          89.0,
          91.0,
          92.0,
          93.0,
          94.0,
          95.0,
          96.0,
          97.0,
          98.0,
          99.0
        ]
      }
    ],
    "group": {
      "name": "livro-infantil",
      "title": "Livro Infantil",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "dentro10",
    "emoji": "📖",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.dentrodahistoria.com.br/"
      }
    ]
  },
  "gympass-diaria-1": {
    "_id": ObjectId("5a33fa710e02c80100a91011"),
    "name": "gympass-diaria-1",
    "title": "Desconto diária academia - R$10",
    "subTitle": "Desconto diária academia - R$10",
    "faceValue": 10.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "voucher",
    "deliveryEngine": "coupon",
    "xewardsOfferCode": "GYMPD010",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Seu desconto do Gympassa está a caminho.",
      "forwarded": "Seu desconto do Gympassa está a caminho.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassavulsa, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "oi",
      "nextel",
      "tim",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "geracao-y",
      "academia"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-diaria-1/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 0.0,
    "description": "Yoga, Crossfit ou Musculação? Ganhe desconto na diária e escolha sua modalidade favorita para fazer uma aula ou treino em mais de 16 mil academias e estúdios parceiros do Gympass em todo o Brasil.",
    "fullForward": false,
    "alliance": {
      "title": "Gympass",
      "name": "gympass",
      "xewards": {
        "provider": ""
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/gympass-diaria-1/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          12.0,
          13.0,
          14.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          22.0,
          24.0,
          27.0,
          28.0,
          31.0,
          32.0,
          33.0,
          34.0,
          35.0,
          37.0,
          38.0,
          41.0,
          42.0,
          43.0,
          44.0,
          45.0,
          46.0,
          47.0,
          48.0,
          49.0,
          51.0,
          53.0,
          54.0,
          55.0,
          61.0,
          62.0,
          63.0,
          64.0,
          65.0,
          66.0,
          67.0,
          68.0,
          69.0,
          71.0,
          73.0,
          74.0,
          75.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          87.0,
          88.0,
          89.0,
          91.0,
          92.0,
          93.0,
          94.0,
          95.0,
          96.0,
          97.0,
          98.0,
          99.0
        ]
      }
    ],
    "group": {
      "name": "academia",
      "title": "Academia",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "diaria10",
    "emoji": "💪",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://bit.ly/Gympassavulsa"
      }
    ]
  },
  "audiolivro-infantis-30-dias": {
    "_id": ObjectId("5af5e4c7422d943655df4828"),
    "alliance": {
      "name": "ubook",
      "title": "Ubook",
      "xewards": {
        "provider": "2011"
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "xewardsOfferCode": "BZUBK001",
    "deliveryInfo": {
      "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
      "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
    },
    "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
    "faceValue": 7.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "audiolivro-infantis-30-dias",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 2.4,
    "subTitle": "Assinatura Ubook Kids de 30 dias",
    "tags": [
      "audio-livro-infantil"
    ],
    "title": "Audiolivro Infantis - 30 dias",
    "type": "oneTimeOnly",
    "unit": "Assinatura",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "audio-livro-infantil",
      "title": "Áudio Livro Infantil",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "ubkids",
    "emoji": "🎧",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.ubook.com/bonuz"
      }
    ]
  },
  "cabify-desconto-5-reais-em-6-corridas": {
    "_id": ObjectId("5ae0d80c91afd6c9f80ff0aa"),
    "alliance": {
      "name": "CABIFY",
      "title": "Cabify",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
    },
    "description": "Com Cabify você ganha R$5,00 de desconto em 6 corridas nas principais cidades do país.",
    "faceValue": 30.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "cabify-desconto-5-reais-em-6-corridas",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 7.5,
    "subTitle": "Desconto de R$5,00 em 6 corridas.",
    "tags": [
      "transporte"
    ],
    "title": "Desconto de R$5,00 em 6 corridas",
    "type": "oneTimeOnly",
    "unit": "UN",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-6-corridas/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "BZCA6C30",
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "transporte",
      "title": "Transporte",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "cabify30",
    "emoji": "🚕"
  },
  "cabify-desconto-5-reais-em-8-corridas": {
    "_id": ObjectId("5ae0d90ced255d8bda259166"),
    "alliance": {
      "name": "CABIFY",
      "title": "Cabify",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
    },
    "description": "Com Cabify você ganha R$5,00 de desconto em 8 corridas nas principais cidades do país.",
    "faceValue": 40.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "cabify-desconto-5-reais-em-8-corridas",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 10.0,
    "subTitle": "Desconto de R$5,00 em 8 corridas.",
    "tags": [
      "transporte"
    ],
    "title": "Desconto de R$5,00 em 8 corridas",
    "type": "oneTimeOnly",
    "unit": "UN",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-5-reais-em-8-corridas/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "BZCA8C40",
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "transporte",
      "title": "Transporte",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "cabify40",
    "emoji": "🚕"
  },
  "cabify-desconto-7e50-reais-em-12-corridas": {
    "_id": ObjectId("5ae0dc83ed255d6315259167"),
    "alliance": {
      "name": "CABIFY",
      "title": "Cabify",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
    },
    "description": "Com Cabify você ganha R$7,50 de desconto em 12 corridas nas principais cidades do país.",
    "faceValue": 90.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "cabify-desconto-7e50-reais-em-12-corridas",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 22.5,
    "subTitle": "Desconto de R$7,50 em 12 corridas.",
    "tags": [
      "transporte"
    ],
    "title": "Desconto de R$7,50 em 12 corridas.",
    "type": "oneTimeOnly",
    "unit": "UN",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-12-corridas/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "BZCA12C90",
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "transporte",
      "title": "Transporte",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "cabify90",
    "emoji": "🚕"
  },
  "cabify-desconto-7e50-reais-em-8-corridas": {
    "_id": ObjectId("5ae0dbec91afd676860ff0ac"),
    "alliance": {
      "name": "CABIFY",
      "title": "Cabify",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "forwarded": "Em breve seus descontos em corridas Cabify estarão disponíveis.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Insira o código {{coupon}} no seu aplicativo para ativar seu desconto.{{/ifvalue}}"
    },
    "description": "Com Cabify você ganha R$7,50 de desconto em 8 corridas nas principais cidades do país.",
    "faceValue": 60.0,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "cabify-desconto-7e50-reais-em-8-corridas",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 17.0,
    "subTitle": "Desconto de R$7,50 em 8 corridas.",
    "tags": [
      "transporte"
    ],
    "title": "Desconto de R$7,50 em 8 corridas.",
    "type": "oneTimeOnly",
    "unit": "UN",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/cabify-desconto-7e50-reais-em-8-corridas/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "BZCA9C68",
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "transporte",
      "title": "Transporte",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "cabify60",
    "emoji": "🚕"
  },
  "casquinha-mcdonalds": {
    "_id": ObjectId("5a5cb2e186117101007d16fc"),
    "name": "casquinha-mcdonalds",
    "title": "Casquinha McDonald´s",
    "subTitle": "Uma casquinha McDonald´s",
    "faceValue": 3.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "Unidade",
    "deliveryEngine": "coupon",
    "xewardsOfferCode": "BZMCC001",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Sua casquinha estará disponível em breve.",
      "forwarded": "Sua casquinha estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "kids",
      "cultura",
      "fast-food"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "customBonuz": {
      "backgroundColor": "df1a23",
      "color": "df1a23"
    },
    "deliveryCost": 0.0,
    "price": 0.9,
    "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
    "fullForward": false,
    "alliance": {
      "title": "McDonald's",
      "name": "mcdonalds",
      "xewards": {
        "provider": ""
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonalds/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          27.0,
          31.0,
          34.0,
          35.0,
          41.0,
          43.0,
          47.0,
          48.0,
          51.0,
          53.0,
          54.0,
          61.0,
          62.0,
          65.0,
          69.0,
          71.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          91.0,
          92.0,
          98.0
        ]
      }
    ],
    "group": {
      "name": "fast-food",
      "title": "Fast Food",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "casquinha",
    "emoji": "🍦",
    "actions": [
      {
        "title": "ABRIR CUPOM",
        "url": "https://casquinha.bonuz.com/{{coupon.code}}"
      }
    ]
  },
  "dentro-da-historia-r-30-00": {
    "_id": ObjectId("5a7b3157558bde55d1f1f21c"),
    "name": "dentro-da-historia-r-30-00",
    "title": "Livro infantil personalizado - R$30",
    "subTitle": "Livro infantil personalizado - R$30",
    "faceValue": 30.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "unit": "voucher",
    "deliveryEngine": "coupon",
    "xewardsOfferCode": "BZDDHI30",
    "comboPrizes": [

    ],
    "deliveryInfo": {
      "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
      "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
    },
    "shouldQueryStatus": false,
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "tags": [
      "livro-infantil"
    ],
    "deliveryAddress": {
      "questions": [

      ]
    },
    "icon": {
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-icon.png"
    },
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryCost": 0.0,
    "price": 0.0,
    "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
    "fullForward": false,
    "alliance": {
      "title": "Dentro da História",
      "name": "dentrodahistoria",
      "xewards": {
        "provider": ""
      }
    },
    "__v": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/dentro-da-historia-r-30-00/prize-wideBanner.png",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          12.0,
          13.0,
          14.0,
          15.0,
          16.0,
          17.0,
          18.0,
          19.0,
          21.0,
          22.0,
          24.0,
          27.0,
          28.0,
          31.0,
          32.0,
          33.0,
          34.0,
          35.0,
          37.0,
          38.0,
          41.0,
          42.0,
          43.0,
          44.0,
          45.0,
          46.0,
          47.0,
          48.0,
          49.0,
          51.0,
          53.0,
          54.0,
          55.0,
          61.0,
          62.0,
          63.0,
          64.0,
          65.0,
          66.0,
          67.0,
          68.0,
          69.0,
          71.0,
          73.0,
          74.0,
          75.0,
          77.0,
          79.0,
          81.0,
          82.0,
          83.0,
          84.0,
          85.0,
          86.0,
          87.0,
          88.0,
          89.0,
          91.0,
          92.0,
          93.0,
          94.0,
          95.0,
          96.0,
          97.0,
          98.0,
          99.0
        ]
      }
    ],
    "group": {
      "name": "livro-infantil",
      "title": "Livro Infantil",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "dentro30",
    "emoji": "📖",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://www.dentrodahistoria.com.br/"
      }
    ]
  },
  "desconto-de-r-100-00-em-frete": {
    "_id": ObjectId("5b3145c59a47951f961e4b33"),
    "alliance": {
      "name": "RAPPI",
      "title": "RAPPI",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "desconto-de-r-100-00-em-frete",
    "title": "Desconto de R$100 em frete",
    "subTitle": "Desconto de R$100,00 no frete da Rappi",
    "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
    "faceValue": 100.0,
    "price": 18.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "1",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "delivery"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZRPI100",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
      "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-100-00-em-frete/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "delivery",
      "title": "Delivery",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "frete100",
    "emoji": "🏍"
  },
  "desconto-de-r-70-00-em-frete": {
    "_id": ObjectId("5b3145009a4795e3471e4b32"),
    "alliance": {
      "name": "RAPPI",
      "title": "RAPPI",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "desconto-de-r-70-00-em-frete",
    "title": "Desconto de R$70 em frete",
    "subTitle": "Desconto de R$,00 no frete da Rappi",
    "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
    "faceValue": 70.0,
    "price": 10.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "1",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "delivery"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZRPI070",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
      "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-70-00-em-frete/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "delivery",
      "title": "Delivery",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "frete70",
    "emoji": "🏍"
  },
  "desconto-em-frete": {
    "_id": ObjectId("5b31429cefd915af5ffb247b"),
    "alliance": {
      "name": "RAPPI",
      "title": "RAPPI",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "desconto-em-frete",
    "title": "Desconto de R$40 em frete",
    "subTitle": "Desconto de R$40,00 no frete da Rappi",
    "description": "Quer comprar roupa, pedir comida, fazer compras no supermercado ou até mesmo sacar dinheiro? A Rappi faz tudo isso e entrega na sua casa ou trabalho, como preferir!",
    "faceValue": 40.0,
    "price": 5.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "delivery"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZRPI040",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve o seu desconto no frete estará disponível. Aguarde!",
      "forwarded": "Em breve o seu desconto no frete estará disponível. Aguarde!",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse o site www.rappi.com.br, informe o código {{coupon}} e aproveite o desconto no frete!{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-frete/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "delivery",
      "title": "Delivery",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "frete40",
    "emoji": "🏍"
  },
  "flores-com-desconto-de-r-10": {
    "_id": ObjectId("5b48edea1a380d34dc897034"),
    "alliance": {
      "name": "GIULIANA FLORES",
      "title": "GIULIANA FLORES",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "flores-com-desconto-de-r-10",
    "title": "Flores com desconto de R$10",
    "subTitle": "Flores com desconto de R$10",
    "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$10 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
    "faceValue": 10.0,
    "price": 2.5,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [

    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZGIFL10",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-10/prize-wideBanner.png",
      "srcset": ""
    }
  },
  "flores-com-desconto-de-r-20": {
    "_id": ObjectId("5b48ee3d1a380d81b1897035"),
    "alliance": {
      "name": "GIULIANA FLORES",
      "title": "GIULIANA FLORES",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "flores-com-desconto-de-r-20",
    "title": "Flores com desconto de R$20",
    "subTitle": "Flores com desconto de R$20",
    "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$20 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
    "faceValue": 20.0,
    "price": 5.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "un",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [

    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZGIFL20",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-20/prize-wideBanner.png",
      "srcset": ""
    }
  },
  "flores-com-desconto-de-r-30": {
    "_id": ObjectId("5b48ee841a380decb8897036"),
    "alliance": {
      "name": "GIULIANA FLORES",
      "title": "GIULIANA FLORES",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "flores-com-desconto-de-r-30",
    "title": "Flores com desconto de R$30",
    "subTitle": "Flores com desconto de R$30",
    "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$30 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
    "faceValue": 30.0,
    "price": 7.5,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [

    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZGIFL30",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-30/prize-wideBanner.png",
      "srcset": ""
    }
  },
  "flores-com-desconto-de-r-40": {
    "_id": ObjectId("5b48ef3c1a380dbc9d897037"),
    "alliance": {
      "name": "GIULIANA FLORES",
      "title": "GIULIANA FLORES",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "flores-com-desconto-de-r-40",
    "title": "Flores com desconto de R$40",
    "subTitle": "Flores com desconto de R$40",
    "description": "Amor, aniversário, agradecimento e muito mais. Aproveite R$40 de desconto e presenteie com flores ou kits especiais Giuliana Flores.",
    "faceValue": 40.0,
    "price": 10.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [

    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZGIFL40",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/flores-com-desconto-de-r-40/prize-wideBanner.png",
      "srcset": ""
    }
  },
  "internet-claro-300mb": {
    "_id": ObjectId("58e63bcea983a601007910ff"),
    "__v": 0.0,
    "alliance": {
      "title": "Claro",
      "name": "claro",
      "xewards": {
        "provider": "1"
      }
    },
    "catalog": true,
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": 48.0,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-icon.png"
    },
    "name": "internet-claro-300mb",
    "operators": [
      "claro"
    ],
    "price": 14.4,
    "shouldQueryStatus": false,
    "subTitle": "Pacote de dados",
    "tags": [
      "internet"
    ],
    "title": "Internet Claro 300MB",
    "type": "oneTimeOnly",
    "unit": "MB",
    "xewardsOfferCode": "BZCPM300",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-300mb/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "dados300mb",
    "emoji": "🌐"
  },
  "internet-claro-400mb": {
    "_id": ObjectId("58e63dafa983a60100791100"),
    "__v": 0.0,
    "alliance": {
      "title": "Claro",
      "name": "claro",
      "xewards": {
        "provider": "1"
      }
    },
    "catalog": true,
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": 64.0,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-icon.png"
    },
    "name": "internet-claro-400mb",
    "operators": [
      "claro"
    ],
    "price": 19.2,
    "shouldQueryStatus": false,
    "subTitle": "Pacote de dados",
    "tags": [
      "internet"
    ],
    "title": "Internet Claro 400MB",
    "type": "oneTimeOnly",
    "unit": "MB",
    "xewardsOfferCode": "BZCPM400",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-claro-400mb/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "dados400mb",
    "emoji": "🌐"
  },
  "internet-oi-300mb": {
    "_id": ObjectId("59b843bc11378f010020ce6b"),
    "__v": 0.0,
    "alliance": {
      "title": "Oi",
      "name": "oi",
      "xewards": {
        "provider": "4"
      }
    },
    "catalog": true,
    "comboPrizes": [

    ],
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": null,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-icon.png"
    },
    "name": "internet-oi-300mb",
    "operators": [
      "oi"
    ],
    "price": 18.0,
    "shouldQueryStatus": false,
    "subTitle": "Pacote de dados",
    "tags": [
      "internet"
    ],
    "title": "Internet Oi 300MB",
    "type": "priceless",
    "unit": "MB",
    "xewardsOfferCode": "BZOPM300",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-300mb/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "dados300mb",
    "emoji": "🌐"
  },
  "internet-oi-400mb": {
    "_id": ObjectId("59badedb419cfc01002e938a"),
    "__v": 0.0,
    "alliance": {
      "title": "Oi",
      "name": "oi",
      "xewards": {
        "provider": "4"
      }
    },
    "catalog": true,
    "comboPrizes": [

    ],
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": null,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-icon.png"
    },
    "name": "internet-oi-400mb",
    "operators": [
      "oi"
    ],
    "price": 24.0,
    "shouldQueryStatus": false,
    "subTitle": "Pacote de dados",
    "tags": [
      "internet"
    ],
    "title": "Internet Oi 400MB",
    "type": "priceless",
    "unit": "MB",
    "xewardsOfferCode": "BZOPM400",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-oi-400mb/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "dados400mb",
    "emoji": "🌐"
  },
  "internet-vivo-300mb": {
    "_id": ObjectId("59b8430311378f010020ce69"),
    "__v": 0.0,
    "alliance": {
      "title": "Vivo",
      "name": "vivo",
      "xewards": {
        "provider": "3"
      }
    },
    "catalog": true,
    "comboPrizes": [

    ],
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": 52.0,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-icon.png"
    },
    "name": "internet-vivo-300mb",
    "operators": [
      "vivo"
    ],
    "price": 15.6,
    "shouldQueryStatus": false,
    "subTitle": "Pacote de dados",
    "tags": [
      "internet"
    ],
    "title": "Internet Vivo 300MB",
    "type": "oneTimeOnly",
    "unit": "MB",
    "xewardsOfferCode": "BZVPM300",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-300mb/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "dados300mb",
    "emoji": "🌐"
  },
  "internet-vivo-400mb": {
    "_id": ObjectId("59badaf4419cfc01002e9387"),
    "__v": 0.0,
    "alliance": {
      "title": "Vivo",
      "name": "vivo",
      "xewards": {
        "provider": "3"
      }
    },
    "catalog": true,
    "comboPrizes": [

    ],
    "customBonuz": {
      "backgroundColor": "ffffff",
      "color": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "Seu pacote de dados será entregue em breve.",
      "forwarded": "Seu pacote de dados será entregue em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seu pacote de dados já está disponível. É hora de acessar suas redes sociais e se manter conectado com os amigos.{{/ifvalue}}"
    },
    "description": "Receba agora mesmo o seu bônus internet para se manter conectado e usar as redes sociais.  Bônus válido para clientes ativos de planos pessoa física.Pós-Pago, Pré-Pago e Controle: Bônus válido por 30 dias.",
    "faceValue": 58.0,
    "fullForward": false,
    "icon": {
      "srcset": "prize-icon.png 1x,prize-icon@2x.png 2x,prize-icon@3x.png 3x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-icon.png"
    },
    "name": "internet-vivo-400mb",
    "operators": [
      "vivo"
    ],
    "price": 17.4,
    "shouldQueryStatus": false,
    "subTitle": "Pacotes de dados",
    "tags": [
      "internet"
    ],
    "title": "Internet Vivo 400MB",
    "type": "oneTimeOnly",
    "unit": "MB",
    "xewardsOfferCode": "BZVPM400",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/internet-vivo-400mb/prize-wideBanner.png",
      "srcset": ""
    },
    "group": {
      "name": "internet",
      "title": "Internet Móvel",
      "created": ISODate("2018-05-03T14:28:55.671-0300")
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "keyword": "dados400mb",
    "emoji": "🌐"
  },
  "primepass-duo": {
    "_id": ObjectId("5ae772d855270636b44de8d2"),
    "alliance": {
      "name": "primepass",
      "title": "primepass",
      "xewards": {
        "provider": "null"
      }
    },
    "fullForward": false,
    "name": "primepass-duo",
    "title": "Ingresso de cinema Duo",
    "subTitle": "PrimePass Duo",
    "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
    "faceValue": 40.0,
    "price": 10.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "FFFFFF",
      "backgroundColor": "FFFFFF"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "cinema"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZPRPADU",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
      "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "cinema",
      "title": "Cinema",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "ingresso2",
    "emoji": "🎥"
  },
  "primepass-smart": {
    "_id": ObjectId("5ae7730f55270636b44de8d4"),
    "alliance": {
      "name": "primepass",
      "title": "primepass",
      "xewards": {
        "provider": "null"
      }
    },
    "fullForward": false,
    "name": "primepass-smart",
    "title": "Ingresso de Cinema Smart",
    "subTitle": "Primepass Smart",
    "description": "Aproveite o passaporte mensal e curta filmes por 30 dias na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
    "faceValue": 70.0,
    "price": 17.5,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "cinema"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZPRPASM",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
      "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-smart/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "cinema",
      "title": "Cinema",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "ingresso30",
    "emoji": "🎥",
    "actions": [
      {
        "title": "RESGATAR CUPOM",
        "url": "https://primepass.club/resgate"
      }
    ]
  },
  "r-110-de-desconto-em-academia": {
    "_id": ObjectId("5aff2721ed86f51b5c9f4846"),
    "alliance": {
      "name": "gympass",
      "title": "Gympass",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "r-110-de-desconto-em-academia",
    "title": "R$110 de Desconto em Academia",
    "subTitle": "Cupom de desconto promocional de R$110,00",
    "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$110,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
    "faceValue": 72.0,
    "price": 1.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Coupon",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "academia"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "sercomtel",
      "nextel",
      "ctbc"
    ],
    "xewardsOfferCode": "GYMPPR72",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
      "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-110-de-desconto-em-academia/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "academia",
      "title": "Academia",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "mensal110",
    "emoji": "💪"
  },
  "r-80-de-desconto-em-academia": {
    "_id": ObjectId("5aff26dfed86f519f49f4845"),
    "alliance": {
      "name": "gympass",
      "title": "Gympass",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "r-80-de-desconto-em-academia",
    "title": "R$80 de Desconto em Academia",
    "subTitle": "Cupom de desconto promocional de R$80,00",
    "description": "Yoga, Crossfit ou Musculação? Neste primeiro mês, o valor da sua recompensa virou R$80,00 em desconto na mensalidade de academias e estúdios na Gympass em todo o Brasil. Não perca tempo, e mantenha a malhação em dia!",
    "faceValue": 41.0,
    "price": 1.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Coupon",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "academia"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "GYMPPR41",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
      "forwarded": "Tá quase na hora da mahação. Seu cupom de desconto da Gympass já está a caminho.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse http://bit.ly/Gympassplano, insira o código {{coupon}}, escolha uma modalidade e aproveite.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/r-80-de-desconto-em-academia/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "academia",
      "title": "Academia",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "mensal80",
    "emoji": "💪"
  },
  "revistas-impressas-desconto-de-r-45-00": {
    "_id": ObjectId("5af5ec01422d94ab9ddf482f"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-desconto-de-r-45-00",
    "title": "Desconto R$45 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 45.0,
    "price": 11.25,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM45",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS45 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-45/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-45-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista45",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-desconto-de-r-50-00": {
    "_id": ObjectId("5af5ec92422d948b60df4830"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-desconto-de-r-50-00",
    "title": "Desconto R$50 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 50.0,
    "price": 12.25,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM50",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS50 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-50/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-50-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista50",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-desconto-de-r-60-00": {
    "_id": ObjectId("5af5edb8422d94b410df4831"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-desconto-de-r-60-00",
    "title": "Desconto R$60 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 60.0,
    "price": 15.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM60",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS60 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-60/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-60-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista60",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-desconto-de-r-70-00": {
    "_id": ObjectId("5af5effe422d943b22df4832"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-desconto-de-r-70-00",
    "title": "Desconto R$70 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 70.0,
    "price": 17.5,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM70",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS70 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-70/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-70-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista70",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "revistas-impressas-desconto-de-r-80-00": {
    "_id": ObjectId("5af5f07cc6ca3843e018e89c"),
    "alliance": {
      "name": "editora3",
      "title": "Editora3",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "revistas-impressas-desconto-de-r-80-00",
    "title": "Desconto R$80 - Assinatura de Revista",
    "subTitle": "Assinatura Trimestral de revistas impressas IstoÉ, IstoÉ Dinheiro, entre outras",
    "description": "Leia sua revista preferida de onde estiver a qualquer hora nas versões digitais ou impressas. São diversos títulos da Editora Três, como: IstoÉ, IstoÉ Dinheiro, Dinheiro Rural, Woman Health, Menu e muito mais.",
    "faceValue": 80.0,
    "price": 20.0,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "Assinatura",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [
      "revista"
    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZE3IM80",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "Seu desconto estará disponível em breve.",
      "forwarded": "Seu desconto estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Chegou seu desconto de RS80 da Editora 3. Acesse <sh>https://www.assine3.com.br/parceria3/minutrade-80/identificacao</sh> insira o codigo {{coupon}} faça o cadastro e escolha uma revista.{{/ifvalue}}"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/revistas-impressas-desconto-de-r-80-00/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "shortDescription": "",
    "praiseTexts": [

    ],
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "revista",
      "title": "Revista"
    },
    "keyword": "revista80",
    "emoji": "📰",
    "actions": null,
    "rules": [

    ],
    "xewardsQuantity": null
  },
  "desconto-de-r-80-em-cursos-online": {
    "_id": ObjectId("5ba5561a45a7f0121c5810cf"),
    "actions": [

    ],
    "alliance": {
      "name": "EDUK",
      "title": "EDUK",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "O seu desconto em cursos online estará disponível em breve.",
      "forwarded": "O seu desconto em cursos online estará disponível em breve.",
      "delivered": "",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}O seu desconto em cursos online já está disponível.{{/ifvalue}}"
    },
    "description": "Gastronomia, artesanato, fotografia e muito mais. São mais de 1.300 cursos profissionalizantes, do básico ao avançado.",
    "emoji": "🖥",
    "faceValue": 40.0,
    "group": {
      "name": "curso-digital",
      "title": "Curso Digital"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-80-em-cursos-online/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "keyword": "EduK80",
    "name": "desconto-de-r-80-em-cursos-online",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 10.0,
    "rules": [

    ],
    "shortDescription": "",
    "socialWeight": 0.0,
    "subTitle": "Desconto de R$80 em cursos online",
    "tags": [

    ],
    "title": "Desconto de R$80 em cursos online",
    "type": "oneTimeOnly",
    "unit": "UN",
    "xewardsOfferCode": "BZEDK080",
    "weight": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-80-em-cursos-online/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    }
  },
  "desconto-de-r-120-em-cursos-online": {
    "_id": ObjectId("5ba555d245a7f0121c57db0a"),
    "actions": [

    ],
    "alliance": {
      "name": "EDUK",
      "title": "EDUK",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "O seu desconto em cursos online estará disponível em breve.",
      "forwarded": "O seu desconto em cursos online estará disponível em breve.",
      "delivered": "",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}O seu desconto em cursos online já está disponível.{{/ifvalue}}"
    },
    "description": "Gastronomia, artesanato, fotografia e muito mais. São mais de 1.300 cursos profissionalizantes, do básico ao avançado.",
    "emoji": "🖥",
    "faceValue": 72.0,
    "group": {
      "name": "curso-digital",
      "title": "Curso Digital"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-120-em-cursos-online/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "keyword": "EduK120",
    "name": "desconto-de-r-120-em-cursos-online",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 18.25,
    "rules": [

    ],
    "shortDescription": "",
    "socialWeight": 0.0,
    "subTitle": "Desconto de R$120 em cursos online",
    "tags": [

    ],
    "title": "Desconto de R$120 em cursos online",
    "type": "oneTimeOnly",
    "unit": "UN",
    "xewardsOfferCode": "BZEDK120",
    "weight": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-120-em-cursos-online/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    }
  },
  "desconto-de-r-150-em-cursos-online": {
    "_id": ObjectId("5ba555eb45a7f0121c57edd1"),
    "actions": [

    ],
    "alliance": {
      "name": "EDUK",
      "title": "EDUK",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "O seu desconto em cursos online estará disponível em breve.",
      "forwarded": "O seu desconto em cursos online estará disponível em breve.",
      "delivered": "",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}O seu desconto em cursos online já está disponível.{{/ifvalue}}"
    },
    "description": "Gastronomia, artesanato, fotografia e muito mais. São mais de 1.300 cursos profissionalizantes, do básico ao avançado.",
    "emoji": "🖥",
    "faceValue": 90.0,
    "group": {
      "name": "curso-digital",
      "title": "Curso Digital"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-150-em-cursos-online/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "keyword": "EduK150",
    "name": "desconto-de-r-150-em-cursos-online",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 22.75,
    "rules": [

    ],
    "shortDescription": "",
    "socialWeight": 0.0,
    "subTitle": "Desconto de R$150 em cursos online",
    "tags": [

    ],
    "title": "Desconto de R$150 em cursos online",
    "type": "oneTimeOnly",
    "unit": "UN",
    "xewardsOfferCode": "BZEDK150",
    "weight": 0.0,
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-150-em-cursos-online/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    }
  },
  "combo-digital-entretenimento-presentear-amigo": {
    "_id": ObjectId("5bbfdc8d45a7f0121c97839d"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como par de ingressos de cinema, desconto incrível de um livro personalizado com ele como protagonista e 2 casquinhas do McDonald´s.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-entretenimento-presentear-amigo",
    "operators": [
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-entretenimento/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "primepass",
          "title": "primepass",
          "xewards": {
            "provider": "null"
          }
        },
        "fullForward": false,
        "title": "Ingresso de cinema Duo",
        "subTitle": "PrimePass Duo",
        "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
        "price": 10.0,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "FFFFFF",
          "backgroundColor": "FFFFFF"
        },
        "icon": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "UN",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "cinema"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "xewardsOfferCode": "BZPRPADU",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso2",
        "emoji": "🎥",
        "name": "primepass-duo",
        "faceValue": 20.0,
        "quantity": 2.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$20",
        "subTitle": "Livro infantil personalizado - R$20",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI20",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro20",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-20-00",
        "faceValue": 20.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "bonuz.com",
          "name": "bonuz.com",
          "xewards": {
            "provider": ""
          }
        },
        "title": "Bônus para Presente",
        "subTitle": "Presenteie parte dos seu bônus",
        "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "donation",
        "customBonuz": {
          "backgroundColor": "FFFFFF",
          "color": "FF0000"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [
            {
              "_id": ObjectId("577bf83b6388d40100b2c820"),
              "required": true,
              "example": "5511988888888",
              "text": "Digite o Celular do seu amigo",
              "name": "Celular",
              "field": "mobile"
            }
          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro",
          "oi",
          "tim",
          "vivo",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_prize_list_gift.png",
          "srcset": "img_prize_list_gift.png 1x,img_prize_list_gift@2x.png 2x,img_prize_list_gift@3x.png 3x"
        },
        "deliveryEngine": "bonuz",
        "deliveryInfo": {
          "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "presente",
        "emoji": "🎁",
        "name": "presentearAmigo",
        "faceValue": 1.0,
        "quantity": 6.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-entretenimento-oi": {
    "_id": ObjectId("5bbfdc8d45a7f0121c9783ba"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como par de ingressos de cinema, desconto incrível de um livro personalizado com ele como protagonista e 2 casquinhas do McDonald´s.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-entretenimento-oi",
    "operators": [
      "oi"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-entretenimento/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "primepass",
          "title": "primepass",
          "xewards": {
            "provider": "null"
          }
        },
        "fullForward": false,
        "title": "Ingresso de cinema Duo",
        "subTitle": "PrimePass Duo",
        "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
        "price": 10.0,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "FFFFFF",
          "backgroundColor": "FFFFFF"
        },
        "icon": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "UN",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "cinema"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "xewardsOfferCode": "BZPRPADU",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso2",
        "emoji": "🎥",
        "name": "primepass-duo",
        "faceValue": 20.0,
        "quantity": 2.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$20",
        "subTitle": "Livro infantil personalizado - R$20",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI20",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro20",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-20-00",
        "faceValue": 20.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "oi",
          "title": "Oi",
          "xewards": {
            "provider": "4"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para Oi + Internet + SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "oi"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_oi.png",
          "srcset": "img_logo_lists_small_oi.png 1x,img_logo_lists_small_oi@2x.png 2x,img_logo_lists_small_oi@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularOi",
        "faceValue": 1.0,
        "quantity": 6.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-entretenimento-claro": {
    "_id": ObjectId("5bbfdc8d45a7f0121c9783d9"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como par de ingressos de cinema, desconto incrível de um livro personalizado com ele como protagonista e 2 casquinhas do McDonald´s.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-entretenimento-claro",
    "operators": [
      "claro"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-entretenimento/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "primepass",
          "title": "primepass",
          "xewards": {
            "provider": "null"
          }
        },
        "fullForward": false,
        "title": "Ingresso de cinema Duo",
        "subTitle": "PrimePass Duo",
        "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
        "price": 10.0,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "FFFFFF",
          "backgroundColor": "FFFFFF"
        },
        "icon": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "UN",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "cinema"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "xewardsOfferCode": "BZPRPADU",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso2",
        "emoji": "🎥",
        "name": "primepass-duo",
        "faceValue": 20.0,
        "quantity": 2.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$20",
        "subTitle": "Livro infantil personalizado - R$20",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI20",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro20",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-20-00",
        "faceValue": 20.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "Claro",
          "name": "claro",
          "xewards": {
            "provider": "1"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Claro",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "backgroundColor": "",
          "color": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_claro.png",
          "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularClaro",
        "faceValue": 1.0,
        "quantity": 6.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-entretenimento-tim": {
    "_id": ObjectId("5bbfdc8d45a7f0121c9783f6"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como par de ingressos de cinema, desconto incrível de um livro personalizado com ele como protagonista e 2 casquinhas do McDonald´s.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-entretenimento-tim",
    "operators": [
      "tim"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-entretenimento/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "primepass",
          "title": "primepass",
          "xewards": {
            "provider": "null"
          }
        },
        "fullForward": false,
        "title": "Ingresso de cinema Duo",
        "subTitle": "PrimePass Duo",
        "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
        "price": 10.0,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "FFFFFF",
          "backgroundColor": "FFFFFF"
        },
        "icon": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "UN",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "cinema"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "xewardsOfferCode": "BZPRPADU",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso2",
        "emoji": "🎥",
        "name": "primepass-duo",
        "faceValue": 20.0,
        "quantity": 2.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$20",
        "subTitle": "Livro infantil personalizado - R$20",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI20",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro20",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-20-00",
        "faceValue": 20.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "tim",
          "title": "TIM",
          "xewards": {
            "provider": "2"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para TIM, Internet e SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "tim"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_tim.png",
          "srcset": "img_logo_lists_small_tim.png 1x,img_logo_lists_small_tim@2x.png 2x,img_logo_lists_small_tim@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularTIM",
        "faceValue": 1.0,
        "quantity": 6.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-entretenimento-vivo": {
    "_id": ObjectId("5bbfdc8d45a7f0121c978413"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como par de ingressos de cinema, desconto incrível de um livro personalizado com ele como protagonista e 2 casquinhas do McDonald´s.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-entretenimento-vivo",
    "operators": [
      "vivo"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-entretenimento/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "primepass",
          "title": "primepass",
          "xewards": {
            "provider": "null"
          }
        },
        "fullForward": false,
        "title": "Ingresso de cinema Duo",
        "subTitle": "PrimePass Duo",
        "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
        "price": 10.0,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "FFFFFF",
          "backgroundColor": "FFFFFF"
        },
        "icon": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "UN",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "cinema"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "xewardsOfferCode": "BZPRPADU",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso2",
        "emoji": "🎥",
        "name": "primepass-duo",
        "faceValue": 20.0,
        "quantity": 2.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$20",
        "subTitle": "Livro infantil personalizado - R$20",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI20",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro20",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-20-00",
        "faceValue": 20.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "vivo",
          "title": "Vivo",
          "xewards": {
            "provider": "3"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Vivo",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "f65e01",
          "backgroundColor": "f65e01"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "vivo"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_vivo.png",
          "srcset": "img_logo_lists_small_vivo.png 1x,img_logo_lists_small_vivo@2x.png 2x,img_logo_lists_small_vivo@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularVivo",
        "faceValue": 1.0,
        "quantity": 6.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-cultura-presentear-amigo": {
    "_id": ObjectId("5bbfdc8d45a7f0121c978430"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para ouvir e um livro incrível personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-cultura-presentear-amigo",
    "operators": [
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "actions": [

        ],
        "alliance": {
          "name": "dentrodahistoria",
          "title": "Dentro da História",
          "xewards": {
            "provider": ""
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "delivered": ""
        },
        "description": "Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse pela leitura de forma divertida! Escolha a história preferida, faça o personagem e pronto!",
        "emoji": "📖",
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "keyword": "",
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "praiseTexts": [

        ],
        "price": 15.0,
        "rules": [

        ],
        "shortDescription": "Vale um livro personalizado do Dentro da História.",
        "socialWeight": 1.0,
        "subTitle": "livro personalizado do Dentro da História",
        "tags": [

        ],
        "title": "Livro infantil personalizado",
        "type": "oneTimeOnly",
        "unit": "un",
        "xewardsOfferCode": "BZDDHI60",
        "weight": 1.0,
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "name": "livro-infantil-personalizado",
        "faceValue": 60.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "bonuz.com",
          "name": "bonuz.com",
          "xewards": {
            "provider": ""
          }
        },
        "title": "Bônus para Presente",
        "subTitle": "Presenteie parte dos seu bônus",
        "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "donation",
        "customBonuz": {
          "backgroundColor": "FFFFFF",
          "color": "FF0000"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [
            {
              "_id": ObjectId("577bf83b6388d40100b2c820"),
              "required": true,
              "example": "5511988888888",
              "text": "Digite o Celular do seu amigo",
              "name": "Celular",
              "field": "mobile"
            }
          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro",
          "oi",
          "tim",
          "vivo",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_prize_list_gift.png",
          "srcset": "img_prize_list_gift.png 1x,img_prize_list_gift@2x.png 2x,img_prize_list_gift@3x.png 3x"
        },
        "deliveryEngine": "bonuz",
        "deliveryInfo": {
          "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "presente",
        "emoji": "🎁",
        "name": "presentearAmigo",
        "faceValue": 1.0,
        "quantity": 5.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-cultura-claro": {
    "_id": ObjectId("5bbfdc8d45a7f0121c97844d"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para ouvir e um livro incrível personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-cultura-claro",
    "operators": [
      "claro"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "actions": [

        ],
        "alliance": {
          "name": "dentrodahistoria",
          "title": "Dentro da História",
          "xewards": {
            "provider": ""
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "delivered": ""
        },
        "description": "Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse pela leitura de forma divertida! Escolha a história preferida, faça o personagem e pronto!",
        "emoji": "📖",
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "keyword": "",
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "praiseTexts": [

        ],
        "price": 15.0,
        "rules": [

        ],
        "shortDescription": "Vale um livro personalizado do Dentro da História.",
        "socialWeight": 1.0,
        "subTitle": "livro personalizado do Dentro da História",
        "tags": [

        ],
        "title": "Livro infantil personalizado",
        "type": "oneTimeOnly",
        "unit": "un",
        "xewardsOfferCode": "BZDDHI60",
        "weight": 1.0,
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "name": "livro-infantil-personalizado",
        "faceValue": 60.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "Claro",
          "name": "claro",
          "xewards": {
            "provider": "1"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Claro",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "backgroundColor": "",
          "color": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_claro.png",
          "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularClaro",
        "faceValue": 1.0,
        "quantity": 5.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-cultura-oi": {
    "_id": ObjectId("5bbfdc8d45a7f0121c978474"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para ouvir e um livro incrível personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-cultura-oi",
    "operators": [
      "oi"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "actions": [

        ],
        "alliance": {
          "name": "dentrodahistoria",
          "title": "Dentro da História",
          "xewards": {
            "provider": ""
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "delivered": ""
        },
        "description": "Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse pela leitura de forma divertida! Escolha a história preferida, faça o personagem e pronto!",
        "emoji": "📖",
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "keyword": "",
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "praiseTexts": [

        ],
        "price": 15.0,
        "rules": [

        ],
        "shortDescription": "Vale um livro personalizado do Dentro da História.",
        "socialWeight": 1.0,
        "subTitle": "livro personalizado do Dentro da História",
        "tags": [

        ],
        "title": "Livro infantil personalizado",
        "type": "oneTimeOnly",
        "unit": "un",
        "xewardsOfferCode": "BZDDHI60",
        "weight": 1.0,
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "name": "livro-infantil-personalizado",
        "faceValue": 60.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "oi",
          "title": "Oi",
          "xewards": {
            "provider": "4"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para Oi + Internet + SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "oi"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_oi.png",
          "srcset": "img_logo_lists_small_oi.png 1x,img_logo_lists_small_oi@2x.png 2x,img_logo_lists_small_oi@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularOi",
        "faceValue": 1.0,
        "quantity": 5.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-cultura-tim": {
    "_id": ObjectId("5bbfdc8d45a7f0121c978493"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para ouvir e um livro incrível personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-cultura-tim",
    "operators": [
      "tim"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "actions": [

        ],
        "alliance": {
          "name": "dentrodahistoria",
          "title": "Dentro da História",
          "xewards": {
            "provider": ""
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "delivered": ""
        },
        "description": "Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse pela leitura de forma divertida! Escolha a história preferida, faça o personagem e pronto!",
        "emoji": "📖",
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "keyword": "",
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "praiseTexts": [

        ],
        "price": 15.0,
        "rules": [

        ],
        "shortDescription": "Vale um livro personalizado do Dentro da História.",
        "socialWeight": 1.0,
        "subTitle": "livro personalizado do Dentro da História",
        "tags": [

        ],
        "title": "Livro infantil personalizado",
        "type": "oneTimeOnly",
        "unit": "un",
        "xewardsOfferCode": "BZDDHI60",
        "weight": 1.0,
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "name": "livro-infantil-personalizado",
        "faceValue": 60.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "tim",
          "title": "TIM",
          "xewards": {
            "provider": "2"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para TIM, Internet e SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "tim"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_tim.png",
          "srcset": "img_logo_lists_small_tim.png 1x,img_logo_lists_small_tim@2x.png 2x,img_logo_lists_small_tim@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularTIM",
        "faceValue": 1.0,
        "quantity": 5.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-cultura-vivo": {
    "_id": ObjectId("5bbfdc8d45a7f0121c9784b2"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para ouvir e um livro incrível personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 72.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-cultura-vivo",
    "operators": [
      "vivo"
    ],
    "praiseTexts": [

    ],
    "price": 21.599999999999998,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "actions": [

        ],
        "alliance": {
          "name": "dentrodahistoria",
          "title": "Dentro da História",
          "xewards": {
            "provider": ""
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "delivered": ""
        },
        "description": "Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse pela leitura de forma divertida! Escolha a história preferida, faça o personagem e pronto!",
        "emoji": "📖",
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "keyword": "",
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "praiseTexts": [

        ],
        "price": 15.0,
        "rules": [

        ],
        "shortDescription": "Vale um livro personalizado do Dentro da História.",
        "socialWeight": 1.0,
        "subTitle": "livro personalizado do Dentro da História",
        "tags": [

        ],
        "title": "Livro infantil personalizado",
        "type": "oneTimeOnly",
        "unit": "un",
        "xewardsOfferCode": "BZDDHI60",
        "weight": 1.0,
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "name": "livro-infantil-personalizado",
        "faceValue": 60.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "vivo",
          "title": "Vivo",
          "xewards": {
            "provider": "3"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Vivo",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "f65e01",
          "backgroundColor": "f65e01"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "vivo"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_vivo.png",
          "srcset": "img_logo_lists_small_vivo.png 1x,img_logo_lists_small_vivo@2x.png 2x,img_logo_lists_small_vivo@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularVivo",
        "faceValue": 1.0,
        "quantity": 5.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-estilo-entretenimento": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978360"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como um livro incrível personalizado com ele como protagonista, 3 ingressos de cinema e 2 casquinhas do McDonald´s.",
    "emoji": "",
    "faceValue": 90.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-estilo-entretenimento",
    "operators": [
      "nextel",
      "sercomtel",
      "ctbc",
      "oi",
      "claro",
      "tim",
      "vivo"
    ],
    "praiseTexts": [

    ],
    "price": 27.0,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital-estilo/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-estilo-entretenimento/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "primepass",
          "title": "primepass",
          "xewards": {
            "provider": "null"
          }
        },
        "fullForward": false,
        "title": "Ingresso de cinema Duo",
        "subTitle": "PrimePass Duo",
        "description": "Aproveite um par de ingressos para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass. Válido para Cinépolis, Moviecom, PlayArte, Cinesystem, Centerplex e Espaço Itaú.",
        "price": 10.0,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "FFFFFF",
          "backgroundColor": "FFFFFF"
        },
        "icon": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "UN",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "cinema"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "xewardsOfferCode": "BZPRPADU",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-duo/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso2",
        "emoji": "🎥",
        "name": "primepass-duo",
        "faceValue": 20.0,
        "quantity": 2.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$20",
        "subTitle": "Livro infantil personalizado - R$20",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI20",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-20-00/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro20",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-20-00",
        "faceValue": 20.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Ingresso cinema",
        "subTitle": "Ingresso cinema",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZPRPA01",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "cultura",
          "cinema"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 6.0,
        "price": 7.2,
        "description": "Ingresso de cinema. Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass.",
        "fullForward": false,
        "alliance": {
          "xewards": {
            "provider": "2016"
          },
          "title": "primepass",
          "name": "primepass"
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso",
        "emoji": "🎥",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://primepass.club/resgate"
          }
        ],
        "name": "primepass-oneshot",
        "faceValue": 24.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      }
    ]
  },
  "combo-digital-estilo-cultura": {
    "_id": ObjectId("5bbfdc8d45a7f0121c978380"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como um livro incrível personalizado com ele como protagonista, um mês de assinatura para assistir filmes e séries e também diversas historinhas para ouvir.",
    "emoji": "",
    "faceValue": 90.0,
    "group": null,
    "keyword": "",
    "name": "combo-digital-estilo-cultura",
    "operators": [
      "nextel",
      "sercomtel",
      "ctbc",
      "oi",
      "claro",
      "tim",
      "vivo"
    ],
    "praiseTexts": [

    ],
    "price": 27.0,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-combo-digital-estilo/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/combo-digital-estilo-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "actions": [

        ],
        "alliance": {
          "name": "dentrodahistoria",
          "title": "Dentro da História",
          "xewards": {
            "provider": ""
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "delivered": ""
        },
        "description": "Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse pela leitura de forma divertida! Escolha a história preferida, faça o personagem e pronto!",
        "emoji": "📖",
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "keyword": "",
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "praiseTexts": [

        ],
        "price": 15.0,
        "rules": [

        ],
        "shortDescription": "Vale um livro personalizado do Dentro da História.",
        "socialWeight": 1.0,
        "subTitle": "livro personalizado do Dentro da História",
        "tags": [

        ],
        "title": "Livro infantil personalizado",
        "type": "oneTimeOnly",
        "unit": "un",
        "xewardsOfferCode": "BZDDHI60",
        "weight": 1.0,
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets.bonuz.com/public/prizes/livro-infantil-personalizado/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "name": "livro-infantil-personalizado",
        "faceValue": 60.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "LOOKE",
          "title": "LOOKE",
          "xewards": {
            "provider": ""
          }
        },
        "fullForward": false,
        "title": "Filmes e séries na Looke por 30 dias",
        "subTitle": "Filmes e séries na Looke por 30 dias",
        "description": "Em casa, no caminho ou onde quiser. Aproveite os melhores filmes e séries por 30 dias.",
        "price": 5.75,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "Assinatura",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "entretenimento"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZLOOK30",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "done": ""
        },
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "group": {
          "name": "entretenimento",
          "title": "Entretenimento",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "",
        "emoji": "",
        "weight": 0.0,
        "socialWeight": 0.0,
        "name": "filmes-e-series-na-looke-por-30-dias",
        "faceValue": 23.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      }
    ]
  },
  "desconto-de-r-5-00-em-3-corridas": {
    "_id": ObjectId("5bb6600545a7f0121ce47025"),
    "alliance": {
      "name": "CABIFY",
      "title": "Cabify",
      "xewards": {
        "provider": ""
      }
    },
    "fullForward": false,
    "name": "desconto-de-r-5-00-em-3-corridas",
    "title": "Desconto de R$5,00 em 3 corridas",
    "subTitle": "Desconto de R$5,00 em 3 corridas",
    "description": "Com Cabify você ganha R$5,00 de desconto em 3 corridas nas principais cidades do país.",
    "faceValue": 15.0,
    "price": 3.75,
    "deliveryCost": 0.0,
    "catalog": true,
    "type": "oneTimeOnly",
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-de-r-5-00-em-3-corridas/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "unit": "UN",
    "deliveryAddress": {
      "questions": [

      ]
    },
    "tags": [

    ],
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "xewardsOfferCode": "BZCA4C15",
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "done": ""
    },
    "rules": [
      {
        "name": "ddd",
        "value": [
          11.0,
          13.0,
          19.0,
          21.0,
          51.0,
          31.0,
          41.0,
          61.0
        ]
      }
    ],
    "wideBanner": {
      "href": "",
      "srcset": ""
    },
    "weight": 0.0,
    "socialWeight": 0.0
  },
  "desconto-em-farmacia-90-dias": {
    "_id": ObjectId("5afc45f5f92c2a6034e58fb5"),
    "alliance": {
      "name": "orizon",
      "title": "Orizon",
      "xewards": {
        "provider": "2012"
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [
        {
          "example": "01234578990",
          "text": "Digite o seu CPF",
          "name": "CPF",
          "field": "cpf",
          "required": true
        }
      ],
      "deliveryRecipientField": "cpf"
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "xewards",
    "deliveryInfo": {
      "created": "O seu desconto em medicamentos estará disponível em breve.",
      "forwarded": "O seu desconto em medicamentos estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}O seu desconto em medicamentos já pode ser utilizado e tem validade de 6 meses. Aproveite nas melhores farmácias!{{/ifvalue}}"
    },
    "description": "Descontos de medicamentos em farmácias credenciadas.",
    "faceValue": null,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-90-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "desconto-em-farmacia-90-dias",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 1.0,
    "shortDescription": "Compre nas melhores farmácias com ótimos descontos que só a Orizon oferece!",
    "subTitle": "Desconto em farmácia",
    "tags": [
      "saude"
    ],
    "title": "Desconto em farmácias",
    "type": "priceless",
    "unit": "1",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-90-dias/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "2012",
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "saude",
      "title": "Saúde",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "farmacia",
    "emoji": "🏥",
    "actions": [
      {
        "title": "BAIXAR O APP",
        "url": "http://onelink.to/jhfydg"
      }
    ]
  },
  "troca-facil-i-cultura-presentear-amigo": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978201"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para escutar, casquinha McDonald´s e um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 24.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-i-cultura-presentear-amigo",
    "operators": [
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 7.199999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-i-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "bonuz.com",
          "name": "bonuz.com",
          "xewards": {
            "provider": ""
          }
        },
        "title": "Bônus para Presente",
        "subTitle": "Presenteie parte dos seu bônus",
        "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "donation",
        "customBonuz": {
          "backgroundColor": "FFFFFF",
          "color": "FF0000"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [
            {
              "_id": ObjectId("577bf83b6388d40100b2c820"),
              "required": true,
              "example": "5511988888888",
              "text": "Digite o Celular do seu amigo",
              "name": "Celular",
              "field": "mobile"
            }
          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro",
          "oi",
          "tim",
          "vivo",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_prize_list_gift.png",
          "srcset": "img_prize_list_gift.png 1x,img_prize_list_gift@2x.png 2x,img_prize_list_gift@3x.png 3x"
        },
        "deliveryEngine": "bonuz",
        "deliveryInfo": {
          "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "presente",
        "emoji": "🎁",
        "name": "presentearAmigo",
        "faceValue": 1.0,
        "quantity": 4.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-i-cultura-claro": {
    "_id": ObjectId("5bbfdc8c45a7f0121c97821a"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para escutar, casquinha McDonald´s e um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 24.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-i-cultura-claro",
    "operators": [
      "claro"
    ],
    "praiseTexts": [

    ],
    "price": 7.199999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-i-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "Claro",
          "name": "claro",
          "xewards": {
            "provider": "1"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Claro",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "backgroundColor": "",
          "color": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_claro.png",
          "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularClaro",
        "faceValue": 1.0,
        "quantity": 4.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-i-cultura-tim": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978238"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para escutar, casquinha McDonald´s e um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 24.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-i-cultura-tim",
    "operators": [
      "tim"
    ],
    "praiseTexts": [

    ],
    "price": 7.199999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-i-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "tim",
          "title": "TIM",
          "xewards": {
            "provider": "2"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para TIM, Internet e SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "tim"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_tim.png",
          "srcset": "img_logo_lists_small_tim.png 1x,img_logo_lists_small_tim@2x.png 2x,img_logo_lists_small_tim@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularTIM",
        "faceValue": 1.0,
        "quantity": 4.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-i-cultura-oi": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978257"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para escutar, casquinha McDonald´s e um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 24.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-i-cultura-oi",
    "operators": [
      "oi"
    ],
    "praiseTexts": [

    ],
    "price": 7.199999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-i-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "oi",
          "title": "Oi",
          "xewards": {
            "provider": "4"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para Oi + Internet + SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "oi"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_oi.png",
          "srcset": "img_logo_lists_small_oi.png 1x,img_logo_lists_small_oi@2x.png 2x,img_logo_lists_small_oi@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularOi",
        "faceValue": 1.0,
        "quantity": 4.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-i-cultura-vivo": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978277"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como historinhas para escutar, casquinha McDonald´s e um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 24.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-i-cultura-vivo",
    "operators": [
      "vivo"
    ],
    "praiseTexts": [

    ],
    "price": 7.199999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-i/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-i-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "vivo",
          "title": "Vivo",
          "xewards": {
            "provider": "3"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Vivo",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "f65e01",
          "backgroundColor": "f65e01"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "vivo"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_vivo.png",
          "srcset": "img_logo_lists_small_vivo.png 1x,img_logo_lists_small_vivo@2x.png 2x,img_logo_lists_small_vivo@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularVivo",
        "faceValue": 1.0,
        "quantity": 4.0,
        "comboType": "deal"
      }
    ]
  },
  "desconto-em-farmacia-180-dias": {
    "_id": ObjectId("5afc4632f92c2acafce58fb6"),
    "alliance": {
      "name": "orizon",
      "title": "Orizon",
      "xewards": {
        "provider": "2012"
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [
        {
          "example": "01234578990",
          "text": "Digite o seu CPF",
          "name": "CPF",
          "field": "cpf",
          "required": true
        }
      ],
      "deliveryRecipientField": "cpf"
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "coupon",
    "deliveryInfo": {
      "created": "O seu desconto em medicamentos estará disponível em breve.",
      "forwarded": "O seu desconto em medicamentos estará disponível em breve.",
      "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}O seu desconto em medicamentos já pode ser utilizado e tem validade de 6 meses. Aproveite nas melhores farmácias!{{/ifvalue}}"
    },
    "description": "Descontos de medicamentos em farmácias credenciadas.",
    "faceValue": null,
    "fullForward": false,
    "icon": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-180-dias/prize-icon.png",
      "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
    },
    "name": "desconto-em-farmacia-180-dias",
    "operators": [
      "vivo",
      "claro",
      "tim",
      "oi",
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "price": 1.0,
    "shortDescription": "Compre nas melhores farmácias com ótimos descontos que só a Orizon oferece!",
    "subTitle": "Desconto em farmácia",
    "tags": [
      "saude"
    ],
    "title": "Desconto em farmácias",
    "type": "priceless",
    "unit": "1",
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/desconto-em-farmacia-180-dias/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsOfferCode": "2012",
    "weight": 0.0,
    "socialWeight": 0.0,
    "group": {
      "name": "saude",
      "title": "Saúde",
      "created": ISODate("2018-07-05T10:00:00.000-0300")
    },
    "keyword": "farmacia",
    "emoji": "🏥",
    "actions": [
      {
        "title": "BAIXAR O APP",
        "url": "http://onelink.to/jhfydg"
      }
    ]
  },
  "troca-facil-ii-entretenimento": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978297"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como ingresso de cinema, historinhas para ouvir e um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 41.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-ii-entretenimento",
    "operators": [
      "nextel",
      "sercomtel",
      "ctbc",
      "oi",
      "claro",
      "tim",
      "vivo"
    ],
    "praiseTexts": [

    ],
    "price": 12.299999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Diversão",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-ii-entretenimento/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "title": "Ingresso cinema",
        "subTitle": "Ingresso cinema",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZPRPA01",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "forwarded": "Em breve seu voucher estará disponível pra você curtir aquele cineminha.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse primepass.club/resgate e insira o código {{voucher}}.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "cultura",
          "cinema"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "srcset": "prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x",
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-icon.png"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 6.0,
        "price": 7.2,
        "description": "Ingresso de cinema. Aproveite para curtir um filme na rede credenciada mais perto de você. As melhores salas e os melhores filmes estão no Primepass.",
        "fullForward": false,
        "alliance": {
          "xewards": {
            "provider": "2016"
          },
          "title": "primepass",
          "name": "primepass"
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "https://s3.sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/primepass-oneshot/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              63.0,
              65.0,
              67.0,
              68.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "cinema",
          "title": "Cinema",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ingresso",
        "emoji": "🎥",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://primepass.club/resgate"
          }
        ],
        "name": "primepass-oneshot",
        "faceValue": 24.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "ubook",
          "title": "Ubook",
          "xewards": {
            "provider": "2011"
          }
        },
        "catalog": true,
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "deliveryAddress": {
          "questions": [

          ]
        },
        "deliveryCost": 0.0,
        "deliveryEngine": "xewards",
        "xewardsOfferCode": "BZUBK001",
        "deliveryInfo": {
          "created": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "forwarded": "Em breve você receberá seu código para acessar a Ubook Kids.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse ubook.com/bonuz e use o código {{voucherCode}} para escutar seus livros preferidos onde quiser.{{/ifvalue}}"
        },
        "description": "Quadrinhos, contos de fadas, heróis e muito mais em uma plataforma pensada especialmente para os pequenos. Com Ubook Kids, as crianças se divertem ouvindo suas histórias prediletas.",
        "fullForward": false,
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "price": 2.4,
        "subTitle": "Assinatura Ubook Kids de 30 dias",
        "tags": [
          "audio-livro-infantil"
        ],
        "title": "Audiolivro Infantis - 30 dias",
        "type": "oneTimeOnly",
        "unit": "Assinatura",
        "wideBanner": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/audiolivro-infantis-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "audio-livro-infantil",
          "title": "Áudio Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "ubkids",
        "emoji": "🎧",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.ubook.com/bonuz"
          }
        ],
        "name": "audiolivro-infantis-30-dias",
        "faceValue": 7.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-ii-cultura-presentear-amigo": {
    "_id": ObjectId("5bbfdc8c45a7f0121c9782c9"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como assinatura mensal para assistir filmes e séries, duas saborosas casquinha McDonald´s um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 41.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-ii-cultura-presentear-amigo",
    "operators": [
      "nextel",
      "sercomtel",
      "ctbc"
    ],
    "praiseTexts": [

    ],
    "price": 12.299999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-ii-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "LOOKE",
          "title": "LOOKE",
          "xewards": {
            "provider": ""
          }
        },
        "fullForward": false,
        "title": "Filmes e séries na Looke por 30 dias",
        "subTitle": "Filmes e séries na Looke por 30 dias",
        "description": "Em casa, no caminho ou onde quiser. Aproveite os melhores filmes e séries por 30 dias.",
        "price": 5.75,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "Assinatura",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "entretenimento"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZLOOK30",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "done": ""
        },
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "group": {
          "name": "entretenimento",
          "title": "Entretenimento",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "",
        "emoji": "",
        "weight": 0.0,
        "socialWeight": 0.0,
        "name": "filmes-e-series-na-looke-por-30-dias",
        "faceValue": 23.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "bonuz.com",
          "name": "bonuz.com",
          "xewards": {
            "provider": ""
          }
        },
        "title": "Bônus para Presente",
        "subTitle": "Presenteie parte dos seu bônus",
        "description": "Faça uma surpresa para quem você gosta! Você pode presentear seu bônus celular e deixar o dia de alguém mais feliz. Escolha um contato na sua agenda para enviar o presente, é simples e rápido.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "donation",
        "customBonuz": {
          "backgroundColor": "FFFFFF",
          "color": "FF0000"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [
            {
              "_id": ObjectId("577bf83b6388d40100b2c820"),
              "required": true,
              "example": "5511988888888",
              "text": "Digite o Celular do seu amigo",
              "name": "Celular",
              "field": "mobile"
            }
          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro",
          "oi",
          "tim",
          "vivo",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_prize_list_gift.png",
          "srcset": "img_prize_list_gift.png 1x,img_prize_list_gift@2x.png 2x,img_prize_list_gift@3x.png 3x"
        },
        "deliveryEngine": "bonuz",
        "deliveryInfo": {
          "created": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "forwarded": "Em breve seu presente de créditos em bônus celular estará disponível para seu amigo.",
          "done": "{{#ifvalue status value='canceled'}}Ops, isso não era pra ter acontecido! Por favor entre em contato conosco através do menu Ajuda.{{else ifvalue status value='delivered'}}Uma nova recompensa foi criada para o número: {{targetMobileFormatted}}{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "presente",
        "emoji": "🎁",
        "name": "presentearAmigo",
        "faceValue": 1.0,
        "quantity": 2.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-ii-cultura-claro": {
    "_id": ObjectId("5bbfdc8c45a7f0121c9782e8"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como assinatura mensal para assistir filmes e séries, duas saborosas casquinha McDonald´s um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 41.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-ii-cultura-claro",
    "operators": [
      "claro"
    ],
    "praiseTexts": [

    ],
    "price": 12.299999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-ii-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "LOOKE",
          "title": "LOOKE",
          "xewards": {
            "provider": ""
          }
        },
        "fullForward": false,
        "title": "Filmes e séries na Looke por 30 dias",
        "subTitle": "Filmes e séries na Looke por 30 dias",
        "description": "Em casa, no caminho ou onde quiser. Aproveite os melhores filmes e séries por 30 dias.",
        "price": 5.75,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "Assinatura",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "entretenimento"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZLOOK30",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "done": ""
        },
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "group": {
          "name": "entretenimento",
          "title": "Entretenimento",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "",
        "emoji": "",
        "weight": 0.0,
        "socialWeight": 0.0,
        "name": "filmes-e-series-na-looke-por-30-dias",
        "faceValue": 23.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "title": "Claro",
          "name": "claro",
          "xewards": {
            "provider": "1"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Claro",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais e SMS para números da e fixos de mesmo DDD.Pós-Pago: Bônus válido até o final do mês da concessão para chamadas locais e SMS para números da Claro e fixos de mesmo DDD.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "backgroundColor": "",
          "color": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "claro"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_claro.png",
          "srcset": "img_logo_lists_small_claro.png 1x,img_logo_lists_small_claro@2x.png 2x,img_logo_lists_small_claro@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularClaro",
        "faceValue": 1.0,
        "quantity": 2.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-ii-cultura-oi": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978306"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como assinatura mensal para assistir filmes e séries, duas saborosas casquinha McDonald´s um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 41.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-ii-cultura-oi",
    "operators": [
      "oi"
    ],
    "praiseTexts": [

    ],
    "price": 12.299999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-ii-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "LOOKE",
          "title": "LOOKE",
          "xewards": {
            "provider": ""
          }
        },
        "fullForward": false,
        "title": "Filmes e séries na Looke por 30 dias",
        "subTitle": "Filmes e séries na Looke por 30 dias",
        "description": "Em casa, no caminho ou onde quiser. Aproveite os melhores filmes e séries por 30 dias.",
        "price": 5.75,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "Assinatura",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "entretenimento"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZLOOK30",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "done": ""
        },
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "group": {
          "name": "entretenimento",
          "title": "Entretenimento",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "",
        "emoji": "",
        "weight": 0.0,
        "socialWeight": 0.0,
        "name": "filmes-e-series-na-looke-por-30-dias",
        "faceValue": 23.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "oi",
          "title": "Oi",
          "xewards": {
            "provider": "4"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para Oi + Internet + SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos Pré-pagos e Controle de pessoa física.Bônus válido por 30 dias para chamadas locais para Oi Móvel, Oi fixo de mesmo DDD, internet e SMS.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "oi"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_oi.png",
          "srcset": "img_logo_lists_small_oi.png 1x,img_logo_lists_small_oi@2x.png 2x,img_logo_lists_small_oi@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularOi",
        "faceValue": 1.0,
        "quantity": 2.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-ii-cultura-tim": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978323"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como assinatura mensal para assistir filmes e séries, duas saborosas casquinha McDonald´s um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 41.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-ii-cultura-tim",
    "operators": [
      "tim"
    ],
    "praiseTexts": [

    ],
    "price": 12.299999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-ii-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "LOOKE",
          "title": "LOOKE",
          "xewards": {
            "provider": ""
          }
        },
        "fullForward": false,
        "title": "Filmes e séries na Looke por 30 dias",
        "subTitle": "Filmes e séries na Looke por 30 dias",
        "description": "Em casa, no caminho ou onde quiser. Aproveite os melhores filmes e séries por 30 dias.",
        "price": 5.75,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "Assinatura",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "entretenimento"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZLOOK30",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "done": ""
        },
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "group": {
          "name": "entretenimento",
          "title": "Entretenimento",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "",
        "emoji": "",
        "weight": 0.0,
        "socialWeight": 0.0,
        "name": "filmes-e-series-na-looke-por-30-dias",
        "faceValue": 23.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "tim",
          "title": "TIM",
          "xewards": {
            "provider": "2"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para TIM, Internet e SMS",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos e pessoa física dos planos Infinity Pré, Infinity Controle, Liberty Controle e Liberty Controle Express.Bônus válido por 30 dias para chamadas locais para TIM Móvel e TIM Fixo, DDD com o CSP 41, SMS e internet.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "",
          "backgroundColor": ""
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "tim"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_tim.png",
          "srcset": "img_logo_lists_small_tim.png 1x,img_logo_lists_small_tim@2x.png 2x,img_logo_lists_small_tim@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularTIM",
        "faceValue": 1.0,
        "quantity": 2.0,
        "comboType": "deal"
      }
    ]
  },
  "troca-facil-ii-cultura-vivo": {
    "_id": ObjectId("5bbfdc8c45a7f0121c978340"),
    "actions": [

    ],
    "alliance": {
      "name": "bonuz.com",
      "title": "bonuz.com",
      "xewards": {
        "provider": ""
      }
    },
    "catalog": true,
    "customBonuz": {
      "color": "ffffff",
      "backgroundColor": "ffffff"
    },
    "deliveryAddress": {
      "questions": [

      ]
    },
    "deliveryCost": 0.0,
    "deliveryEngine": "bonuz",
    "deliveryInfo": {
      "created": "",
      "forwarded": "",
      "delivered": ""
    },
    "description": "O mês das crianças é muito especial. E que tal aproveitar para presentear um pequeno com recompensas super divertidas como assinatura mensal para assistir filmes e séries, duas saborosas casquinha McDonald´s um desconto incrível de um livro personalizado com ele como protagonista.",
    "emoji": "",
    "faceValue": 41.0,
    "group": null,
    "keyword": "",
    "name": "troca-facil-ii-cultura-vivo",
    "operators": [
      "vivo"
    ],
    "praiseTexts": [

    ],
    "price": 12.299999999999999,
    "rules": [

    ],
    "shortDescription": "Dia das Crianças Banco do Brasil",
    "socialWeight": 1.0,
    "subTitle": "Dia das Crianças",
    "tags": [

    ],
    "title": "Cultura",
    "type": "oneTimeOnly",
    "unit": "un",
    "xewardsOfferCode": "",
    "weight": 1.0,
    "icon": {
      "srcset": "icon.png 1x, icon@1.5x.png 1.5x, icon@2x.png 2x, icon@3x.png 3x, icon@4x.png 4x",
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/experiences/bb-troca-facil-ii/icon.png"
    },
    "wideBanner": {
      "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/troca-facil-ii-cultura/prize-wideBanner.png",
      "srcset": "prize-wideBanner.png 1x, prize-wideBanner@1.5x.png 1.5x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
    },
    "xewardsQuantity": null,
    "comboPrizes": [
      {
        "alliance": {
          "name": "LOOKE",
          "title": "LOOKE",
          "xewards": {
            "provider": ""
          }
        },
        "fullForward": false,
        "title": "Filmes e séries na Looke por 30 dias",
        "subTitle": "Filmes e séries na Looke por 30 dias",
        "description": "Em casa, no caminho ou onde quiser. Aproveite os melhores filmes e séries por 30 dias.",
        "price": 5.75,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "oneTimeOnly",
        "customBonuz": {
          "color": "ffffff",
          "backgroundColor": "ffffff"
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "unit": "Assinatura",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "entretenimento"
        ],
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "ctbc",
          "sercomtel"
        ],
        "xewardsOfferCode": "BZLOOK30",
        "deliveryEngine": "coupon",
        "deliveryInfo": {
          "created": "",
          "forwarded": "",
          "done": ""
        },
        "wideBanner": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/filmes-e-series-na-looke-por-30-dias/prize-wideBanner.png",
          "srcset": "prize-wideBanner.png 1x, prize-wideBanner@2x.png 2x, prize-wideBanner@2x.png 2x, prize-wideBanner@3x.png 3x, prize-wideBanner@4x.png 4x"
        },
        "group": {
          "name": "entretenimento",
          "title": "Entretenimento",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "",
        "emoji": "",
        "weight": 0.0,
        "socialWeight": 0.0,
        "name": "filmes-e-series-na-looke-por-30-dias",
        "faceValue": 23.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Casquinha McDonald´s",
        "subTitle": "Uma casquinha McDonald´s",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "Unidade",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZMCC001",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Sua casquinha estará disponível em breve.",
          "forwarded": "Sua casquinha estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Humm! Sua casquinha já está disponível.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "cultura",
          "fast-food"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3-sa-east-1.amazonaws.com/assets.bonuz.com/public/prizes/casquinha-mcdonald-s/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "df1a23",
          "color": "df1a23"
        },
        "deliveryCost": 0.0,
        "price": 0.9,
        "description": "Esse mês você pode saborear uma casquinha do McDonald’s. Escolha essa delícia de recompensa e receba uma mensagem com seu cupom. Depois, basta apresentá-lo em um dos restaurantes participantes, disponíveis em www.mcdonalds.com.br e pronto!",
        "fullForward": false,
        "alliance": {
          "title": "McDonald's",
          "name": "mcdonalds",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              27.0,
              31.0,
              34.0,
              35.0,
              41.0,
              43.0,
              47.0,
              48.0,
              51.0,
              53.0,
              54.0,
              61.0,
              62.0,
              65.0,
              69.0,
              71.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              91.0,
              92.0,
              98.0
            ]
          }
        ],
        "group": {
          "name": "fast-food",
          "title": "Fast Food",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "casquinha",
        "emoji": "🍦",
        "actions": [
          {
            "title": "ABRIR CUPOM",
            "url": "https://casquinha.bonuz.com/{{coupon.code}}"
          }
        ],
        "name": "casquinha-mcdonalds",
        "faceValue": 3.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "title": "Livro infantil personalizado - R$10",
        "subTitle": "Livro infantil personalizado - R$10",
        "catalog": true,
        "type": "oneTimeOnly",
        "unit": "voucher",
        "deliveryEngine": "coupon",
        "xewardsOfferCode": "BZDDHI10",
        "comboPrizes": [

        ],
        "deliveryInfo": {
          "created": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "forwarded": "Seu desconto em um livro infantil personalizado estará disponível em breve.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Acesse https://goo.gl/vu9dCt, escolha seu livro e insira o código {{coupon}} para usar seu desconto.{{/ifvalue}}"
        },
        "shouldQueryStatus": false,
        "operators": [
          "vivo",
          "claro",
          "tim",
          "oi",
          "nextel",
          "sercomtel",
          "ctbc"
        ],
        "tags": [
          "kids",
          "livro-infantil"
        ],
        "deliveryAddress": {
          "questions": [

          ]
        },
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/dentro-da-historia-r-10-00/prize-icon.png",
          "srcset": "prize-icon.png 1x, prize-icon@1.5x.png 1.5x, prize-icon@2x.png 2x, prize-icon@3x.png 3x, prize-icon@4x.png 4x"
        },
        "customBonuz": {
          "backgroundColor": "ffffff",
          "color": "ffffff"
        },
        "deliveryCost": 0.0,
        "price": 0.0,
        "description": "Desconto em um livro infantil personalizado. Torne seu pequeno protagonista de histórias incríveis como a Turma da Mônica, Show da Luna e muito mais. No Dentro da História, você desperta o interesse dele pela leitura de forma divertida!",
        "fullForward": false,
        "alliance": {
          "title": "Dentro da História",
          "name": "dentrodahistoria",
          "xewards": {
            "provider": ""
          }
        },
        "__v": 0.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "rules": [
          {
            "name": "ddd",
            "value": [
              11.0,
              12.0,
              13.0,
              14.0,
              15.0,
              16.0,
              17.0,
              18.0,
              19.0,
              21.0,
              22.0,
              24.0,
              27.0,
              28.0,
              31.0,
              32.0,
              33.0,
              34.0,
              35.0,
              37.0,
              38.0,
              41.0,
              42.0,
              43.0,
              44.0,
              45.0,
              46.0,
              47.0,
              48.0,
              49.0,
              51.0,
              53.0,
              54.0,
              55.0,
              61.0,
              62.0,
              63.0,
              64.0,
              65.0,
              66.0,
              67.0,
              68.0,
              69.0,
              71.0,
              73.0,
              74.0,
              75.0,
              77.0,
              79.0,
              81.0,
              82.0,
              83.0,
              84.0,
              85.0,
              86.0,
              87.0,
              88.0,
              89.0,
              91.0,
              92.0,
              93.0,
              94.0,
              95.0,
              96.0,
              97.0,
              98.0,
              99.0
            ]
          }
        ],
        "group": {
          "name": "livro-infantil",
          "title": "Livro Infantil",
          "created": ISODate("2018-07-05T10:00:00.000-0300")
        },
        "keyword": "dentro10",
        "emoji": "📖",
        "actions": [
          {
            "title": "RESGATAR CUPOM",
            "url": "https://www.dentrodahistoria.com.br/"
          }
        ],
        "name": "dentro-da-historia-r-10-00",
        "faceValue": 10.0,
        "quantity": 1.0,
        "comboType": "deal"
      },
      {
        "alliance": {
          "name": "vivo",
          "title": "Vivo",
          "xewards": {
            "provider": "3"
          }
        },
        "title": "Bônus Celular",
        "subTitle": "Ligações para telefones Vivo",
        "description": "Clique em Enviar para confirmar seu bônus.Válido para clientes ativos de planos pessoa física.Pré-Pago e Controle: Bônus válido por 30 dias para chamadas locais para Vivo móvel, SMS e internet.Pós-Pago: Bônus válido por 30 dias para chamadas locais para Vivo móvel e Vivo fixo.O bônus será concedido em até 10 dias.",
        "price": 0.3,
        "deliveryCost": 0.0,
        "catalog": true,
        "type": "defaultPrize",
        "customBonuz": {
          "color": "f65e01",
          "backgroundColor": "f65e01"
        },
        "unit": "R$",
        "deliveryAddress": {
          "questions": [

          ]
        },
        "tags": [
          "bonus"
        ],
        "operators": [
          "vivo"
        ],
        "xewardsOfferCode": "BZCBC001",
        "icon": {
          "href": "https://s3.amazonaws.com/assets-hmg.bonuz.com/public/prizes/img_logo_lists_small_vivo.png",
          "srcset": "img_logo_lists_small_vivo.png 1x,img_logo_lists_small_vivo@2x.png 2x,img_logo_lists_small_vivo@3x.png 3x"
        },
        "deliveryEngine": "xewards",
        "deliveryInfo": {
          "created": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "forwarded": "Agora é só aguardar. Normalmente a entrega do bônus acontece em até 4 horas.",
          "done": "{{#ifvalue status value='canceled'}}Ocorreu um problema ao entregar sua recompensa. Entre em contato com o nosso atendimento. {{else}}Seus créditos em bônus celular já estão disponíveis.{{/ifvalue}}"
        },
        "fullForward": false,
        "shouldQueryStatus": false,
        "xewardsQuantity": 1.0,
        "wideBanner": {
          "href": "",
          "srcset": ""
        },
        "group": {
          "name": "bonus",
          "title": "Bônus Celular",
          "created": ISODate("2018-05-03T14:28:55.671-0300")
        },
        "weight": 0.0,
        "socialWeight": 0.0,
        "keyword": "bonus",
        "emoji": "📱",
        "name": "bonusCelularVivo",
        "faceValue": 1.0,
        "quantity": 2.0,
        "comboType": "deal"
      }
    ]
  }
}
