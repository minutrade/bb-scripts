rs.slaveOk();
var expsBB = [
  'bb-troca-facil-i',
  'bb-troca-facil-ii',
  'bb-combo-digital',
  'bb-combo-digital-estilo',
  'banco-do-brasil-conta-facil',
  'banco-do-brasil-clube-ponto-pra-voce',
  'bb-ourocard-anuidade-bonificada'
];

var prizes = {};
var bonuz = db.getSiblingDB('bonuz');
bonuz.experiences.find({
  name: { $in: expsBB }
}).forEach(function (exp) {
  exp.prizes.forEach(function (prize) {
    var prz = bonuz.prizes.findOne({ name: prize.name });
    prizes[prize.name] = prz;
  })
})

printjson(prizes)