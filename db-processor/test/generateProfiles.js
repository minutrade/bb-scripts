
var mapping = {
  "0025": { "exp": "bb-clube-conta-facil", "value": NumberInt(18) },
  "0026": { "exp": "bb-clube-conta-digital-i", "value": NumberInt(24) },
  "0027": { "exp": "bb-clube-conta-digital-ii", "value": NumberInt(36) },
  "0003": { "exp": "bb-troca-facil-i", "value": NumberInt(24) },
  "0022": { "exp": "bb-clube-facil-ii", "value": NumberInt(32) },
  "0004": { "exp": "bb-troca-facil-ii", "value": NumberInt(41) },
  "0023": { "exp": "bb-clube-digital-i", "value": NumberInt(62) },
  "0019": { "exp": "bb-combo-digital", "value": NumberInt(72) },
  "0024": { "exp": "bb-clube-mais-digital-estilo-i", "value": NumberInt(82) },
  "0018": { "exp": "bb-combo-digital-estilo", "value": NumberInt(90) }
}
var inserts = [];
var count = 0;
db.consumer_bb.find({}).limit(1).forEach(function (consumer) {
  var obj = {
    "consumerId": "66db7cad48664a5bb480ef20c8a1b867",
    "cpf": consumer.cpf,
    "experience": mapping[consumer.codClube].exp,
    "portal": "bb",
    "keys": [
      "portal",
      "cpf",
      "experience",
      "consumerId"
    ],
    "created": new Date(),
    "status": {
      "name": "active",
      "timestamp": new Date()
    },
    "rewardValue": mapping[consumer.codClube].value,
    "nome": "CLIENTE " + consumer.cpf,
    "codigoClube": consumer.codClube,
    "nomeClube": "COMBO DIGITAL ESTILO",
    "cep": "007100000",
    "nomeCarteira": "ESTILO",
    "codigoCarteira": "2112",
    "dataOperacao": "20181025",
    "diaVencimento": NumberInt(5),
    "email": "EMAIL" + consumer.cpf + "Clube@EMAIL.COM",
    "areaLivre": "",
    "telefoneContato": "61990018880"
  }
  inserts.push({ insertOne: { "document": obj } });
  count++;
  if (count % 1000 === 0) {
    db.profiles_test.bulkWrite(inserts);
    inserts = [];
    count = 0;
  }
})
