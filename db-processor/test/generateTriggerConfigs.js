rs.slaveOk();
var expsBB = [
  'bb-troca-facil-i',
  'bb-troca-facil-ii',
  'bb-combo-digital',
  'bb-combo-digital-estilo',
  'banco-do-brasil-conta-facil',
  'banco-do-brasil-clube-ponto-pra-voce',
  'bb-ourocard-anuidade-bonificada'
];

var triggerConfigs = {};
var engage = db.getSiblingDB('engage');
engage.triggerConfigs.find({
  experience: { $in: expsBB }
}).forEach(function (triggerConfig) {
  triggerConfigs[triggerConfig.experience] = triggerConfig;
})

printjson(triggerConfigs)