function buildCoupon(paymentId, rewardId, triggerId, requestId, batchId, consumerId, bucket) {
  var coupon = {
    "origin": "bb-db-processor",
    "refId": "5beb5ce9a2cbb159100ee3dd",
    "paymentId": paymentId,
    "rewardId": rewardId,
    "bucket": bucket,
    "triggerId": triggerId,
    "requestId": requestId,
    "batchId": batchId,
    "consumerId": consumerId,
    "status": {
      "name": "created",
      "timestamp": new Date(),
      "detail": "Coupon generated manually"
    }
  }
  return coupon;
}