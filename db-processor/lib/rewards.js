function buildReward(id, value, consumer, experience, batchId, requestId, triggerId) {
  var dataAtual = new Date();
  var date2Days = new Date(dataAtual.getFullYear(), dataAtual.getMonth(), dataAtual.getDate(), dataAtual.getHours() + 48, 0, 0);
  var experience;

  var reward = {
    "_id": id,
    "consumer": {
      "id": consumer,
      "countryCode": "BR"
    },
    "countryCode": "BR",
    "account": {
      "id": "573f994c068c480100b3f4ff",
      "isPrepaid": false,
      "description": "Operação realizada através do bônus negócio.",
      "reason": "reward",
      "created": ISODate("2018-10-17T06:45:25.171-0300"),
      "responsible": {
        "name": "Banco do Brasil S.A.",
        "email": "bb@minutrade.com"
      }
    },
    "created": dataAtual,
    "trigger": {
      "id": triggerId.str
    },
    "batch": {
      "id": batchId.str
    },
    "request": {
      "id": requestId.str
    },
    "priceless": false,
    "experience": experience,
    "expirationDate": date2Days,
    "amount": NumberInt(value),
    "value": NumberInt(value),
    "status": {
      "name": "created",
      "timestamp": dataAtual
    },
    "trace": [
      {
        "name": "created",
        "timestamp": dataAtual
      }
    ],
    "eligibleDeals": [],
    "offeredDeals": [],
    "chosenDeals": [],
    "appreciationMessage": experience.appreciationMessage,
    "forwardedBy": {},
    "isDonation": false,
    "fallback": false,
    "fallbackCountdown": NumberInt(10),
    "consumerData": {

    },
    "linkedRewards": [],
    "preQualified": false
  }
  return reward;
}
