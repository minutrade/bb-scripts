function buildRequest(id, batchId, raw, experience, data, triggerId, value, reward) {
  var dataAtual = new Date();
  var request = {
    "_id": id,
    "batchId": batchId,
    "raw": raw,
    "batch": {
      "id": batchId,
      "account": {
        "id": "573f994c068c480100b3f4ff",
        "isPrepaid": false,
        "description": "Operação realizada através do bônus negócio.",
        "reason": "reward",
        "created": ISODate("2018-10-17T06:45:25.171-0300"),
        "responsible": {
          "name": "Banco do Brasil S.A.",
          "email": "bb@minutrade.com"
        }
      }
    },
    "experience": {
      "name": experience.name,
      "title": experience.title
    },
    "event": "fee-paid",
    "created": dataAtual,
    "lastUpdated": dataAtual,
    "source": {
      "origin": "bb-db-processor",
      "url": "/integrations/banco-do-brasil/requests"
    },
    "integration": {
      "name": "banco-do-brasil"
    },
    "data": data,
    "dataDescription": {
      "cpf": "Cpf",
      "codClube": "Código do clube"
    },
    "status": {
      "name": "forwarded",
      "timestamp": dataAtual,
      "detail": {

      }
    },
    "newStatus": {
      "name": "dispatched",
      "timestamp": dataAtual,
      "detail": {
        "name": "success"
      }
    },
    "trace": [
      {
        "name": "forwarded",
        "timestamp": dataAtual,
        "detail": {

        }
      },
      {
        "name": "created",
        "timestamp": dataAtual
      }
    ],
    "newTrace": [
      {
        "name": "dispatched",
        "timestamp": dataAtual,
        "detail": {
          "name": "success"
        }
      },
      {
        "name": "processing",
        "timestamp": dataAtual,
        "detail": {

        }
      },
      {
        "name": "enqueued",
        "timestamp": dataAtual,
        "detail": {

        }
      },
      {
        "name": "created",
        "timestamp": dataAtual
      }
    ],
    "errorDescription": null,
    "error": null,
    "triggerId": triggerId.str,
    "rewardValue": NumberInt(value),
    "delivered": {
      "value": NumberInt(0),
      "pricelessPrizesQuantity": NumberInt(0),
      "prizes": [

      ]
    },
    "canceled": {
      "value": NumberInt(0),
      "pricelessPrizesQuantity": NumberInt(0),
      "prizes": [

      ]
    },
    "forwardedValue": NumberInt(0),
    "preQualifiedPrizes": [

    ],
    "registeredDate": dataAtual,
    "result": {
      "rewards": [{
        "id": reward._id.str,
        "amount": reward.amount,
        "countryCode": reward.countryCode,
        "value": reward.value,
        "appreciationMessage": reward.appreciationMessage,
        "fallback": reward.fallback,
        "priceless": reward.priceless,
        "status": reward.status,
        "trace": reward.trace
      }]
    },
    "actionTypes": [
      "reward"
    ]
  };

  return request;
}
