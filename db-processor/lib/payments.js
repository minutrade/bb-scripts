function buildPayment(mobile, reward, deal, dealQuantity) {
  var dataAtual = new Date();

  if (!reward.rewardId && reward._id) {
    reward.rewardId = reward._id;
  }

  var payment = {
    "amount": NumberInt(deal.value / dealQuantity),
    "mobile": mobile,
    "trace": [
      {
        "name": "fowarded",
        "timestamp": dataAtual
      },
      {
        "name": "created",
        "timestamp": dataAtual
      }
    ],
    "reward": reward,
    "deal": deal,
    "deliveryAddress": {
      "questions": []
    },
    "deliveryEngine": deliveryEngine,
    "status": {
      "name": "forwarded",
      "timestamp": dataAtual,
      "detail": {
        "name": "forwarded",
        "description": "Seu pacote de dados será entregue em breve."
      }
    },
    "quantity": NumberInt(dealQuantity),
    "value": NumberInt(deal.value / dealQuantity),
    "countryCode": "BR"
  }

  return payment;
}