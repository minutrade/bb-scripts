function buildTrigger(id, triggerConfigs, data, experience, requestId, reward) {
  var dateNow = new Date();
  var rew = formatReward(reward);
  var trigger = {
    "_id": id,
    "triggerConfigs": triggerConfigs,
    "data": data,
    "language": "pt-BR",
    "source": {
      "origin": "bb-db-processor",
      "url": "/integrations/banco-do-brasil/requests"
    },
    "status": {
      "name": "dispatched",
      "timestamp": dateNow
    },
    "trace": [
      {
        "name": "dispatched",
        "timestamp": dateNow
      },
      {
        "name": "processing",
        "timestamp": dateNow
      },
      {
        "name": "created",
        "timestamp": dateNow
      }
    ],
    "created": dateNow,
    "mappedData": {},
    "actionTypes": [
      "reward"
    ],
    "experience": experience,
    "result": {
      "code": NumberInt(200),
      "data": {
        "rewards": [
          rew
        ],
        "id": id.str,
        "rewardValue": rew.value,
        "delivered": {
          "value": NumberInt(0),
          "pricelessPrizesQuantity": NumberInt(0),
          "prizes": []
        },
        "canceled": {
          "value": NumberInt(0),
          "pricelessPrizesQuantity": NumberInt(0),
          "prizes": []
        },
        "forwardedValue": NumberInt(0),
        "pricelessPrizes": {
          "quantity": NumberInt(0),
          "prizes": []
        },
        "preQualifiedPrizes": [],
        "actionTypes": [
          "reward"
        ]
      }
    },
    "request": {
      "id": requestId.str
    }
  }

  return trigger;
}


function formatReward(reward) {
  var rew = {
    id: reward._id.str,
    amount: reward.amount,
    countryCode: reward.countryCode,
    value: reward.value,
    appreciationMessage: reward.appreciationMessage,
    consumerData: reward.consumerData,
    forwardedBy: {},
    fallback: reward.fallback,
    isDonation: reward.isDonation,
    status: reward.status,
    trace: reward.trace,
    trigger: reward.trigger
  }
  return rew;
}