var bonuzConsumer = db.getSiblingDB('bonuzConsumer')
var path = '/home/caetano.milord/workspace/bb-db-processor/lib/';
load(path + "rewards.js");

bonuzConsumer.consumer_bb.createIndex({ 'cpf': 1 });

bonuzConsumer.consumer_bb.aggregate([
  { $lookup: { from: 'profiles_bb', localField: 'cpf', foreignField: 'cpf', as: 'pro' } },
  { $limit: 1 },
  { $unwind: { path: '$pro', preserveNullAndEmptyArrays: false } }
], { allowDiskUse: true, cursor: {} }).forEach(function (consumerAggregated) {
  var reward = buildReward(consumerAggregated.pro.rewardValue, consumerAggregated.pro.consumerId, consumerAggregated.pro.experience)
  print(JSON.stringify(reward));
});