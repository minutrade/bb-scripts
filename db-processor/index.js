
var bonuzConsumer = db.getSiblingDB("bonuzConsumer");
var bonuzRewards = db.getSiblingDB("bonuz").getCollection("rewards_bb");
var businessRequests = db.getSiblingDB("business").getCollection("requests_bb");
var engageTriggers = db.getSiblingDB("engage").getCollection("triggers_bb");

var path = "/home/caetano.milord/workspace/bb-db-processor";
load(path + "/lib/requests.js");
load(path + "/lib/triggers.js");
load(path + "/lib/rewards.js");
load(path + "/sources/triggerConfigs.js");
load(path + "/sources/experiences.js");

var bulkWriteRequests = [];
var bulkWriteRewards = [];
var bulkWriteTriggers = [];
var count = 0;

bonuzConsumer.consumer_bb.aggregate([
  { $lookup: { from: "profiles_bb", localField: "cpf", foreignField: "cpf", as: "pro" } },
  { $limit: 1 },
  { $unwind: { path: "$pro", preserveNullAndEmptyArrays: false } }
], { allowDiskUse: true, cursor: {} }).forEach(function (consumer) {
  var data = {
    "cpf": consumer.cpf,
    "codClube": consumer.codClube
  };

  var batchId = new ObjectId();
  var requestId = new ObjectId();
  var triggerId = new ObjectId();
  var rewardId = new ObjectId();

  var reward = buildReward(rewardId, consumer.pro.rewardValue, consumer.pro.consumerId, getExperience(consumer.pro.experience), batchId, requestId, triggerId);
  bulkWriteRewards.push({ insertOne: { "document": reward } });

  var request = buildRequest(requestId, batchId, consumer.raw, getExperience(consumer.pro.experience), data, triggerId, consumer.pro.rewardValue, reward);
  bulkWriteRequests.push({ insertOne: { "document": request } });

  var trigger = buildTrigger(triggerId, getTriggerConfig(consumer.pro.experience), data, consumer.pro.experience, requestId, reward);
  bulkWriteTriggers.push({ insertOne: { "document": trigger } });

  count++;
  if (count % 1000 === 0) {
    bonuzRewards.bulkWrite(bulkWriteRewards, { writeConcern: { w: 0 }, ordered: false });
    bulkWriteRewards = [];
    businessRequests.bulkWrite(bulkWriteRequests, { writeConcern: { w: 0 }, ordered: false });
    bulkWriteRequests = [];
    engageTriggers.bulkWrite(bulkWriteTriggers, { writeConcern: { w: 0 }, ordered: false });
    bulkWriteTriggers = [];
    count = 0;
  }
});

if (count > 0) {
  bonuzRewards.bulkWrite(bulkWriteRewards, { writeConcern: { w: 0 }, ordered: false });
  businessRequests.bulkWrite(bulkWriteRequests, { writeConcern: { w: 0 }, ordered: false });
  engageTriggers.bulkWrite(bulkWriteTriggers, { writeConcern: { w: 0 }, ordered: false });
}