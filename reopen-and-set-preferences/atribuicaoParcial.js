//Script para atualzar os prêmios defaults para a qualificação automática.

load('./configExp.js');
var bonuzDb = db.getSiblingDB('bonuz');
// var businessDb = db.getSiblingDB('business');
//Define data de expiração.
var expirationDate = new Date('2018-10-30T20:00:00.000-0300');

// var limit = {
// 	'bb-troca-facil-i': 24,
// 	'bb-troca-facil-ii': 41 ,
// 	'bb-combo-digital': 72,
// 	'bb-combo-digital-estilo': 90,
// 	'banco-do-brasil-conta-facil' : 10,
// 	'banco-do-brasil-clube-ponto-pra-voce': 40,
// 	'bb-ourocard-anuidade-bonificada': 18
// }

var defaulPrizeOperator = {
    'vivo': 'bonusCelularVivo',
    'claro': 'claro-hibrido',
    'oi': 'bonusCelularOi',
    'tim': 'bonusCelularTIM'
}

var expsBB = [
    'bb-troca-facil-i',
    'bb-troca-facil-ii',
    'bb-combo-digital',
    'bb-combo-digital-estilo',
    'banco-do-brasil-conta-facil',
    'banco-do-brasil-clube-ponto-pra-voce',
    'bb-ourocard-anuidade-bonificada'
]

var dateNow = new Date();
var date2DaysAgo = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(), dateNow.getHours() - 48, 0, 0);
var profileDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - 15);

var rewardCursor = bonuzDb.rewards.find({
    'experience.name': { $in: expsBB },
    'status.name': 'created',
    'consumerData.digitalRewardsPreferences': { $exists: false },
    'consumerData.lastChosenDeals': { $exists: false },
    $or: [
        {
            'created': {
                $gt: ISODate('2018-07-31T00:00:00-03:00'),
                $lte: date2DaysAgo
            }
        }, { fallback: true }
    ]
}, {
        'consumer.mobile': 1, 'consumer.operator': 1, 'eligibleDeals.name': 1, 'eligibleDeals.type': 1,
        'eligibleDeals.faceValue': 1, 'value': 1, 'experience.name': 1, 'fallback': 1
    })

var contProcess = 0;
var contUpdate = 0;
rewardCursor.forEach(function (reward) {
    // if (reward.value === limit[reward.experience.name]){
    var ddd = reward.consumer.mobile.substring(2, 4);
    var operator = reward.consumer.operator;
    var configs = configExp.filter(function (config) {
        var ddds = config.ddds.length > 0 ? config.ddds : [ddd];
        if (reward.fallback) {
            return (
                ddds.indexOf(ddd) >= 0 &&
                config.operators.length === 0 &&
                config.experience === reward.experience.name &&
                config.value === reward.value
            );
        }
        else {
            var operators = config.operators.length > 0 ? config.operators : [operator];
            return (
                ddds.indexOf(ddd) >= 0 &&
                operators.indexOf(operator) >= 0 &&
                config.experience === reward.experience.name &&
                config.value === reward.value
            );
        }
    });
    if (configs.length > 0) {
        var configByOperator = configs[0];
        var random = Math.floor(Math.random() * configByOperator.prizes.length);
        var digitalRewardsPreferences = configByOperator.prizes[random];
        var update = {
            $set: {
                consumerData: {
                    digitalRewardsPreferences: digitalRewardsPreferences
                },
                expirationDate: dateNow
            }
        };
        var valorTotal = 0;
        digitalRewardsPreferences.forEach(function (digitalPrefPrize) {
            prizeExists = reward.eligibleDeals.filter(function (e) { return e.name === digitalPrefPrize.name });
            if (prizeExists.length === 0) {
                var prize = bonuzDb.prizes.findOne({ name: digitalPrefPrize.name }, { _id: 0 });
                valorTotal += prize.faceValue;
                if (!update['$push']) {
                    update['$push'] = { eligibleDeals: { '$each': [] } };
                }
                update['$push'].eligibleDeals['$each'].push(prize);
            } else {
                valorTotal += prizeExists[0].faceValue;
            }
        });
        //        print(ddd + ',' + operator + ',' + valorTotal +' , '+ reward.value)
        if (valorTotal < reward.value) {
            var defaultExists = reward.eligibleDeals.filter(function (e) { return e.type === 'defaultPrize' })
            if (defaultExists.length === 0) {
                var prize = bonuzDb.prizes.findOne({ type: 'defaultPrize', operators: reward.consumer.operator, name: defaulPrizeOperator[reward.consumer.operator] }, { _id: 0 });
                if (!update['$push']) {
                    update['$push'] = { eligibleDeals: { '$each': [] } };
                }
                update['$push'].eligibleDeals['$each'].push(prize);
            }
        }
        print('Update _id: ' + reward._id);// + ' / ' + JSON.stringify(update));
        bonuzDb.rewards.update(
            { _id: reward._id },
            update
        );
        if (valorTotal > reward.value && valorTotal <= configByOperator.roundLimit) {
            var subtract = reward.value - configByOperator.roundLimit;
            //            print('Update _id: ' + reward._id + ' / $inc ' + subtract);
            bonuzDb.rewards.update(
                { _id: reward._id, 'eligibleDeals.name': digitalRewardsPreferences[0].name },
                { $inc: { 'eligibleDeals.$.faceValue': subtract } }
            );
        };
        contUpdate = contUpdate + 1;
        if (contUpdate % 1000 === 0) {
            print('Linhas atualizadas ' + contUpdate);
        }
    }
    // }
    contProcess = contProcess + 1;
    if (contProcess % 1000 === 0) {
        print('Linhas processadas ' + contProcess);
    }
});

print('Fim do processo ... \n' +
    'Quantidade de linhas processadas: ' + contProcess + '\n' +
    'Quantidade de linhas atualizadas: ' + contUpdate);

