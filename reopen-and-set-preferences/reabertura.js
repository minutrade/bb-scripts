//rs.slaveOk()
var bonuz = db.getSiblingDB('bonuz');
var engage = db.getSiblingDB('engage');
var business = db.getSiblingDB('business');

var experiences = [
    'bb-troca-facil-i',
    'bb-troca-facil-ii',
    'bb-combo-digital',
    'bb-combo-digital-estilo',
    'banco-do-brasil-conta-facil',
    'banco-do-brasil-clube-ponto-pra-voce',
    'bb-ourocard-anuidade-bonificada'
];

var initialDate = new Date(Date.UTC(2018, 6, 31, 3, 0, 0))
var expirationDateReward = new Date(Date.UTC(2018, 11, 19, 19, 00, 0))

var requests = business.requests.find({
    'experience.name': {
        $in: experiences
    },
    'newStatus.name': 'processed',
    'newStatus.timestamp': {
        $gte: initialDate
    },
    'registeredDate': {
        $gte: initialDate
    },
    'newStatus.detail.description': { $in: ['Parcialmente entregue', 'Cancelado'] }
}, {
        _id: 1,
        triggerId: 1
    }
)

function buildStatus(name, timestamp, detail) {
    var status = {
        "name": "",
        "timestamp": new Date()
    }
    status.name = name;
    if (timestamp) { status.timestamp = timestamp };
    if (detail) { status.detail = detail };
    return status;
};

function reopenReward(id, statusReward) {
    var result = bonuz.rewards.update(
        { _id: id },
        {
            $set: {
                status: statusReward,
                expirationDate: expirationDateReward,
                eligibleDeals: []

            },
            $push: {
                trace: {
                    $each: [statusReward],
                    $position: 0
                }
            }
        }
    );
    return result.nModified === 1;
}

function reopenTrigger(id, statusTrigger) {
    var result = engage.triggers.update(
        { _id: id },
        {
            $set: {
                status: statusTrigger
            },
            $push: {
                trace: {
                    $each: [statusTrigger],
                    $position: 0
                }
            }
        }
    );
    return result.nModified === 1;
}

function reopenRequest(id, statusRequest, newStatusRequest) {
    var result = business.requests.update(
        { _id: id },
        {
            $set: {
                status: statusRequest,
                newStatus: newStatusRequest,
                result: {}
            },
            $push: {
                trace: {
                    $each: [statusRequest],
                    $position: 0
                },
                newTrace: {
                    $each: [newStatusRequest],
                    $position: 0
                }
            }
        }
    );
    return result.nModified === 1;
};

var cont = 0;

requests.forEach(function (request) {
    var reward = bonuz.rewards.find({
        'trigger.id': request.triggerId,
        'status.detail.name': 'canceled',
        'chosenDeals': { $size: 0 },
        'priceless': false
    }).sort({ _id: -1 }).limit(1)

    if (reward[0]) {
        var statusReward = buildStatus("created");
        var updatedReward = reopenReward(reward[0]._id, statusReward);
        if (updatedReward) {
            var statusTrigger = buildStatus("dispatched");
            var updatedTrigger = reopenTrigger(ObjectId(request.triggerId), statusTrigger);
            if (updatedTrigger) {
                var statusRequest = buildStatus('forwarded');
                var newStatusRequest = buildStatus('dispatched');
                var updatedRequest = reopenRequest(request._id, statusRequest, newStatusRequest);
                if (updatedRequest) {
                    cont++;
                } else {
                    print('Request não atualizada,' + request._id);
                }
            } else {
                print('Trigger não atualizada,' + request.triggerId);
            }
        } else {
            print('Reward não atualizada, ' + reward[0]._id);
        }
    } else {
        print('Reward não encontrada para reabertura, TriggerId: ' + request.triggerId);
    };
});

print('Linhas atualizadas: ' + cont);
