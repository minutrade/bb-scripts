# BB Script

Projeto destinado ao armazenamento de scripts criados a partir de demandas do BB.

## Execução

É aconselhado que os scripts sejam executados a partir das máquinas DB Client, existentes em cada um dos ambientes do bonuz, para que a performance destes não seja impactada pela infra.

Para tal, é necessário executar um SCP para a máquina de destino, copiando a pasta com todos os arquivos necessários para a execução do mesmo.

Os scripts devem ser executados através do comando abaixo:

```
mongo --host <host> --port <port> --username <user> --authenticationDatabase admin --quiet <file> -p
```

A senha de banco será requisitada para execução.

## Scripts

* [Reabertura e Atribuição de Preferências](https://bitbucket.org/milrren/bb-scripts/src/master/reopen-and-set-preferences/) - Reabertura de requests para entrega
* [Processamento em batch de pedidos](https://bitbucket.org/milrren/bb-scripts/src/master/db-processor/) - Processamento de pedidos do BB em batches (uso de bulkWrite e/ou mongoimport)
* [Reabertura de requests cancelados por mobile inválido](https://bitbucket.org/milrren/bb-scripts/src/master/reopen-requests-with-invalid-mobile/) - Reabertura de requests cancelados por mobile inválido, gerando profiles, consumers, rewards e payments. Para finalização deste, é necessário executar a task de retryPayments do bonuz Engine

## Autores

* **Caetano Milord** - *Criação do script de reabertura e atribuição e processamento batch*
* **Lucas Machado** - *Criação do script de reabertura e atribuição*
* **Milrren Mattar** - *Criação do script de reabertura de requests cancelados por mobile inválido e processamento batch*
